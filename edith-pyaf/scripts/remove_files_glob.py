import glob
import os

N = 12
LA = 6
WA = 6.0

# Define your pattern
pattern = f"../data/*sites-{N}*LA-{LA}*{WA},*"

# Use glob to find files that match the pattern
files_to_delete = glob.glob(pattern)

# Check if there are matching files and delete them
if files_to_delete:
    for file_path in files_to_delete:
        try:
            os.remove(file_path)
            #print(f"Deleted: {file_path}")
        except Exception as e:
            print(f"Failed to delete {file_path}: {e}")
else:
    print("No files matched the pattern.")

