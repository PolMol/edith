import subprocess
import numpy as np

t_init = -1
t_final = 4
t_max = 101
N = 16
LA = 10
WA = 6.0

for t in 10**(np.linspace(t_init,t_final,t_max)):

	print(f"Copying data for t={t}...")
	s = str(t)[:str(t).find('.') + 4]
	t = np.round(t,3)
	command1 = f"cp ../../../../long-range\ interacting\ MBL/data/N-{N}/LA-{LA}/WA-{WA}/*time_{t}*LA-{LA}*{WA},* ../data/"
	command2 = f"cp ../../../../long-range\ interacting\ MBL/data/N-{N}/LA-{LA}/WA-{WA}/*time_{s}*LA-{LA}*{WA},* ../data/"
	subprocess.run(command1, shell=True)
	subprocess.run(command2, shell=True)
