#/bin/bash

# Define source and destination directories (adjust these paths as needed)
#SOURCE_DIR="/Users/paolomolignini_work/Dropbox/PostDoc/Stockholm/Projects/Ongoing/long-range interacting MBL/data/N-12/LA-10/WA-6.0/initial-state-A-126/"
SOURCE_DIR="/Users/paolomolignini_work/Desktop/MBL-data/data/N-12/LA-6/WA-6.0/initial-state-A-10/"
DESTINATION_DIR="/Users/paolomolignini_work/Dropbox/PostDoc/Stockholm/Projects/Ongoing/EDITH/edith/edith-pyaf/data"

# Ensure the destination directory exists
#mkdir -p "$DESTINATION_DIR"

# Use rsync to copy all contents, including hidden files
rsync -ah --info=progress2 --stats "$SOURCE_DIR"/ "$DESTINATION_DIR"

echo "All files from $SOURCE_DIR have been copied to $DESTINATION_DIR"

