import subprocess
import numpy as np

t_init = -1
t_final = 4
t_max = 101
Wmax = 3.0

for idx, t in enumerate(10**(np.linspace(t_init,t_final,t_max))):

	if idx >= 0:    	
		t = np.round(t,3)
		print(f"Downloading data for t={t}...")
		command = f"scp paolomo@euler.ethz.ch:/cluster/scratch/paolomo/EDITH/edith/edith-pyaf/data/*time_{t}-*LA-12*{Wmax},* ../data/"
		subprocess.run(command, shell=True)

