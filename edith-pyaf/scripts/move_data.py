import subprocess
import numpy as np

t_init = -1
t_final = 4
t_max = 101
sites = 16
LA = 12
Wmax = 6.0

for t in 10**(np.linspace(t_init,t_final,t_max)):
        
        t = np.round(t,3)
        print(f"Moving data for t={t}...")

        # Use `find` to handle large numbers of files
        command = f"find /Users/paolomolignini_work/Dropbox/PostDoc/Stockholm/Projects/Ongoing/long-range\\ interacting\\ MBL/data/exp-times/ -type f -name '*time_{t}*sites-{sites}*LA-{LA}*Wmax-*{Wmax},*' -exec mv {{}} . \\;"
        #command = f"mv /Users/paolomolignini_work/Dropbox/PostDoc/Stockholm/Projects/Ongoing/long-range\ interacting\ MBL/data/exp-times/*time_{t}*sites-{sites}*LA-{LA}*Wmax-[{Wmax},* ."
        subprocess.run(command, shell=True)
