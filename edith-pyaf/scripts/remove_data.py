import subprocess
import numpy as np

t_init = -1
t_final = 4
t_max = 101
N = 16
LA = 14
Wmax = 6.0

for idx, t in enumerate(10**(np.linspace(t_init,t_final,t_max))):

	#t = np.round(t,3)
	s = str(t)[:str(t).find('.') + 4]
	print(f"Removing data for t={s}...")
	command = f"rm ../data/*time_{s}*sites-{N}*LA-{LA}*Wmax-[{Wmax}*"
	subprocess.run(command, shell=True)

