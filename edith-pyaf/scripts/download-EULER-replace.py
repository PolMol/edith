import subprocess
import numpy as np
import os
import glob

t_init = -1.0
t_final = 4.0
t_max = 101
Wmax = 6.0
sites = 14
LA = 8

for idx, t in enumerate(10**(np.linspace(t_init, t_final, t_max))):
    if idx >= 21:
        # Round t to three decimal places
        s = str(t)[:str(t).find('.') + 4]
        t = np.round(t, 3)
        print(idx)
        print(f"Downloading data for t={s}...")

        # Download the file with the approximate time value
        #command = f"scp paolomo@euler.ethz.ch:/cluster/scratch/paolomo/EDITH/edith/edith-pyaf/data/*time_{s}*sites-{sites}*LA-{LA}*{Wmax},* ."
        command = f"scp paolomo@euler.ethz.ch:/cluster/scratch/paolomo/EDITH/edith/edith-pyaf/data/*time_{t}*sites-{sites}*LA-{LA}*{Wmax},* ."
        
        subprocess.run(command, shell=True)

        # Find downloaded files matching time in the file name
        #for filename in glob.glob(f"*time_{s}*sites-{sites}*LA-{LA}*{Wmax},*"):
        #    # Define the new filename with t rounded to three decimal places
        #    new_filename = filename.replace(f"time_{s}", f"time_{t:.3f}")
        #    # Rename the file
        #    os.rename(filename, new_filename)
        #    print(f"Renamed {filename} to {new_filename}")

