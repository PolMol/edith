#/bin/bash

for (( i=0; i<10100; i+=100 )); do

	echo "Copying i=${i}"
	cp /Users/paolomolignini_work/Dropbox/PostDoc/Stockholm/Projects/Ongoing/long-range\ interacting\ MBL/data/zz_correlation_matrix_time_${i}.0*2.5,* ../data/
done
