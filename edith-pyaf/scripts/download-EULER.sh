#!/bin/bash

tmax=10100

for ((t=0; t<${tmax}; t+=100));
do
	echo "Downloading data for t=${t}..."
	scp "paolomo@euler.ethz.ch:/cluster/scratch/paolomo/EDITH/edith/edith-pyaf/data/*time_${t}.0*LA-12.0*6.0,*" .	

done
