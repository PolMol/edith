import subprocess
import numpy as np

t_init = -1.0
t_final = 4.0
t_max = 101
Wmax = 6.0
sites = 14
LA = 8

#for idx,t in enumerate(10**(np.linspace(t_init,t_final,t_max))):
#
#	if idx>=0:
#    	
#		t = np.round(t,3)
#		print(idx)
#		print(f"Downloading data for t={t}...")
#		command = f"scp -P 31422 pamo6261@sol-login.fysik.su.se:/cfs/data/pamo6261/NEW-EDITH/edith-pyaf/data/*time_{t}-*sites-{sites}*LA-{LA}*{Wmax},* ."
#		subprocess.run(command, shell=True)





print(f"Downloading data for L={sites}, LA={LA}...")
command = f"scp -P 31422 pamo6261@sol-login.fysik.su.se:/cfs/data/pamo6261/NEW-EDITH/edith-pyaf/data/*sites-{sites}*LA-{LA}*{Wmax},* ."
subprocess.run(command, shell=True)

