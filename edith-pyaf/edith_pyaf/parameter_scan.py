#!/usr/bin/env python
##########################################################################################
# This python program performs exact diagonalization of closed systems.
###########################################################################################


######################################
#############  IMPORTS   #############
######################################
from __future__ import division
from __future__ import print_function
import numpy as np
from numpy import cos, sin, sqrt, arctan, tan, exp
import itertools
import importlib as imp
from tqdm import tqdm

# CUSTOM modules:
from . import input as inp
from . import input_output as ino
from . import run
from . import viz
from . import utils
######################################

######################################
#### INPUT ####
############################################################################
input_file="input.dat"
model = "bosonic-interacting-Hatano-Nelson"
parameters = ["U", "J"]
static_parameters = {"N": 8, "Vmax": 1.0, "V": None, "eta": [0.0, 0.0], "gamma": [0.0, 0.0], "max_occ": 8}
extra_parameters = {"t": 1.0}    # Other combinations of parameters used to generate data
start_values = {"U": -2.0, "J": -2.0}
end_values = {"U": 2.0, "J": 2.0}
n_points = {"U": 20, "J": 20}
sites = 8
BC = "OBC"

# TODO: modify also static parameters in input file?
rounding_prec = 4


# Parameter scan flags:
exec_scan=False                        # run the scan
plot_parameter_scan=True            # plot the result
plot_GS_IPR=False                      # plot of various quantities
plot_MB_IPR=False                     # plot of various quantities
plot_evals=False                    
plot_gap=True
energy_gap_indices=[1,0]
vmin=1e-9                           # Minimal value to plot
vmax=None                            # Maximal value to plot
############################################################################


#TODO: unify all loops over different models
if exec_scan:

      # Modify static parameters in input file: TODO: expand this to all the input parameters.
      ino.modify_input(input_file=input_file, target="model=", value=model)
      ino.modify_input(input_file=input_file, target="sites=", value=sites)
      ino.modify_input(input_file=input_file, target="BC=", value=BC)

      if model=="dipolar-Hatano-Nelson":
            if "N" in static_parameters.keys():
                  ino.modify_input(input_file=input_file, target="Ham_par1=", value=static_parameters["N"])
            ino.modify_input(input_file=input_file, target="energy_gap_indices=", value=energy_gap_indices)

            # self-consistent eigenvalue determination:
            if "N" in static_parameters.keys():
                  n_lowest_evals = utils.binomial(sites, int(static_parameters["N"]))
                  ino.modify_input(input_file=input_file, target="n_lowest_eigenvalues=", value=n_lowest_evals)


      ######################################
      #### transverse field Ising model ####
      ############################################################################

      if plot_evals==True:
            evals = []

      if model=="transverse-Ising":
            for par in tqdm(parameters):
                  p_list = np.linspace(start_values[par], end_values[par], n_points[par])
                  for value in tqdm(p_list):

                        value = np.round(value, rounding_prec)

                        print(f"Generating data for {par}={value}")

                        # Modifying the input file:
                        if par=="hx":
                              ino.modify_input(input_file=input_file, target="Ham_par2=", value=value)
                        imp.reload(inp)   # Here the input is reloaded in the import!

                        # Calculating the eigenvalues and saving the data:
                        Ham = run.main(input_filename="input.dat") 

                        if inp.plot_evals==True:
                              # Load data:
                              evals.append(ino.load_eigenvalues(Ham=Ham))
      ############################################################################

      ######################################
      #### Staggered Heisenberg model ####
      ############################################################################
      elif model=="staggered-Heisenberg":
            for par in tqdm(parameters):
                  p_list = np.linspace(start_values[par], end_values[par], n_points[par])
                  for value in tqdm(p_list):
                        value = np.round(value, rounding_prec)
                        print(f"Generating data for {par}={value}")

                        # Modifying the input file:
                        if par=="hs":
                              ino.modify_input(input_file=input_file, target="Ham_par2=", value=value)
                              imp.reload(inp)   # Here the input is reloaded in the import!

                        # Calculating the observables and saving the data:
                        Ham = run.main(input_filename="input.dat") 

                        if inp.plot_evals==True:
                              # Load data:
                              evals.append(ino.load_eigenvalues(Ham=Ham))

      ############################################################################

      ######################################
      #### Dipolar Aubry-Andre ####
      ############################################################################
      elif model=="dipolar-Aubry-Andre":

            # Defining all the parameter lists:
            par_lists = {}
            args = []
            for par in parameters:
                  if par=="Delta":
                        par_lists[par] = np.linspace(start_values[par], end_values[par], n_points[par])
                  elif par=="eta":
                        par_lists[par] = 10**(np.linspace(start_values[par], end_values[par], n_points[par]))

                  args.append(par_lists[par])


            # Loop over all combination of parameters:
            for combination in tqdm(itertools.product(*args)):
            
                  out_str = "Generating data for "
                  for i in range(len(parameters)):
                        out_str += f"{parameters[i]}={combination[i]}     "
                  print(out_str)

                  # Iteratively modify the input file for each parameter:
                  for idx, value in enumerate(combination):

                        par=parameters[idx]
                        value = np.round(value, rounding_prec)

                        # Modifying the input file:
                        if par=="Delta":
                              ino.modify_input(input_file=input_file, target="Ham_par3=", value=value)

                        if par=="eta":
                              ino.modify_input(input_file=input_file, target="Ham_par6=", value=[value])    #eta has to be given as a list!

                        if par=="N":
                              ino.modify_input(input_file=input_file, target="Ham_par1=", value=value)


                  imp.reload(inp)   # Here the input is reloaded in the import!

                  # Doing the main calculation:
                  Ham = run.main(input_filename="input.dat") 
      ############################################################################

      ######################################
      #### Dipolar Hatano-Nelson ####
      ############################################################################
      elif model=="dipolar-Hatano-Nelson":

            # Defining all the parameter lists:
            par_lists = {}
            args = []
            for par in parameters:
                  par_lists[par] = np.linspace(start_values[par], end_values[par], n_points[par])
                  args.append(par_lists[par])

            # Loop over all combination of parameters:
            for combination in tqdm(itertools.product(*args)):
            
                  out_str = "Generating data for "
                  for i in range(len(parameters)):
                        out_str += f"{parameters[i]}={combination[i]}     "
                  print(out_str)

                  # Iteratively modify the input file for each parameter:
                  for idx, value in enumerate(combination):

                        par=parameters[idx]
                        value = np.round(value, rounding_prec)

                        # Modifying the input file:
                        ## Used to benchmark against PRB 106, L121102 (2022):
                        if par=="imbalance":          # Imbalance affects both entries in J: J=[t-imbalance, t+imbalance], similarly for gamma (impurity hopping).
                              ino.modify_input(input_file=input_file, target="Ham_par2=", value=[np.round(extra_parameters["t"] - value, rounding_prec), np.round(extra_parameters["t"] + value, rounding_prec)])
                              if BC=="PBC":
                                    ino.modify_input(input_file=input_file, target="Ham_par6=", value=[np.round(extra_parameters["t"] + value, rounding_prec), np.round(extra_parameters["t"] - value, rounding_prec)])
                              elif BC=="APBC":
                                    ino.modify_input(input_file=input_file, target="Ham_par6=", value=[-np.round(extra_parameters["t"] + value, rounding_prec), -np.round(extra_parameters["t"] - value, rounding_prec)])
                              elif BC=="OBC":
                                    ino.modify_input(input_file=input_file, target="Ham_par6=", value=[0.0, 0.0])

                        if par=="eta":
                              arr=value*np.array([1, 0.125, 0.037037, 0.015625, 0.008, 0.004630])#, 0.002915, 0.001953, 0.001372, 0.001])
                              #arr=value*np.array([1, 0.125, 0.037037])
                              ino.modify_input(input_file=input_file, target="Ham_par5=", value=list(arr))    #eta has to be given as a list!


                  imp.reload(inp)   # Here the input is reloaded in the import!

                  # Doing the main calculation:
                  Ham = run.main(input_filename="input.dat") 
      ############################################################################

      #######################################
      #### BOSONIC Hatano-Nelson ####
      ############################################################################
      elif model=="bosonic-interacting-Hatano-Nelson":

            # Defining all the parameter lists:
            par_lists = {}
            args = []
            for par in parameters:
                  par_lists[par] = 10**np.linspace(start_values[par], end_values[par], n_points[par])
                  args.append(par_lists[par])

            # Loop over all combination of parameters:
            for combination in tqdm(itertools.product(*args)):
            
                  out_str = "Generating data for "
                  for i in range(len(parameters)):
                        out_str += f"{parameters[i]}={combination[i]}     "
                  print(out_str)

                  # Iteratively modify the input file for each parameter:
                  for idx, value in enumerate(combination):

                        par=parameters[idx]
                        value = np.round(value, rounding_prec)

                        if par=="J":
                              arr=np.array([value, np.round(value*0.1,rounding_prec+1)])
                              ino.modify_input(input_file=input_file, target="Ham_par2=", value=list(arr))    #J has to be given as a list!

                        if par=="U":
                              ino.modify_input(input_file=input_file, target="Ham_par5=", value=value)


                  imp.reload(inp)   # Here the input is reloaded in the import!

                  # Doing the main calculation:
                  Ham = run.main(input_filename="input.dat") 
      ############################################################################




#####################################
#### PLOT of the parameter scan  ####
########################################################################
if plot_parameter_scan:

      # Defining all the parameter lists:
      par_lists = {}
      args = []
      for par in parameters:
            if model=="dipolar-Aubry-Andre":
                  if par=="Delta": 
                        par_lists[par] = np.linspace(start_values[par], end_values[par], n_points[par])
                  elif par=="eta":
                        par_lists[par] = 10**(np.linspace(start_values[par], end_values[par], n_points[par]))
            elif model=="bosonic-interacting-Hatano-Nelson":
                  par_lists[par] = 10**(np.linspace(start_values[par], end_values[par], n_points[par]))

            else:
                  par_lists[par] = np.linspace(start_values[par], end_values[par], n_points[par])

            args.append(par_lists[par])

      # Initialize observables:
      shape = tuple(n_points[parameters[k]] for k in range(len(parameters)))
      if plot_MB_IPR==True:
            MB_IPR_arr_avg = np.zeros(shape=shape)
      if plot_GS_IPR==True:
            GS_IPR_arr_avg = np.zeros(shape=shape)
      if plot_gap==True:
            gap_arr_avg = np.zeros(shape=shape)

      # Loop over all combination of parameters:
      for idx, combination in tqdm(utils.enumerated_product(*args)):
            
            # Generating output string and dictionary for current parameter selection 
            # (merged with static parameters)
            pars = dict(static_parameters)
            out_str = "Loading data for "

            for i in range(len(parameters)):
                  rounded_value = np.round(combination[i], rounding_prec)

                  # Adding to dictionary pars parameters being changed in scan: 
                  if parameters[i]=="imbalance":      # "imbalance" is not a standard parameter of model "dipolar-Hatano-Nelson", needs to be converted to JL, JR etc.
                        parameter_value = rounded_value
                        JL = np.round(extra_parameters["t"] - combination[i], rounding_prec)
                        JR = np.round(extra_parameters["t"] + combination[i], rounding_prec)
                        pars["J"]=[JL,JR]
                        
                        gammaL = np.round(extra_parameters["t"] - combination[i], rounding_prec)
                        gammaR = np.round(extra_parameters["t"] + combination[i], rounding_prec)
                        if BC=="APBC":
                              pars["gamma"]=[-gammaR,-gammaL]
                        elif BC=="PBC":
                              pars["gamma"]=[gammaR, gammaL]
                        elif BC=="OBC":
                              pars["gamma"]=0.0

                  if parameters[i]=="eta":
                        parameter_value = list(rounded_value*np.array([1, 0.125, 0.037037, 0.015625, 0.008, 0.004630]))#, 0.002915, 0.001953, 0.001372, 0.001])
                        pars[parameters[i]] = parameter_value
                  elif parameters[i]=="J":
                        if model=="bosonic-interacting-Hatano-Nelson":
                              parameter_value = rounded_value
                              pars[parameters[i]] = [parameter_value, np.round(parameter_value*0.1,rounding_prec+1)]
                              #pars[parameters[i]] = [parameter_value, parameter_value]

                        else:
                              parameter_value = rounded_value
                              pars[parameters[i]] = [parameter_value]

                  elif parameters[i]=="gamma" or parameters[i]=="W":
                        parameter_value = rounded_value
                        pars[parameters[i]] = [parameter_value]


                  else:
                        parameter_value = rounded_value
                        pars[parameters[i]] = parameter_value
                  out_str += f"{parameters[i]}={str(parameter_value)}     "

            print(out_str) 
            
            par_keys = list(pars.keys())
            par_keys.sort()
            sorted_pars = {i: pars[i] for i in par_keys}                
            # pop imbalance from parameter dictionary:
            if "imbalance" in sorted_pars.keys():
                  del sorted_pars["imbalance"]

            # Load data
            if plot_MB_IPR==True:
                  MB_IPR_arr_avg[idx] = ino.load_obs(model=model,
                                              sites=sites, 
                                              pars=sorted_pars,
                                              BC=BC,
                                              obs_name="MB_IPR_avg")
                  print(f"MB_IPR value: {MB_IPR_arr_avg[idx]}")

                  if len(parameters)==2:
                        viz.plot_observable_vs_two_parameters(obs_name="MB_IPR_avg",
                                                              obs=MB_IPR_arr_avg, 
                                                              pars=parameters, 
                                                              par_lists=par_lists, 
                                                              model=model, 
                                                              sites=sites, 
                                                              BC=BC, 
                                                              static_pars=static_parameters,
                                                              log_opt="eta")

            if plot_GS_IPR==True:
                  GS_IPR_arr_avg[idx] = ino.load_obs(model=model,
                                              sites=sites, 
                                              pars=sorted_pars,
                                              BC=BC,
                                              obs_name="GS_IPR_avg")
                  print(f"GS_IPR value: {GS_IPR_arr_avg[idx]}")
                  
                  if len(parameters)==2:
                        viz.plot_observable_vs_two_parameters(obs_name="GS_IPR_avg",
                                                              obs=GS_IPR_arr_avg, 
                                                              pars=parameters, 
                                                              par_lists=par_lists, 
                                                              model=model, 
                                                              sites=sites, 
                                                              BC=BC, 
                                                              static_pars=static_parameters,
                                                              log_opt=["U", "J"])                        

            
            if plot_gap==True:
                  gap_arr_avg[idx] = ino.load_obs(model=model,
                                              sites=sites, 
                                              pars=sorted_pars,
                                              BC=BC,
                                              obs_name="energy_gap_avg")
                  print(f"gap_arr_avg value: {gap_arr_avg[idx]}")
                  if gap_arr_avg[idx] == 0:
                        gap_arr_avg[idx] = 1e-10

                  if len(parameters)==2:
                        # Change the values that are zero to a tiny number:


                        viz.plot_observable_vs_two_parameters(obs_name="Average energy gap",
                                                              obs=gap_arr_avg, 
                                                              pars=parameters, 
                                                              par_lists=par_lists, 
                                                              model=model, 
                                                              sites=sites, 
                                                              BC=BC, 
                                                              static_pars=static_parameters,
                                                              energy_gap_indices=energy_gap_indices,
                                                              vmin=vmin,
                                                              vmax=vmax,
                                                              log_opt=["U", "J"],
                                                              log_plot=True)  

                  print(gap_arr_avg)

########################################################################

