#!/usr/bin/env python
#####################################
#########   IMPORTS   ############
#####################################
import ast
#####################################

#print("input_filename in input2:", input_filename)

class Input():
    """
    Methods
    ----------
     

    Parameters
    ----------
    

    Examples
    --------
  
    """

    def __init__(self, 
                 input_filename,
                 **kwargs):
        
        """
        Parameters:
        ----------
        input_filename, str: the path to the input file (usually input.dat).

        """
        #####################################
        #########   PARAMETERS   ############
        #####################################
        self.input_filename = input_filename
        ########################################################################
        # Job parameters
        ########################################################################
        self.clean_data = self.load_input_parameters(input_filename=input_filename, target_parameter_name="clean_data", p_type="bool")
        self.clean_plots = self.load_input_parameters(input_filename=input_filename, target_parameter_name="clean_plots", p_type="bool")
        self.generate_data = self.load_input_parameters(input_filename=input_filename, target_parameter_name="generate_data", p_type="bool")
        self.averaging = self.load_input_parameters(input_filename=input_filename, target_parameter_name="averaging", p_type="bool")
        self.averaging_method = self.load_input_parameters(input_filename=input_filename, target_parameter_name="averaging_method", p_type="str")
        ############################################################################################
        # Hamiltonian parameters (the meaning of each parameter depends on which Hamiltonian you are implementing)
        ########################################################################
        self.stats = self.load_input_parameters(input_filename=input_filename, target_parameter_name="stats", p_type="str")
        self.model = self.load_input_parameters(input_filename=input_filename, target_parameter_name="model", p_type="str")
        self.sites = self.load_input_parameters(input_filename=input_filename, target_parameter_name="sites", p_type="int")
        self.BC = self.load_input_parameters(input_filename=input_filename, target_parameter_name="BC", p_type="str")
        self.sparse = self.load_input_parameters(input_filename=input_filename, target_parameter_name="sparse", p_type="bool")
        self.Ham_par1 = self.load_input_parameters(input_filename=input_filename, target_parameter_name="Ham_par1", p_type="float")
        self.Ham_par2 = self.load_input_parameters(input_filename=input_filename, target_parameter_name="Ham_par2", p_type="float")
        self.Ham_par3 = self.load_input_parameters(input_filename=input_filename, target_parameter_name="Ham_par3", p_type="float")
        self.Ham_par4 = self.load_input_parameters(input_filename=input_filename, target_parameter_name="Ham_par4", p_type="float")
        self.Ham_par5 = self.load_input_parameters(input_filename=input_filename, target_parameter_name="Ham_par5", p_type="float")
        self.Ham_par6 = self.load_input_parameters(input_filename=input_filename, target_parameter_name="Ham_par6", p_type="float")
        self.Ham_par7 = self.load_input_parameters(input_filename=input_filename, target_parameter_name="Ham_par7", p_type="float")
        self.Ham_par8 = self.load_input_parameters(input_filename=input_filename, target_parameter_name="Ham_par8", p_type="float")
        self.Ham_par9 = self.load_input_parameters(input_filename=input_filename, target_parameter_name="Ham_par9", p_type="float")
        self.Ham_par10 = self.load_input_parameters(input_filename=input_filename, target_parameter_name="Ham_par10", p_type="float")
        self.realizations = self.load_input_parameters(input_filename=input_filename, target_parameter_name="realizations", p_type="int")
        ########################################################################
        # Parameters for diagonalization
        ########################################################################
        self.n_lowest_eigenvalues = self.load_input_parameters(input_filename=input_filename, target_parameter_name="n_lowest_eigenvalues", p_type="int")
        self.maxiter_Arnoldi = self.load_input_parameters(input_filename=input_filename, target_parameter_name="maxiter_Arnoldi", p_type="int")      # maximal number of iteration steps in the Arnoldi method for finding eigenvalues
        self.tolerance_Arnoldi = self.load_input_parameters(input_filename=input_filename, target_parameter_name="tolerance_Arnoldi", p_type="float")
        self.save_evals = self.load_input_parameters(input_filename=input_filename, target_parameter_name="save_evals", p_type="bool")
        self.compute_evecs = self.load_input_parameters(input_filename=input_filename, target_parameter_name="compute_evecs", p_type="bool")
        self.save_evecs = self.load_input_parameters(input_filename=input_filename, target_parameter_name="save_evecs", p_type="bool")
        ########################################################################
        # Parameters for dynamics
        ########################################################################
        self.dynamics = self.load_input_parameters(input_filename=input_filename, target_parameter_name="dynamics", p_type="bool")
        self.save_time_evol = self.load_input_parameters(input_filename=input_filename, target_parameter_name="save_time_evol", p_type="bool")
        self.initial_state_name = self.load_input_parameters(input_filename=input_filename, target_parameter_name="initial_state_name", p_type="str")
        self.initial_state_mode_sys = self.load_input_parameters(input_filename=input_filename, target_parameter_name="initial_state_mode_sys", p_type="str")
        self.initial_state_mode_bath = self.load_input_parameters(input_filename=input_filename, target_parameter_name="initial_state_mode_bath", p_type="str")
        self.initial_state_list_sys = self.load_input_parameters(input_filename=input_filename, target_parameter_name="initial_state_list_sys", p_type="int")
        self.initial_state_list_bath = self.load_input_parameters(input_filename=input_filename, target_parameter_name="initial_state_list_bath", p_type="int")
        self.initial_state_file_sys = self.load_input_parameters(input_filename=input_filename, target_parameter_name="initial_state_file_sys", p_type="str")
        self.initial_state_file_bath = self.load_input_parameters(input_filename=input_filename, target_parameter_name="initial_state_file_bath", p_type="str")
        self.sys_eigenstate = self.load_input_parameters(input_filename=input_filename, target_parameter_name="sys_eigenstate", p_type="int")
        self.bath_eigenstate = self.load_input_parameters(input_filename=input_filename, target_parameter_name="bath_eigenstate", p_type="int")
        self.L_bath = self.load_input_parameters(input_filename=input_filename, target_parameter_name="L_bath", p_type="int")
        ########################
        self.initial_time = self.load_input_parameters(input_filename=input_filename, target_parameter_name="initial_time", p_type="float")
        self.final_time = self.load_input_parameters(input_filename=input_filename, target_parameter_name="final_time", p_type="float")
        self.time_points = self.load_input_parameters(input_filename=input_filename, target_parameter_name="time_points", p_type="int")
        self.time_spacing = self.load_input_parameters(input_filename=input_filename, target_parameter_name="time_spacing", p_type="str")
        ########################################################################
        # Parameters for observable calculations -- ground state
        ########################################################################
        self.energy_gap = self.load_input_parameters(input_filename=input_filename, target_parameter_name="energy_gap", p_type="bool")
        self.energy_gap_indices = self.load_input_parameters(input_filename=input_filename, target_parameter_name="energy_gap_indices", p_type="int")
        self.energy_gap_kind = self.load_input_parameters(input_filename=input_filename, target_parameter_name="energy_gap_kind", p_type="str")
        self.GS_IPR = self.load_input_parameters(input_filename=input_filename, target_parameter_name="GS_IPR", p_type="bool")
        self.MB_IPR = self.load_input_parameters(input_filename=input_filename, target_parameter_name="MB_IPR", p_type="bool")
        self.mean_density = self.load_input_parameters(input_filename=input_filename, target_parameter_name="mean_density", p_type="bool")
        self.mean_dens_idx = self.load_input_parameters(input_filename=input_filename, target_parameter_name="mean_dens_idx", p_type="str")
        self.DOS = self.load_input_parameters(input_filename=input_filename, target_parameter_name="DOS", p_type="bool")
        self.LDOS = self.load_input_parameters(input_filename=input_filename, target_parameter_name="LDOS", p_type="bool")
        self.broadening = self.load_input_parameters(input_filename=input_filename, target_parameter_name="broadening", p_type="float")
        self.spin_profile_z_gs = self.load_input_parameters(input_filename=input_filename, target_parameter_name="spin_profile_z_gs", p_type="bool")
        self.correlation_zz_gs = self.load_input_parameters(input_filename=input_filename, target_parameter_name="correlation_zz_gs", p_type="bool")
        self.bath_averaged_correlation_zz_gs = self.load_input_parameters(input_filename=input_filename, target_parameter_name="bath_averaged_correlation_zz_gs", p_type="bool")
        self.correlation_length_gs = self.load_input_parameters(input_filename=input_filename, target_parameter_name="correlation_length_gs", p_type="bool")
        self.correlation_length_gs_fit = self.load_input_parameters(input_filename=input_filename, target_parameter_name="correlation_length_gs_fit", p_type="bool")
        self.connected = self.load_input_parameters(input_filename=input_filename, target_parameter_name="connected", p_type="bool")
        ########################################################################
        # Parameters for observable calculations -- dynamics
        ########################################################################
        self.spin_profile_z_dyn = self.load_input_parameters(input_filename=input_filename, target_parameter_name="spin_profile_z_dyn", p_type="bool")
        self.correlation_zz_dyn = self.load_input_parameters(input_filename=input_filename, target_parameter_name="correlation_zz_dyn", p_type="bool")
        self.bath_averaged_correlation_zz_dyn = self.load_input_parameters(input_filename=input_filename, target_parameter_name="bath_averaged_correlation_zz_dyn", p_type="bool")
        self.correlation_length_dyn = self.load_input_parameters(input_filename=input_filename, target_parameter_name="correlation_length_dyn", p_type="bool")
        self.correlation_length_dyn_fit = self.load_input_parameters(input_filename=input_filename, target_parameter_name="correlation_length_dyn_fit", p_type="bool")
        self.zz_corr_vmin = self.load_input_parameters(input_filename=input_filename, target_parameter_name="zz_corr_vmin", p_type="float")
        self.zz_corr_vmax = self.load_input_parameters(input_filename=input_filename, target_parameter_name="zz_corr_vmax", p_type="float")
        self.zz_corr_log_plot = self.load_input_parameters(input_filename=input_filename, target_parameter_name="zz_corr_log_plot", p_type="bool")
        self.zz_corr_abs_plot = self.load_input_parameters(input_filename=input_filename, target_parameter_name="zz_corr_abs_plot", p_type="bool")
        ########################################################################
        # Parameters for output
        ########################################################################
        self.print_Hamiltonian = self.load_input_parameters(input_filename=input_filename, target_parameter_name="print_Hamiltonian", p_type="bool")
        self.print_basis = self.load_input_parameters(input_filename=input_filename, target_parameter_name="print_basis", p_type="bool")
        self.print_gs_energy = self.load_input_parameters(input_filename=input_filename, target_parameter_name="print_gs_energy", p_type="bool")
        self.print_es_energies = self.load_input_parameters(input_filename=input_filename, target_parameter_name="print_es_energies", p_type="bool")
        self.print_gs = self.load_input_parameters(input_filename=input_filename, target_parameter_name="print_gs", p_type="bool")
        self.print_es = self.load_input_parameters(input_filename=input_filename, target_parameter_name="print_es", p_type="bool")
        self.print_dyn_state = self.load_input_parameters(input_filename=input_filename, target_parameter_name="print_dyn_state", p_type="bool")
        ########################################################################
        # Parameters for visualization
        ########################################################################
        self.plot_Hamiltonian = self.load_input_parameters(input_filename=input_filename, target_parameter_name="plot_Hamiltonian", p_type="bool")
        self.plot_evals = self.load_input_parameters(input_filename=input_filename, target_parameter_name="plot_evals", p_type="bool")
        self.plot_evecs = self.load_input_parameters(input_filename=input_filename, target_parameter_name="plot_evecs", p_type="bool")
        self.plot_evecs_num = self.load_input_parameters(input_filename=input_filename, target_parameter_name="plot_evecs_num", p_type="int")
        self.plot_DOS = self.load_input_parameters(input_filename=input_filename, target_parameter_name="plot_DOS", p_type="bool")
        self.plot_LDOS = self.load_input_parameters(input_filename=input_filename, target_parameter_name="plot_LDOS", p_type="bool")
        self.plot_mean_density = self.load_input_parameters(input_filename=input_filename, target_parameter_name="plot_mean_density", p_type="bool")
        self.plot_spin_profile = self.load_input_parameters(input_filename=input_filename, target_parameter_name="plot_spin_profile", p_type="bool")
        self.plot_correlation = self.load_input_parameters(input_filename=input_filename, target_parameter_name="plot_correlation", p_type="bool")
        self.plot_dynamics = self.load_input_parameters(input_filename=input_filename, target_parameter_name="plot_dynamics", p_type="bool")
        self.movie_dynamics = self.load_input_parameters(input_filename=input_filename, target_parameter_name="movie_dynamics", p_type="bool")
        ########################################################################
        # Other parameters
        ########################################################################
        self.verbose = self.load_input_parameters(input_filename=input_filename, target_parameter_name="verbose", p_type="bool")
        ########################################################################

        self.input_checks()

    ############################################################################################
    def load_input_parameters(self, 
                              input_filename, 
                              target_parameter_name, 
                              p_type):
        """
        This function loads the input parameters from the input file (usually input.dat) into the instance of the Input class.
        
        """
        with open(input_filename) as file:
            for line in file:
                # Remove leading and trailing whitespaces
                line = line.strip()

                # Ignore empty lines
                if not line:
                    continue

                # Ignore comments (text after #)
                line = line.split('#')[0].strip()

                # Split the line into parameter and value using '=' as a delimiter
                parts = line.split('=')

                # Ensure that the line contains '=' and has a valid parameter and value
                if len(parts) == 2:
                    parameter_name = parts[0].strip()
                    parameter_value = ast.literal_eval(parts[1].strip())
                
                    if parameter_name==target_parameter_name:
                        #print(parameter_name, ":", parameter_value)
                        
                        if p_type=="str":
                            return str(parameter_value)
                        elif p_type=="float":
                            if type(parameter_value)==list:
                                return [parameter_value[i] for i in range(len(parameter_value))]
                            elif parameter_value==None:
                                return None
                            else:
                                return float(parameter_value)
                        elif p_type=="int":
                            if type(parameter_value)==list:
                                return [int(parameter_value[i]) for i in range(len(parameter_value))]
                            elif parameter_value==None:
                                return None
                            else:
                                return int(parameter_value)
                        elif p_type=="bool":
                            return parameter_value
    ############################################################################################



    ############################################################################################
    def input_checks(self):
        if self.BC != "OBC" and self.BC != "PBC" and self.BC != "APBC":
            raise AttributeError("Input variable 'BC' can only be one of the following strings: 'OBC', 'PBC', 'APBC'.")
    ############################################################################################

