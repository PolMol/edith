#!/usr/bin/env python
############################################################################################
##### This module deals with input/output.
############################################################################################


####################################
###########  IMPORTS   #############
####################################
import time
import numpy as np 
import scipy as sp
import datetime
import copy     # for deep copies, e.g. of inputs objects, that do not modify the original.

# Custom modules:
from . import utils
from . import hams
from . import viz
from . import diag
####################################

# TODO: unify reading input (observables) into single function
# TODO: unify writing output (observables) into single function



######################################################################################
######################################################################################
########## SAVING DATA
######################################################################################
######################################################################################

######################################################################################
def save_eigenvalues(model, 
                     sites, 
                     pars, 
                     BC, 
                     eigs, 
                     timestamp=False,
                     format=".npy"):
    """
    Saves the eigenvalues in a .dat or .npy file.

    Parameters:
    -----------
    model, str: name of the model.
    sites, int: number of sites L in the system.
    pars, list of str: parameters for the model.
    BC, bool: flag for periodic boundary conditions.
    eigs, list of floats: the eigenvalues of the Hamiltonian.
    timestamp, bool: flag for adding timestamp at the end of the file.

    """
    utils.check_dir("data")

    filename = get_filename(model, sites, pars, BC, name="evals", timestamp=timestamp, format=format)[0]

    if format==".dat":
        eigs_list=eigs.tolist()
        with open(filename, 'w') as f:
            for line in eigs_list:
                f.write(str(line))
                f.write('\n')
    elif format==".npy":
        np.save(filename, eigs)
    
    return
######################################################################################

######################################################################################
def save_eigenvectors(model, 
                      sites, 
                      pars, 
                      BC, 
                      evecs, 
                      timestamp=False, 
                      format=".npy"):
    """
    Saves the eigenvalues in a .dat file.

    Parameters:
    -----------
    Ham, instance of Hamiltonian class: the (possibly sparse) Hamiltonian.
    evecs, list of list of floats: the eigenvectors of the Hamiltonian.
    timestamp, bool: flag for adding timestamp at the end of the file.

    """

    utils.check_dir("data")
    filename = get_filename(model, sites, pars, BC, name="evecs", timestamp=timestamp, format=format)[0]

    if format==".dat":
        # Different eigenvectors will be separated by an empty line in the file.
        evecs_list=evecs.T.tolist() #the transpose is needed to save the entries in the correct order in the file
        #print(evecs)
        with open(filename, 'w') as f:
            for line in evecs_list:
                for item in line:
                    f.write(str(item))
                    f.write('\n')
                f.write('\n')
    elif format==".npy":
        np.save(filename, evecs)

    return
######################################################################################

######################################################################################
def save_obs(model, 
             sites, 
             pars, 
             BC, 
             obs_name,
             obs_value,
             timestamp=False,
             format=".npy"):
    """
    Saves the observable in a .npy or .dat file.

    Parameters:
    -----------
    model, str: name of the model.
    sites, int: number of sites L in the system.
    pars, list of str: parameters for the model.
    BC, bool: flag for periodic boundary conditions.
    obs_name, str: the name of the observable.

    """

    utils.check_dir("data")
    filename = get_filename(model=model, 
                            sites=sites, 
                            pars=pars, 
                            BC=BC, 
                            name=obs_name,
                            timestamp=timestamp)[0]

    if format==".dat":
        with open(filename, 'w') as f:
            f.write(str(obs_value))
    elif format==".npy":
        np.save(filename, obs_value)

    return
######################################################################################


######################################################################################
######################################################################################
########## LOADING DATA
######################################################################################
######################################################################################

# This is very spaghetti. TODO: modularize it / break it down further. Also recycle many of the repeated bits.
#
######################################################################################
def load_initial_state_dynamics(Ham, input, evecs):
    """
    Loads the initial state for the system dynamics based on input and/or self-consistent calculations.
    
    Parameters:
    -----------
    Ham, object of class Hamiltonian: the Hamiltonian and its properties.

    input, object of class Input: the input parameters.

    evecs, numpy array: the eigenvectors obtained from exact diagonalization.

    
    Returns
    -------
    initial_state_vec, numpy array: the initial state as vector in the sector Hilbert space basis.


    References
    ----------

    Notes
    -----

    Examples
    --------

    """
    t1 = time.time()

    if input.stats=="spinful-fermions":
        raise NotImplementedError("Initial state selection for spinful fermions not yet implemented!")
    if Ham.sector_dim is None:
        raise NotImplementedError("Initial state selection for all Hilbert space sectors not yet implemented!")

    # Determine whether system has a bath:
    if input.model=="disordered-XXZ-bath":
        bath = True
    else:
        bath = False

    # WITHOUT BATH:
    if not bath:

        # Create the initial state for the system
        print("====================================")
        print("The initial state does not include a bath! ") 

        if input.initial_state_mode_sys=="list":
            
            print("Initial state given as a (density) list.") 
            init_state_lst = input.initial_state_list_sys
            utils.initial_state_checks(stats=input.stats, 
                               bath=bath, 
                               mode_sys=input.initial_state_mode_sys, 
                               mode_bath=input.initial_state_mode_bath, 
                               state=init_state_lst,
                               N=Ham.pars["N"])
            initial_state_vec = utils.convert_state_from_list_to_vec(state_list=init_state_lst,
                                                                     stats=input.stats,
                                                                     basis_dim=Ham.sector_dim,
                                                                     sector=Ham.sector)
            
        elif input.initial_state_mode_sys=="file":

            init_state_filename = input.initial_state_file_sys
            print(f"Initial state given in file {init_state_filename}.") 
            initial_state_vec = load_initial_state_from_file(filename=init_state_filename)
            utils.initial_state_checks(stats=input.stats, 
                               bath=bath, 
                               mode_sys=input.initial_state_mode_sys, 
                               mode_bath=input.initial_state_mode_bath, 
                               state=initial_state_vec,
                               N=Ham.pars["N"])

        elif input.initial_state_mode_sys=="self":
            print(f"The initial state will be generated self-consistently.")

            sys_eigenstate = input.sys_eigenstate
            print(f"The initial state is the {sys_eigenstate}-th eigenstate.")
            initial_state_vec = evecs[:,sys_eigenstate]
 
        else:
            raise NotImplementedError("Initial state mode not recognized!")

    # WITH BATH
    else:

        # Create the initial state as a combination of system and bath.
        print("====================================")
        print("The initial state includes a bath! ") 
        

        #############################################################################
        # SYSTEM INPUT: LIST
        #############################################################################
        if input.initial_state_mode_sys=="list":
            print("Initial states for system given as a (density) list.") 
            init_state_lst_sys = input.initial_state_list_sys

            if input.initial_state_mode_bath=="list":

                print("Initial states for bath given as a (density) list.")
                init_state_lst_bath = input.initial_state_list_bath
                utils.initial_state_checks(stats=input.stats, 
                                           bath=bath, 
                                           mode_sys=input.initial_state_mode_sys,
                                           mode_bath=input.initial_state_mode_bath, 
                                           state=init_state_lst_sys,
                                           Ham=Ham,
                                           state_bath=init_state_lst_bath,
                                           L_bath=Ham.sites - Ham.pars["LA"])

                # Join lists:
                init_state_lst = init_state_lst_bath + init_state_lst_sys
                #print(init_state_lst)
                initial_state_vec = utils.convert_state_from_list_to_vec(state_list=init_state_lst,
                                                                        stats=input.stats,
                                                                        basis_dim=Ham.sector_dim,
                                                                        sector=Ham.sector)
        
            elif input.initial_state_mode_bath=="file" or input.initial_state_mode_bath=="self":

                if input.initial_state_mode_bath=="file":
                    init_state_filename_bath = input.initial_state_file_bath
                    print(f"Initial state for bath given in file {init_state_filename_bath}.")
                    
                    init_state_vec_bath = load_initial_state_from_file(filename=init_state_filename_bath)
                    utils.initial_state_checks(stats=input.stats, 
                                               bath=bath, 
                                               mode_sys=input.initial_state_mode_sys,
                                               mode_bath=input.initial_state_mode_bath,
                                               state=init_state_lst_sys,
                                               Ham=Ham,
                                               state_bath=init_state_vec_bath,
                                               L_bath=Ham.sites - Ham.pars["LA"])
                    
                elif input.initial_state_mode_bath=="self":
                    
                    ###################################################################
                    if Ham.model=="disordered-XXZ-bath":

                        print("Performing internal self-consistent calculation of initial bath state.")
                        print("Setting up BATH Hamiltonian...")
                        ti2 = time.time()
                        # Extract the bath part of the Hamiltonian:
                        # Copy the input and modify its parameters to recreate an input object for the bath geometry alone:
                        input_bath = copy.deepcopy(input)
                        LB = int(Ham.sites-Ham.pars["LA"])
                        input_bath.sites = LB
                        input_bath.Ham_par2 = input.Ham_par3 # Using the bath coupling DeltaB for the "new DeltaA" of the bath
                        input_bath.Ham_par5 = [input.Ham_par5[1], None]
                        input_bath.Ham_par6 = Ham.potential[int(Ham.pars["LA"]):]
                        input_bath.Ham_par7 = LB
                        input_bath.Ham_par8 = int(Ham.sector)   # keep the same sector for all calculations
                        Nup_bath = int(int(LB/2.0) + Ham.sector)
                        input_bath.n_lowest_eigenvalues = utils.binomial(n=LB, k=Nup_bath)
                        Ham_bath = hams.Hamiltonian(input=input_bath) 
                        tf2 = time.time()
                        print(f"Time needed to set up BATH Hamiltonian: {utils.get_time_diff(ti2,tf2)}")
                        print("\n")
                        print(f"Model: {Ham_bath.model}")   
                        print(f"Type of particles: {Ham_bath.stats}")   
                        print("\n") 
                        print("Parameters of the BATH Hamiltonian:")
                        print("Sites:", Ham_bath.sites)
                        print("Hilbert space dimension:", Ham_bath.Hilbert_dim)
                        print("Periodic boundary conditions?", Ham_bath.BC)
                        # Ham_par1:
                        print("x and y coupling J:", Ham_bath.pars["J"])
                        # Ham_par2:
                        # This is a bit confusing as the bath Hamiltonian is constructed from the same
                        # routine to construct the full Hamiltonian but only has a region A (which is actually region B),
                        # so all the "A" parameters like DeltaA actually refer to region B!
                        print("Multiplicative z coupling for region B (bath only has region B!):", Ham_bath.pars["DeltaA"])
                        # Ham_par3:
                        #print("Multiplicative z coupling for region B:", Ham_bath.pars["DeltaB"])
                        # Ham_par4:
                        #print("Multiplicative z coupling for region AB:", Ham_bath.pars["DeltaAB"])
                        # Ham_par5:
                        print("Disorder strength in region B:", Ham_bath.pars["Wmax"][0])
                        # Ham_par6:
                        print("Custom-given magnetic field:", Ham_bath.pars["W"])
                        print("Magnetic field:", Ham_bath.potential)
                        # Ham_par7: (note that since we recycle the same function to assemble the bath Hamiltonian, we need to pass the parameter LA and to be the full length of the system)            
                        print("Length of region B (bath has only region B!):", Ham_bath.pars["LA"])
                        # Ham_par8:            
                        print("Total spin z-component:", Ham_bath.pars["sz"])   
                        print("Sparse encoding?", Ham_bath.sparse)
                        print("\n") 
                        if input.print_Hamiltonian:
                            Hprint = sp.sparse.csr_matrix((Ham_bath.mat_els, (Ham_bath.rows, Ham_bath.cols)))
                            print("Bath Hamiltonian:")
                            print(Hprint.todense())
                            print("\n") 
                    
                        # Visualize Hamiltonian matrix:
                        if input.plot_Hamiltonian:
                            viz.visualize_hamiltonian(Ham_bath)
                        else:
                            print("BATH Hamiltonian visualization skipped.")
                            print("\n") 
                        ######### DIAGONALIZATION
                        print("Performing BATH diagonalization...") 
                        ti = time.time()
                        _, eigenvectors_bath = diag.diagonalize(Ham=Ham_bath, input=input_bath)
                        print(eigenvectors_bath)
                        tf = time.time()
                        print(f"Time needed to perform BATH diagonalization: {utils.get_time_diff(ti,tf)}")
                        print("\n")
                        init_state_vec_bath = eigenvectors_bath[:,input.bath_eigenstate]

                    else:
                        raise NotImplementedError("Model not recognized/incompatible with bath initial state generation!")
                    ###################################################################

                print("The bath initial state is:")
                print(init_state_vec_bath) 
                print("\n")                
                    
                # Initialize initial (full) state vector and populate it with the tensor product of |system> \otimes |bath>
                initial_state_vec = np.zeros(Ham.sector_dim)
                for n, cn in enumerate(init_state_vec_bath):

                    # Get binary repr of each basis state in the bath subspace:
                    if input.stats=="spinless-fermions" or input.stats=="spin-0.5":
                        bin_bath = utils.get_binary_from_rel_rank(rank=n, sites=Ham.sites-len(init_state_lst_sys), sz=int(Ham.sector))
                        state_sys_str = ''.join(map(str, init_state_lst_sys))
                        full_state = bin_bath+state_sys_str    # Get binary rep of full state (bath+system)
                        idx_full_state = Ham.basis_states[int(Ham.sector)][full_state]['rel_rank']  # Get corresponding rel rank in the combined Hilbert space sector
                        initial_state_vec[idx_full_state] = cn      # Add the coefficient to the vector

                    elif input.stats=="bosons":
                        raise NotImplementedError("Converting relative rank to vector representation not yet implemented for bosons (unranking of weak restricted compositions)! However, one could simply generate the states using the lexicographic ordering if n is assumed to increase by one at each step.")

                        # Workaround:
                        # 1) The bath state is given as vector |b> = c0 |0> + c1 |1> + ..., where |n> are basis states in the bath subspace.
                        # 2) The system is in a basis state |s> b/c it is given as a list of densities. Call its vector representation v.
                        # 3) Their tensor product will be |psi> = c0 |0> \otimes |s> + c1 |1> \otimes |s> + ... . 
                        # 4) The idea is then to create on the fly all the basis states for the bath, i.e. list all the states |0>, |1> etc. in order.
                        # 5) For the actual realization, do the following. 
                        #   Loop over all bath states |n> in the basis B:
                        #   - a) Create the vector representation of |n>. Call it w.
                        #   - b) Merge w+v to get vector representation over the full system (i.e. system+bath).
                        #   - c) Search for w+v in Ham.basis_state like above for fermions. Get the corresponding index of the larger dimensional vector (system+bath).
                        #   - d) Set the coefficient of this vector to be cn.
                        #
                        # Things might get slightly trickier if we need to cap the occupation, but the principle is the same. The important thing in the end is self-consistencty between bath and system.

            else:
                raise NotImplementedError("Initial state mode not recognized!")
        #############################################################################
       

        #############################################################################
        # SYSTEM INPUT: FILE / SELF
        #############################################################################
        elif input.initial_state_mode_sys=="file" or input.initial_state_mode_sys=="self":
            
            if input.initial_state_mode_sys=="file":
                init_state_filename_sys = input.initial_state_file_sys
                print(f"Initial state for system given in file {init_state_filename_sys}.") 
                init_state_vec_sys = load_initial_state_from_file(filename=init_state_filename_sys)

            elif input.initial_state_mode_sys=="self":
                print(f"The initial state will be generated self-consistently.")

                if Ham.model=="disordered-XXZ-bath":

                    # Redo the calculation of the eigenstate for the subsystem A only:
                    print("Performing internal self-consistent calculation of initial subsystem A state.")
                    print("Setting up SUBSYTEM A Hamiltonian...")
                    ti2 = time.time()
                    # Extract the system A part of the Hamiltonian:
                    # Copy the input and modify its parameters to recreate an input object for the bath geometry alone:
                    input_A = copy.deepcopy(input)
                    input_A.sites = Ham.pars["LA"]
                    input_A.Ham_par5 = [input.Ham_par5[0], None]
                    input_A.Ham_par6 = Ham.potential[:int(Ham.pars["LA"])]
                    input_A.Ham_par7 = Ham.pars["LA"]
                    input_A.Ham_par8 = int(Ham.sector)   # keep the same sector for all calculations
                    Nup_A = int(int(Ham.pars["LA"]/2.0) + Ham.sector)
                    input_A.n_lowest_eigenvalues = utils.binomial(n=Ham.pars["LA"], k=Nup_A)
                    Ham_A = hams.Hamiltonian(input=input_A) 
                    tf2 = time.time()
                    print(f"Time needed to set up SUBSYSTEM A Hamiltonian: {utils.get_time_diff(ti2,tf2)}")
                    print("\n")
                    print(f"Model: {Ham_A.model}")   
                    print(f"Type of particles: {Ham_A.stats}")   
                    print("\n") 
                    print("Parameters of the SUBSYSTEM A Hamiltonian:")
                    print("Sites:", Ham_A.sites)
                    print("Hilbert space dimension:", Ham_A.Hilbert_dim)
                    print("Periodic boundary conditions?", Ham_A.BC)
                    # Ham_par1:
                    print("x and y coupling J:", Ham_A.pars["J"])
                    # Ham_par2:
                    print("Multiplicative z coupling for region A (SUBSYSTEM only has region A!):", Ham_A.pars["DeltaA"])
                    # Ham_par3:
                    #print("Multiplicative z coupling for region B:", Ham_A.pars["DeltaB"])
                    # Ham_par4:
                    #print("Multiplicative z coupling for region AB:", Ham_A.pars["DeltaAB"])
                    # Ham_par5:
                    print("Disorder strength in region A:", Ham_A.pars["Wmax"][0])
                    # Ham_par6:
                    print("Custom-given magnetic field:", Ham_A.pars["W"])
                    print("Magnetic field:", Ham_A.potential)
                    # Ham_par7: (note that since we recycle the same function to assemble the bath Hamiltonian, we need to pass the parameter LA and to be the full length of the system)            
                    print("Length of region A (subsytem has only region A!):", Ham_A.pars["LA"])
                    # Ham_par8:            
                    print("Total spin z-component:", Ham_A.pars["sz"])   
                    print("Sparse encoding?", Ham_A.sparse)
                    print("\n") 

                if input.print_Hamiltonian:
                    Hprint = sp.sparse.csr_matrix((Ham_A.mat_els, (Ham_A.rows, Ham_A.cols)))
                    print("Subsystem A Hamiltonian:")
                    print(Hprint.todense())
                    print("\n") 
                # Visualize Hamiltonian matrix:
                if input.plot_Hamiltonian:
                    viz.visualize_hamiltonian(Ham_A)
                else:
                    print("Subsystem A Hamiltonian visualization skipped.")
                    print("\n") 
                ######### DIAGONALIZATION
                print("Performing SUBSYSTEM A diagonalization...") 
                ti = time.time()
                _, eigenvectors_A = diag.diagonalize(Ham=Ham_A, input=input_A)
                #print(eigenvectors_A)
                tf = time.time()
                print(f"Time needed to perform SUBSYSTEM A diagonalization: {utils.get_time_diff(ti,tf)}")
                print("\n")
                init_state_vec_sys = eigenvectors_A[:,input.sys_eigenstate]
                ###################################################################

                print(f"The initial state of the SUBSYSTEM A is the {input.sys_eigenstate}-th eigenstate: {init_state_vec_sys}")
            
            # BATH CASES  
            if input.initial_state_mode_bath=="list":
                    
                # get vector from list for bath:
                print("Initial state for bath given as a (density) list.") 
                init_state_lst_bath = input.initial_state_list_bath
                utils.initial_state_checks(stats=input.stats, 
                                               bath=bath, 
                                               mode_sys=input.initial_state_mode_sys,
                                               mode_bath=input.initial_state_mode_bath, 
                                               state=init_state_vec_sys,
                                               Ham=Ham,
                                               state_bath=init_state_lst_bath,
                                               L_bath=Ham.sites - Ham.pars["LA"])
                    
                # Initialize initial (full) state vector and populate it with the tensor product of |system> \otimes |bath>
                initial_state_vec = np.zeros(Ham.sector_dim)
                for n, cn in enumerate(init_state_vec_sys):

                    # Get binary repr of each basis state in the bath subspace:
                    if input.stats=="spinless-fermions" or input.stats=="spin-0.5":
                        bin_sys = utils.get_binary_from_rel_rank(rank=n, sites=Ham.sites-len(init_state_lst_bath), sz=int(Ham.sector))
                        state_bath_str = ''.join(map(str, init_state_lst_bath))
                        full_state = state_bath_str+bin_sys    # Get binary rep of full state (bath+system)
                        #print(full_state)
                        idx_full_state = Ham.basis_states[int(Ham.sector)][full_state]['rel_rank']  # Get corresponding rel rank in the combined Hilbert space sector
                        initial_state_vec[idx_full_state] = cn      # Add the coefficient to the vector

                    elif input.stats=="bosons":
                        raise NotImplementedError("Converting relative rank to vector representation not yet implemented for bosons (unranking of weak restricted compositions)! However, one could simply generate the states using the lexicographic ordering if n is assumed to increase by one at each step.")
      
            elif input.initial_state_mode_bath=="file" or input.initial_state_mode_bath=="self":

                if input.initial_state_mode_bath=="file":
                    init_state_filename_bath = input.initial_state_file_bath
                    print(f"Initial state for system given in file {init_state_filename_bath}.") 
                    init_state_vec_bath = load_initial_state_from_file(filename=init_state_filename_bath)

                    utils.initial_state_checks(stats=input.stats,
                                                   bath=bath,
                                                   mode_sys=input.initial_state_mode_sys,
                                                   mode_bath=input.initial_state_mode_bath,
                                                   state=init_state_vec_sys,
                                                   Ham=Ham,
                                                   state_bath=init_state_vec_bath,
                                                   L_bath=Ham.sites - Ham.pars["LA"])
                        
                elif input.initial_state_mode_bath=="self":

                    ###################################################################
                    if Ham.model=="disordered-XXZ-bath":

                        print("Performing internal self-consistent calculation of initial bath state.")
                        print("Setting up BATH Hamiltonian...")
                        ti2 = time.time()
                        # Extract the bath part of the Hamiltonian:
                        # Copy the input and modify its parameters to recreate an input object for the bath geometry alone:
                        input_bath = copy.deepcopy(input)
                        LB = int(Ham.sites-Ham.pars["LA"])
                        input_bath.sites = LB
                        input_bath.Ham_par5 = [input.Ham_par5[1], None]
                        input_bath.Ham_par6 = Ham.potential[int(Ham.pars["LA"]):]
                        input_bath.Ham_par7 = LB
                        input_bath.Ham_par8 = int(Ham.sector)   # keep the same sector for all calculations
                        Nup_bath = int(int(LB/2.0) + Ham.sector)
                        input_bath.n_lowest_eigenvalues = utils.binomial(n=LB, k=Nup_bath)
                        Ham_bath = hams.Hamiltonian(input=input_bath) 
                        tf2 = time.time()
                        print(f"Time needed to set up BATH Hamiltonian: {utils.get_time_diff(ti2,tf2)}")
                        print("\n")
                        print(f"Model: {Ham_bath.model}")   
                        print(f"Type of particles: {Ham_bath.stats}")   
                        print("\n") 
                        print("Parameters of the BATH Hamiltonian:")
                        print("Sites:", Ham_bath.sites)
                        print("Hilbert space dimension:", Ham_bath.Hilbert_dim)
                        print("Periodic boundary conditions?", Ham_bath.BC)
                        # Ham_par1:
                        print("x and y coupling J:", Ham_bath.pars["J"])
                        # Ham_par2:
                        print("Multiplicative z coupling for region A:", Ham_bath.pars["DeltaA"])
                        # Ham_par3:
                        print("Multiplicative z coupling for region B:", Ham_bath.pars["DeltaB"])
                        # Ham_par4:
                        print("Multiplicative z coupling for region AB:", Ham_bath.pars["DeltaAB"])
                        # Ham_par5:
                        print("Disorder strength in regions A and B:", Ham_bath.pars["Wmax"])
                        # Ham_par6:
                        print("Custom-given magnetic field:", Ham_bath.pars["W"])
                        print("Magnetic field:", Ham_bath.potential)
                        # Ham_par7: (note that since we recycle the same function to assemble the bath Hamiltonian, we need to pass the parameter LA and to be the full length of the system)            
                        print("Length of region B (bath has only region B!):", Ham_bath.pars["LA"])
                        # Ham_par8:            
                        print("Total spin z-component:", Ham_bath.pars["sz"])   
                        print("Sparse encoding?", Ham_bath.sparse)
                        print("\n") 
                        if input.print_Hamiltonian:
                            Hprint = sp.sparse.csr_matrix((Ham_bath.mat_els, (Ham_bath.rows, Ham_bath.cols)))
                            print("Bath Hamiltonian:")
                            print(Hprint.todense())
                            print("\n") 
                    
                        # Visualize Hamiltonian matrix:
                        if input.plot_Hamiltonian:
                            viz.visualize_hamiltonian(Ham_bath)
                        else:
                            print("BATH Hamiltonian visualization skipped.")
                            print("\n") 
                        ######### DIAGONALIZATION
                        print("Performing BATH diagonalization...") 
                        ti = time.time()
                        _, eigenvectors_bath = diag.diagonalize(Ham=Ham_bath, input=input_bath)
                        print(eigenvectors_bath)
                        tf = time.time()
                        print(f"Time needed to perform BATH diagonalization: {utils.get_time_diff(ti,tf)}")
                        print("\n")
                        init_state_vec_bath = eigenvectors_bath[:,input.bath_eigenstate]

                    else:
                        raise NotImplementedError("Model not recognized/incompatible with bath initial state generation!")
                    ###################################################################

                else:
                    raise NotImplementedError("Initial BATH state mode not recognized!")
            
                print("The bath initial state is:")
                print(init_state_vec_bath) 
                print("\n")   

                # Initialize initial (full) state vector and populate it with the tensor product of |system> \otimes |bath>
                initial_state_vec = np.zeros(Ham.sector_dim)
                for n, cn in enumerate(init_state_vec_sys):
                    for m, cm in enumerate(init_state_vec_bath):

                        # Get binary repr of each basis state in the bath subspace:
                        if input.stats=="spinless-fermions" or input.stats=="spin-0.5":
                            #print(int(input.sites-input.L_bath))
                            #print(input.sites)
                            bin_sys = utils.get_binary_from_rel_rank(rank=n, sites=int(input.sites-input.L_bath), sz=int(Ham.sector))
                            bin_bath = utils.get_binary_from_rel_rank(rank=m, sites=input.L_bath, sz=int(Ham.sector))
                            full_state = bin_bath+bin_sys    # Get binary rep of full state (bath+system)
                            #print("bin_sys:", bin_sys)
                            #print("bin_bath:", bin_bath)
                            #print("full_state:", full_state)
                            idx_full_state = Ham.basis_states[int(Ham.sector)][full_state]['rel_rank']  # Get corresponding rel rank in the combined Hilbert space sector
                            initial_state_vec[idx_full_state] = cn*cm      # Add the coefficient to the vector

                        elif input.stats=="bosons":
                            raise NotImplementedError("Converting relative rank to vector representation not yet implemented for bosons (unranking of weak restricted compositions)! However, one could simply generate the states using the lexicographic ordering if n is assumed to increase by one at each step.")

        else:
            raise NotImplementedError("Initial state mode not recognized!")
        #############################################################################

    print(f"The initial state is: {initial_state_vec}")
    return initial_state_vec

######################################################################################



######################################################################################
def load_initial_state_dynamics_old(Ham, input):
    """
    Loads the initial state for the system dynamics based on input and/or self-consistent calculations.
    
    Parameters:
    -----------
    Ham, object of class Hamiltonian: the Hamiltonian and its properties.

    input, object of class Input: the input parameters.

    
    Returns
    -------
    initial_state_vec, numpy array: the initial state as vector in the sector Hilbert space basis.


    References
    ----------

    Notes
    -----

    Examples
    --------

    """

    t1 = time.time()
    # Create the initial state as a combination of system and bath.
    if input.bath_initial_state is not None:
        print("====================================")
        print("The initial state includes a bath! ") 
        
    
        if Ham.model=="disordered-XXZ-bath":

            # Get binary representation for the initial state of the system
            LA = int(Ham.pars["LA"])
            initial_bin_system = input.initial_state[-LA:]   #The initial input state will be something like "1010101bbbb", where b is a string/placeholder reserved for the bath. This notation is needed for back-compatibility.
            print(f"The system initial state is: {initial_bin_system}")
            # Get the bath initial state:
            ###################################################################
            if input.custom_bath_initial_state:   # Initial bath state provided at input
                print("Initial bath state provided at input.") 
                bath_initial_state = input.bath_initial_state
            else:
                print("Performing internal self-consistent calculation of initial bath state.")
                print("Setting up BATH Hamiltonian...")
                ti = time.time()
                # Extract the bath part of the Hamiltonian:
                # Copy the input and modify its parameters to recreate an input object for the bath geometry alone:
                input_bath = input
                LB = int(Ham.sites-Ham.pars["LA"])
                input_bath.sites = LB
                input_bath.Ham_par5 = [input.Ham_par5[1], None]
                input_bath.Ham_par6 = Ham.potential[int(Ham.pars["LA"]):]
                input_bath.Ham_par7 = LB
                input_bath.Ham_par8 = int(Ham.sector)   # keep the same sector for all calculations
                Nup_bath = int(int(LB/2.0) + Ham.sector)
                input_bath.n_lowest_eigenvalues = utils.binomial(n=LB, k=Nup_bath)
                Ham_bath = hams.Hamiltonian(input=input) 
                tf = time.time()
                print(f"Time needed to set up BATH Hamiltonian: {utils.get_time_diff(ti,tf)}")
                print("\n")
                print(f"Model: {Ham_bath.model}")   
                print(f"Type of particles: {Ham_bath.stats}")   
                print("\n") 
                print("Parameters of the BATH Hamiltonian:")
                print("Sites:", Ham_bath.sites)
                print("Hilbert space dimension:", Ham_bath.Hilbert_dim)
                print("Periodic boundary conditions?", Ham_bath.BC)
                # Ham_par1:
                print("x and y coupling J:", Ham_bath.pars["J"])
                # Ham_par2:
                print("Multiplicative z coupling for region A:", Ham_bath.pars["DeltaA"])
                # Ham_par3:
                print("Multiplicative z coupling for region B:", Ham_bath.pars["DeltaB"])
                # Ham_par4:
                print("Multiplicative z coupling for region AB:", Ham_bath.pars["DeltaAB"])
                # Ham_par5:
                print("Disorder strength in regions A and B:", Ham_bath.pars["Wmax"])
                # Ham_par6:
                print("Custom-given magnetic field:", Ham_bath.pars["W"])
                print("Magnetic field:", Ham_bath.potential)
                # Ham_par7: (note that since we recycle the same function to assemble the bath Hamiltonian, we need to pass the parameter LA and to be the full length of the system)            
                print("Length of region B (bath has only region B!):", Ham_bath.pars["LA"])
                # Ham_par8:            
                print("Total spin z-component:", Ham_bath.pars["sz"])   
                print("Sparse encoding?", Ham_bath.sparse)
                print("\n") 
                if input.print_Hamiltonian:
                    Hprint = sp.sparse.csr_matrix((Ham_bath.mat_els, (Ham_bath.rows, Ham_bath.cols)))
                    print("Bath Hamiltonian:")
                    print(Hprint.todense())
                    print("\n") 
                # Visualize Hamiltonian matrix:
                if input.plot_Hamiltonian:
                    viz.visualize_hamiltonian(Ham_bath)
                else:
                    print("BATH Hamiltonian visualization skipped.")
                    print("\n") 
                ######### DIAGONALIZATION
                print("Performing BATH diagonalization...") 
                ti = time.time()
                _, eigenvectors_bath = diag.diagonalize(Ham=Ham_bath, input=input_bath)
                print(eigenvectors_bath)
                tf = time.time()
                print(f"Time needed to perform BATH diagonalization: {utils.get_time_diff(ti,tf)}")
                print("\n")
                bath_initial_state = eigenvectors_bath[:,input.bath_eigenstate]
            ###################################################################
            print("The bath initial state is:")
            print(bath_initial_state) 
            print("\n")
            # Initialize initial (full) state vector and populate it with the tensor product of |system> \otimes |bath>
            initial_state_vec = np.zeros(Ham.sector_dim)
            for n, cn in enumerate(bath_initial_state):
                # Get binary repr of each basis state in the bath subspace:
                bin_bath = utils.get_binary_from_rel_rank(rank=n, sites=Ham.sites-LA, sz=int(Ham.sector))
                full_state = bin_bath+initial_bin_system    # Get binary rep of full state (bath+system)
                idx_full_state = Ham.basis_states[int(Ham.sector)][full_state]['rel_rank']  # Get corresponding rel rank in the combined Hilbert space sector
                initial_state_vec[idx_full_state] = cn      # Add the coefficient to the vector

    # The state does not contain a bath part.
    else:
        print("====================================")
        print("The initial state does NOT include a bath! ") 
        print("\n")

        print(input.initial_state)
        print(Ham.sector_dim)
        print(Ham.sector)
        # Convert initial state to a vector in the Hilbert subspace of the sector:
        # e.g. |1011> -> (0,0,1,0)
        # because the (lexicographically ordered) basis is:
        # B = [ |1110>, |1101>, |1011>, |0111> ]
        initial_state_vec = utils.convert_state_from_bin_to_vec(state_str=input.initial_state,
                                                                basis_dim=Ham.sector_dim, 
                                                                sector=Ham.sector)
        

    print("The full initial state is:")
    print(initial_state_vec)
    print("\n")
    t2 = time.time()
    print(f"Time needed to set up initial state: {utils.get_time_diff(t1,t2)}")

    return initial_state_vec

######################################################################################

######################################################################################
def load_eigenvalues(model, 
                     sites, 
                     pars, 
                     BC, 
                     timestamp=False,
                     format=".npy"):
    """
    Saves the eigenvalues in a .dat file.

    Parameters:
    -----------
    eigs, list of floats: the eigenvalues of the Hamiltonian.
    timestamp, bool: flag for adding timestamp at the end of the file.

    """

    filename = get_filename(model, sites, pars, BC, name="evals", timestamp=timestamp, format=format)

    if format==".dat":
        eigs = []
        with open(filename) as file:
            for line in file:
                eigs.append(float(line.rstrip()))
    elif format==".npy":
        eigs = np.load(filename)

    return eigs
######################################################################################

######################################################################################
def load_obs(model, 
             sites, 
             pars, 
             BC, 
             obs_name,
             **kwargs):
    
    format=kwargs.get("format", ".npy")
    averaging_method=kwargs.get("averaging_method", None)
    verbose=kwargs.get("verbose", False)

    if averaging_method is None:
        filename = get_filename(model=model, 
                                sites=sites, 
                                pars=pars, 
                                BC=BC, 
                                name=obs_name,
                                **kwargs)[0]

        if format==".dat":
            with open(filename) as file:
                for line in file:
                    obs=float(line.rstrip())
        elif format==".npy":
            obs = np.load(filename)

    else:   # Averaging over many instances of the same variable
        obs_list = []
        filenames = get_filename(model=model, 
                                sites=sites, 
                                pars=pars, 
                                BC=BC, 
                                name=obs_name,
                                **kwargs)
        if filenames == []:
            raise FileNotFoundError("I didn't find any file matching the requested filenames. Please check that you have save the files correctly.")
        for filename in filenames:
            
            if verbose:
                print(f"Extracting data from {filename}")
            if format==".dat":
                with open(filename) as file:
                    for line in file:
                        obs_list.append(float(line.rstrip()))
            elif format==".npy":
                obs_list.append(np.load(filename))

        obs=utils.compute_average(obs_list=obs_list,
                                  averaging_method=averaging_method)        


    return obs
######################################################################################

######################################################################################
def load_obs_new(model, 
             sites, 
             pars, 
             BC, 
             obs_name,
             **kwargs):
    
    format = kwargs.get("format", ".npy")
    averaging_method = kwargs.get("averaging_method", None)
    verbose = kwargs.get("verbose", False)

    if averaging_method is None:
        filename = get_filename(model=model, 
                                sites=sites, 
                                pars=pars, 
                                BC=BC, 
                                name=obs_name,
                                **kwargs)[0]

        if format == ".dat":
            with open(filename) as file:
                for line in file:
                    obs = float(line.rstrip())
        elif format == ".npy":
            obs = np.load(filename, allow_pickle=True)

    else:  # Averaging over many instances of the same variable
        all_obs_lists = []  # Store lists of arrays from all files
        filenames = get_filename(model=model, 
                                  sites=sites, 
                                  pars=pars, 
                                  BC=BC, 
                                  name=obs_name,
                                  **kwargs)
        if not filenames:
            raise FileNotFoundError(
                "I didn't find any file matching the requested filenames. Please check that you have saved the files correctly."
            )
        
        #print(filenames)

        for filename in filenames:
            if verbose:
                print(f"Extracting data from {filename}")
            if format==".dat":
                with open(filename) as file:
                    for line in file:
                        obs=float(line.rstrip())
            elif format == ".npy":
                file_data = np.load(filename, allow_pickle=True)
                if isinstance(file_data, list) or isinstance(file_data, np.ndarray):
                    all_obs_lists.append(file_data)
                elif np.isscalar(file_data):
                    all_obs_lists.append([file_data])  # Wrap single data as a one-element list
                else:
                    raise ValueError(f"Unexpected data type in {filename}: {type(file_data)}.")

        #print(np.shape(all_obs_lists))
        # Check that all files have the same list length
        list_lengths = [len(data) for data in all_obs_lists]
        if len(set(list_lengths)) > 1:
            raise ValueError("Files contain lists of different lengths. Ensure all files have the same number of elements in the lists.")

        # Prepare for element-wise stacking and averaging
        num_entries = list_lengths[0]
        obs = []  # Store the averaged entries
        for i in range(num_entries):  # Iterate over list indices
            data_to_average = [data[i] for data in all_obs_lists]  # Collect i-th entries across all realizations. This will be trivial (1) for scalar data.
            
            # Handle scalars, arrays, or higher-dimensional data automatically in compute_average
            avg_data = utils.compute_average(obs_list=data_to_average, averaging_method=averaging_method)
            obs.append(avg_data)  # Append the averaged data for the i-th entry

    return obs

######################################################################################

######################################################################################
def load_initial_state_from_file(filename, **kwargs):
    """
    Notes:
    ------
    For .dat files, it is assumed that there is one entry per line (the weight of the state for a given basis state index), e.g.
    the vector [-0.16833063, 0.63679473, -0.42309096, -0.38430585, 0.47262106, -0.126845] for a 4-site system at half filling 
    will be saved as
    ---file begins on the line below---
    -0.16833063
    0.63679473
    -0.42309096
    -0.38430585
    0.47262106
    -0.126845
    ---file ends on line above---

    Right now it is assumed that the entries are real numbers,
    TODO: extend this to complex entries.

    """

    if ".npy" in filename:
        file_format=".npy"
    elif ".dat" in filename:
        file_format=".dat"
    else:
        file_format="unknown"

    if file_format==".dat":
        state = []
        with open(filename) as file:
            for line in file:
                state.append(float(line.rstrip()))
        state = np.array(state)
    elif file_format==".npy":
        state = np.load(filename)
    else:
        raise NotImplementedError("Unknown file format for initial state!")

    return state
######################################################################################

######################################################################################
######################################################################################
########## CHANGING STRINGS
######################################################################################
######################################################################################

######################################################################################
def get_filename(model, 
                 sites, 
                 pars, 
                 BC, 
                 name, 
                 **kwargs):

    format=kwargs.get("format", ".npy")
    averaging_method=kwargs.get("averaging_method", None)
    if averaging_method is not None:
        averaging=True
    else:
        averaging=False
    timestamp=kwargs.get("timestamp", False)

    par_keys, par_vals = utils.get_parameters_from_dict(pars)

    filename="{0}-model-{1}-sites-{2}-{3}-pars".format(name, model, sites, BC)
    #TODO: this is repeated in viz.py --> unify

    for i in range(len(pars)):
        filename=filename+"-{0}-{1}".format(par_keys[i],par_vals[i])

    if averaging:
        if format==".dat":
            filename=filename+"*.dat"
        elif format==".npy":
            filename=filename+"*.npy"
        else:
            raise FileNotFoundError(f"Format type {format} not recognized.")
        
        print(filename)
        # Get all the filenames that match the given string, over which we will average.
        filenames = utils.get_matching_files(directory="data", pattern=filename)

        return filenames

    else:
        filename = "data/" + filename
        # Add timestamp, e.g. to make each correlation plot unique once we need to average over many disorder realizations.
        if timestamp:
            curr_dt = datetime.datetime.now()
            ts = int(round(curr_dt.timestamp()))
            r_int = np.random.randint(10000)
            filename = filename+"-"+str(ts)+"_"+str(r_int)        
    
        # format:
        if format==".dat":
            filename = filename + ".dat"
        elif format==".npy":
            filename = filename + ".npy"
        else:
            raise FileNotFoundError(f"Format type {format} not recognized.")
        
        return [filename]
######################################################################################


######################################################################################
def load_logo(file_path="edith_pyaf/templates/logo.txt"):

    try:
        with open(file_path, 'r', encoding='utf-8') as file:
            content = file.read()
        return content
    except FileNotFoundError:
        return f"File at {file_path} not found."
    except Exception as e:
        return f"An error occurred: {e}"

######################################################################################


######################################################################################
######################################################################################
########## MODIFYING INPUTS
######################################################################################
######################################################################################

######################################################################################
def modify_input(input_file, target, value):
    """
    Searches for the string "target" and modifies with "value".

    Notes:
    ------
    The target has to always end with an equal sign "=", e.g.:
    "Ham_par1="
    This is to avoid potential duplicates (Ham_par1 and Ham_par10 would both match the string "Ham_par1").
    """
    is_str=False
    if type(value) is str:
        is_str=True

    # Convert everything to strings to be safe:
    target = str(target)
    value = str(value)

    # Read in input file and corresponding lines:
    with open(input_file, 'r', encoding='utf-8') as file:
        lines = file.readlines()

    # Search for target line and modify it with new value:
    for i, line in enumerate(lines):
        if target in line:
            if is_str:
                value="\""+value+"\""
            line = target+value+"\n"
            lines[i] = line

    with open(input_file, 'w', encoding='utf-8') as file:
        file.writelines(lines)
    
    # delay to allow the file to be properly saved:
    time.sleep(1)
    
######################################################################################

