#!/usr/bin/env python
############################################################################################
##### This module calculates various observables
############################################################################################

####################################
###########  IMPORTS   #############
####################################
from __future__ import division
from __future__ import print_function
import numpy as np
from functools import partial
from scipy.optimize import curve_fit

# Custom modules:
from . import utils
from . import hams
from . import fit
####################################


#####################################################
###########  ENERGY-BASED OBSERVABLES   #############
#####################################################
########################################################################
def energy_gap(evals, i, j, kind="real"):
    """
    Compute the difference in energy between the i-th and j-th eigenstate.

    Parameters:
    -----------
    evals, list: the eigenvalues in the given sector.

    i, int: the i-th eigenvalue to compare.

    j, int: the j-th eigenvalue to compare.

    kind, str: how to compute the energy difference. Options:
                - "real": only consider the real part.
                - "imag": only consider the imaginary part.
                - "mod": use the modulus |z|=sqrt(Re(z)^2 + Im(z)^2).


    Returns
    -------
    gap, float: the calculated energy gap.


    References
    ----------

    Notes
    -----

    Examples
    --------

    """

    sorted_evals = np.sort_complex(np.array(evals))   # sort eigenvalues in case they aren't sorted.

    if kind=="real":
        gap = np.real(sorted_evals[i]) - np.real(sorted_evals[j])
    elif kind=="imag":
        gap = np.imag(sorted_evals[i]) - np.imag(sorted_evals[j])
    elif kind=="mod":
        gap = np.abs(sorted_evals[i]) - np.abs(sorted_evals[j])
    
    return gap

########################################################################

########################################################################
def DOS(evals, **kwargs):
    """
    Compute the density of states.

    Parameters:
    -----------
    evals, list: the eigenvalues in the given sector.

    **kwargs:
        - broadening, float: broadening (standard deviation) used to represent the delta function as a Gaussian.

    Returns
    -------
    dos, numpy array: the DOS in the energy interval defined by the minimal and maximal eigenenergies.


    References
    ----------

    Notes
    -----
    The function will automatically detect whether the system has complex eigenenergies and construct a complex DOS in that case.

    Examples
    --------

    """

    broadening = kwargs.get("broadening", None)
    if broadening is None:
        broadening = 0.01*np.amax([(np.max(np.real(evals)) - np.min(np.real(evals))), np.max(np.imag(evals)) - np.min(np.imag(evals))])

    # Detect whether the eigenenergies are complex:
    is_compl = (np.any(np.imag(evals) > 1e-10))

    if is_compl:    # Complex energies -> DOS on the complex plane.

        # Detect energy boundaries:
        E_min_re = np.min(np.real(evals)) - 0.01*(np.max(np.real(evals)) - np.min(np.real(evals)))
        E_max_re = np.max(np.real(evals)) + 0.01*(np.max(np.real(evals)) - np.min(np.real(evals))) 
        E_min_im = np.min(np.imag(evals)) - 0.01*(np.max(np.imag(evals)) - np.min(np.imag(evals)))
        E_max_im = np.max(np.imag(evals)) + 0.01*(np.max(np.imag(evals)) - np.min(np.imag(evals)))

        # Construct energy grid:
        E_points = 1000  # Number of points in energy grid
        E_grid_re = np.linspace(E_min_re, E_max_re, E_points)
        E_grid_im = np.linspace(E_min_im, E_max_im, E_points)
        E_real, E_imag = np.meshgrid(E_grid_re, E_grid_im)
        E_grid = E_real + 1j * E_imag

        # Calculate DOS
        dos = np.zeros_like(E_real, dtype=float)
        for E_n in evals:
            dos += (1 / (2 * np.pi * broadening**2)) * np.exp(-((E_real - E_n.real)**2 + (E_imag - E_n.imag)**2) / (2 * broadening**2))
        
        return E_grid, dos
    
    else:   # Real energies -> DOS on the real axis.

        # Detect energy boundaries:
        E_min = np.min(np.real(evals)) - 0.01*(np.max(np.real(evals)) - np.min(np.real(evals)))
        E_max = np.max(np.real(evals)) + 0.01*(np.max(np.real(evals)) - np.min(np.real(evals))) 

        # Construct energy grid:
        E_points = 1000  # Number of points in energy grid
        E_grid = np.linspace(E_min, E_max, E_points)

        # Calculate DOS
        dos = np.zeros_like(E_grid, dtype=float)
        for E_n in evals:
            dos += (1 / np.sqrt(2 * np.pi * broadening**2)) * np.exp(-((E_grid - np.real(E_n))**2) / (2 * broadening**2))
        
        return E_grid, dos
########################################################################

#####################################################
###########   STATE-BASED OBSERVABLES   #############
#####################################################

########################################################################
def LDOS(Ham, evals, evecs, site, **kwargs):
    """
    Compute the local density of states at a given site.

    Parameters:
    -----------
    Ham, object of class Hamiltonian: the Hamiltonian and its properties.

    evals, list: the eigenvalues in the given sector.

    evecs, list: the eigenvectors in the given sector.

    site, int: the site where to calculate ldos.

    broadening, float: broadening (standard deviation) used to represent the delta function as a Gaussian.

    Returns
    -------
    ldos, numpy array: the LDOS in the energy interval defined by the minimal and maximal eigenenergies.


    References
    ----------

    Notes
    -----
    The function will automatically detect whether the system has complex eigenenergies and construct a complex LDOS in that case.

    Examples
    --------

    """

    broadening = kwargs.get("broadening", None)
    if broadening is None:
        broadening = 0.01*np.amax([(np.max(np.real(evals)) - np.min(np.real(evals))), np.max(np.imag(evals)) - np.min(np.imag(evals))])

    # Detect whether the eigenenergies are complex:
    is_compl = (np.any(np.imag(evals) > 1e-10))

    if is_compl:    # Complex energies -> LDOS on the complex plane.

        # Detect energy boundaries:
        E_min_re = np.min(np.real(evals)) - 0.01*(np.max(np.real(evals)) - np.min(np.real(evals)))
        E_max_re = np.max(np.real(evals)) + 0.01*(np.max(np.real(evals)) - np.min(np.real(evals))) 
        E_min_im = np.min(np.imag(evals)) - 0.01*(np.max(np.imag(evals)) - np.min(np.imag(evals)))
        E_max_im = np.max(np.imag(evals)) + 0.01*(np.max(np.imag(evals)) - np.min(np.imag(evals)))

        # Construct energy grid:
        E_points = 1000  # Number of points in energy grid
        E_grid_re = np.linspace(E_min_re, E_max_re, E_points)
        E_grid_im = np.linspace(E_min_im, E_max_im, E_points)
        E_real, E_imag = np.meshgrid(E_grid_re, E_grid_im)
        E_grid = E_real + 1j * E_imag

        # Calculate LDOS
        ldos = np.zeros_like(E_real, dtype=float)
        for idx, E_n in enumerate(evals):
            state = evecs[:,idx]
            if Ham.stats=="spinless-fermions":
                amp2 = hams.get_state_population_spinless_site(Ham=Ham, state=state, site=site)
            elif Ham.stats=="bosons":
                amp2 = hams.get_state_population_bosons_site(Ham=Ham, state=state, site=site)
            ldos += amp2*(1 / (2 * np.pi * broadening**2)) * np.exp(-((E_real - E_n.real)**2 + (E_imag - E_n.imag)**2) / (2 * broadening**2))
        
        return E_grid, ldos
    
    else:   # Real energies -> LDOS on the real axis.

        # Detect energy boundaries:
        E_min = np.min(np.real(evals)) - 0.01*(np.max(np.real(evals)) - np.min(np.real(evals)))
        E_max = np.max(np.real(evals)) + 0.01*(np.max(np.real(evals)) - np.min(np.real(evals))) 

        # Construct energy grid:
        E_points = 1000  # Number of points in energy grid
        E_grid = np.linspace(E_min, E_max, E_points)
 
        # Calculate LDOS
        ldos = np.zeros_like(E_grid, dtype=float)
        for idx, E_n in enumerate(evals):
            state = evecs[:,idx]
            if Ham.stats=="spinless-fermions":
                amp2 = hams.get_state_population_spinless_site(Ham=Ham, state=state, site=site)
            elif Ham.stats=="bosons":
                amp2 = hams.get_state_population_bosons_site(Ham=Ham, state=state, site=site)    
            ldos += amp2*(1 / np.sqrt(2 * np.pi * broadening**2)) * np.exp(-((E_grid - np.real(E_n))**2) / (2 * broadening**2))

        return E_grid, ldos
########################################################################

########################################################################
def LDOS_all_sites(Ham, evals, evecs, broadening):
    """
    Compute the local density of states at a given site.

    Parameters:
    -----------
    Ham, object of class Hamiltonian: the Hamiltonian and its properties.

    evals, list: the eigenvalues in the given sector.

    evecs, list: the eigenvectors in the given sector.

    broadening, float: broadening (standard deviation) used to represent the delta function as a Gaussian.

    Returns
    -------
    full_LDOS, list of numpy arrays: the LDOS in the energy interval defined by the minimal and maximal eigenenergies for all the sites.


    References
    ----------

    Notes
    -----

    Examples
    --------

    """
    if Ham.stats=="spinful-fermions" or Ham.stats=="spins-0.5":
        raise NotImplementedError("LDOS for spinful fermions and spin 0.5 not implemented!")
    
    sites = Ham.sites
    full_LDOS = []

    for site in range(sites):
        print(f"Calculating LDOS for site {site}")
        E_grid, ldos = LDOS(Ham=Ham, evals=evals, evecs=evecs, site=site, broadening=broadening)
        full_LDOS.append(ldos)

    return E_grid, full_LDOS
########################################################################


########################################################################
def mean_density_localization(Ham, input, evecs, **kwargs):
    """
    Compute the average density profile across all eigenstates.
    
    Parameters:
    -----------
    Ham, object of class Hamiltonian: the Hamiltonian and its properties.

    evecs, 2-dim numpy array: all the eigenvectors of the diagonalized Hamiltonian.

    Returns
    -------
    avg_dens, float: the average density profile across all eigenstates.


    References
    ----------

    Notes
    -----

    Examples
    --------

    """

    if Ham.stats == "spinful-fermions" or Ham.stats == "spin-0.5":
        raise NotImplementedError("Mean density localization for spinful fermions and spin 1/2s not implemented.")
    
    sites = Ham.sites
    avg_dens = np.zeros(sites)

    # project state into each site to get particle number at each site:
    if input.mean_dens_idx=="all":
        for state in evecs.T:
            if Ham.stats == "spinless-fermions":
                avg_dens += hams.get_state_population_spinless(Ham, state, **kwargs)
            elif Ham.stats == "bosons":
                avg_dens += hams.get_state_population_bosons(Ham, state, **kwargs)
        
        avg_dens /= np.shape(evecs)[0]  # Divide the sum of all the densities by the number of states to obtain the average.
    
    elif input.mean_dens_idx.isdigit():
        idx = int(input.mean_dens_idx)
        state = evecs[:,idx]
        if Ham.stats == "spinless-fermions":
            avg_dens += hams.get_state_population_spinless(Ham, state, **kwargs)
        elif Ham.stats == "bosons":
            avg_dens += hams.get_state_population_bosons(Ham, state, **kwargs)

    return avg_dens
########################################################################


########################################################################
def spin_exp_z(Ham, state, site, **kwargs):
    """
    Compute the expectation <\psi|sigma^z_i|\psi>.
    
    Parameters:
    -----------
    Ham, object of class Hamiltonian: the Hamiltonian and its properties.

    state, list or 1-dim: the state in the sector basis.

    site, int: the site at which to calculate the magnetization.


    Returns
    -------
    s, float: the spin expectation in z direction at the given site.


    References
    ----------

    Notes
    -----

    Examples
    --------

    """
            
    # Get basis for sector:
    if Ham.sector is None:
        raise NotImplementedError("Calculation of magnetization is only supported when running calculations for a specific sector and not for the entire Hilbert space.")
    else:
        sz = int(Ham.sector)
    
    sector_basis = Ham.basis_states[sz]

    # Initialize the expectation value:
    s=0
    # Loop over all entries in the state (basis states)
    for bin, ranks in sector_basis.items():

        idx_b = ranks['rel_rank']     # (relative) index of the basis state in the sector sz
        basis_state = ranks['abs_rank']
        coeff = state[idx_b]
        weight = np.abs(coeff)**2     # weight that the state has in the basis state b
        #print(weight)
        #print("\n")

        if Ham.stats == "spin-0.5":
            s += 0.5*(2*Ham.get_site_value(basis_state, site) - 1)*weight    # spins
        else:
            raise ValueError("You are trying to calculate magnetization for a non-spin model!")
        
    return s

########################################################################


########################################################################
def magnetization_z(Ham, state, **kwargs):
    """
    Compute the expectation \sum_i <\psi|sigma^z_i|\psi>.
    
    Parameters:
    -----------
    Ham, object of class Hamiltonian: the Hamiltonian and its properties.

    state, list or 1-dim: the state in the sector basis.

    Returns
    -------
    m, float: the magnetization (total spin in z direction).


    References
    ----------

    Notes
    -----

    Examples
    --------

    """
    
    m = 0
    for site in range(Ham.sites):
        m += spin_exp_z(Ham=Ham, state=state, site=site)

    return m

########################################################################

########################################################################
def spin_profile(Ham, state, **kwargs):
    """
    Compute the vector [<\psi|sigma^z_1|\psi>, <\psi|sigma^z_2|\psi>, ..., <\psi|sigma^z_L|\psi>]
    
    Parameters:
    -----------
    Ham, object of class Hamiltonian: the Hamiltonian and its properties.

    state, list or 1-dim: the state in the sector basis.

    Returns
    -------
    prof, list of float: the spin profile (in z direction).


    References
    ----------

    Notes
    -----

    Examples
    --------

    """
    
    prof = np.zeros(Ham.sites)
    for site in range(Ham.sites):
        prof[site] = spin_exp_z(Ham=Ham, state=state, site=site)

    return prof

########################################################################

########################################################################
def zz_correlation(Ham, state, site_i, site_j, connected, **kwargs):
    """
    Compute the expectation <\psi|sigma^z_i sigma^z_j|\psi>.
    
    Parameters:
    -----------
    Ham, object of class Hamiltonian: the Hamiltonian and its properties.

    state, list or 1-dim: the state in the sector basis.

    site_i, int: the first site at which correlation should be calculated.

    site_j, int: the second site at which correlation should be calculated.

    connected, boolean: flag to activate the calculation of the connected component of the correlation, i.e. <\psi|sigma^z_i sigma^z_j|\psi> - <\psi|sigma^z_i|\psi><\psi|sigma^z_j|\psi>

    Returns
    -------
    corr, float: the correlation (in z direction).


    References
    ----------

    Notes
    -----

    Examples
    --------

    """
    if Ham.stats != "spin-0.5":
        raise ValueError("You are trying to calculate magnetization for a non-spin model!")

    # Get basis for sector:
    if Ham.sector is None:
        raise NotImplementedError("Calculation of zz correlation is only supported when running calculations for a specific sector and not for the entire Hilbert space.")
    else:
        sz = int(Ham.sector)

    sector_basis = Ham.basis_states[sz]

    # Initialize the correlation value:
    corr=0
    # Loop over all entries in the state (basis states)
    for _, ranks in sector_basis.items():

        idx_b = ranks['rel_rank']     # (relative) index of the basis state in the sector sz
        basis_state = ranks['abs_rank']     # The integer representation of the binary
        coeff = state[idx_b]
        weight = np.abs(coeff)**2     # weight that the state has in the basis state b

        corr += 0.25*(2*Ham.get_site_value(basis_state, site_i) - 1)*(2*Ham.get_site_value(basis_state, site_j) - 1)*weight    # spins
    
    if connected==True:
        corr -= spin_exp_z(Ham=Ham, state=state, site=site_i)*spin_exp_z(Ham=Ham, state=state, site=site_j)

    return corr

########################################################################

########################################################################
def zz_correlation_matrix(Ham, state, connected, **kwargs):
    """
    Compute the expectation <\psi|sigma^z_i sigma^z_j|\psi> for all i, j.
    
    Parameters:
    -----------
    Ham, object of class Hamiltonian: the Hamiltonian and its properties.

    state, list or 1-dim: the state in the sector basis.

    connected, boolean: flag to activate the calculation of the connected component of the correlation, i.e. <\psi|sigma^z_i sigma^z_j|\psi> - <\psi|sigma^z_i|\psi><\psi|sigma^z_j|\psi>

    Returns
    -------
    corr_mat, numpy array: the correlation matrix (in z direction).


    References
    ----------

    Notes
    -----

    Examples
    --------

    """
    
    # Calculate only the elements above the diagonal since the matrix is symmetric:
    corr_mat = np.zeros(shape=(Ham.sites, Ham.sites))
    for site_i in range(Ham.sites):
        for site_j in range(site_i+1, Ham.sites):
            corr_mat[site_i, site_j] = zz_correlation(Ham=Ham, state=state, site_i=site_i, site_j=site_j, connected=connected)
    
    # TODO: Check this
    corr_mat = corr_mat + corr_mat.T
    for site_i in range(Ham.sites):
        corr_mat[site_i, site_i] = zz_correlation(Ham=Ham, state=state, site_i=site_i, site_j=site_i, connected=connected)

    return corr_mat

########################################################################

########################################################################
def bath_averaged_zz_correlation_old(Ham, state, site_i, connected, **kwargs):
    """
    Compute the expectation <\psi|sigma^z_i sigma^z_j|\psi> averaged over all j's in the bath.
    
    Parameters:
    -----------
    Ham, object of class Hamiltonian: the Hamiltonian and its properties.

    state, list or 1-dim: the state in the sector basis.

    site_i, int: the first site at which correlation should be calculated.

    connected, boolean: flag to activate the calculation of the connected component of the correlation, i.e. <\psi|sigma^z_i sigma^z_j|\psi> - <\psi|sigma^z_i|\psi><\psi|sigma^z_j|\psi>

    Returns
    -------
    corr, float: the correlation (in z direction).


    References
    ----------

    Notes
    -----

    Examples
    --------

    """
    
    if Ham.stats != "spin-0.5":
        raise ValueError("You are trying to calculate magnetization for a non-spin model!")

    averaging_method = kwargs.get("averaging_method", "median")

    # Check that there is a bath to begin with:
    if Ham.model == "disordered-XXZ-bath":

        # Get basis for sector:
        if Ham.sector is None:
            raise NotImplementedError("Calculation of zz correlation is only supported when running calculations for a specific sector and not for the entire Hilbert space.")
        else:
            sz = int(Ham.sector)

        sector_basis = Ham.basis_states[sz]

        # Loop over all sites in the bath (the bath is always assumed on the RIGHT):
        avg_corr = []
        for site_j in range(Ham.pars["LA"], Ham.sites+1):

            # Initialize the correlation value:
            corr=0
            # Loop over all entries in the state (basis states)
            for _, ranks in sector_basis.items():

                idx_b = ranks['rel_rank']     # (relative) index of the basis state in the sector sz
                basis_state = ranks['abs_rank']     # The integer representation of the binary
                coeff = state[idx_b]
                weight = np.abs(coeff)**2     # weight that the state has in the basis state b
            
                corr += 0.25*(2*Ham.get_site_value(basis_state, site_i) - 1)*(2*Ham.get_site_value(basis_state, site_j) - 1)*weight    # spins
        
            if connected==True:
                corr -= spin_exp_z(Ham=Ham, state=state, site=site_i)*spin_exp_z(Ham=Ham, state=state, site=site_j)

            avg_corr.append(corr)

        bath_avg_corr = utils.compute_average(obs_list=avg_corr,
                                              averaging_method=averaging_method)

    else:
        raise NotImplementedError("The model you chose does not have a bath! The only model with a bath is currently disorder-XXZ-bath.")

    return bath_avg_corr

########################################################################

########################################################################
def bath_averaged_zz_correlation_site(corr_mat, site_i, LA, **kwargs):
    """
    Compute the expectation <\psi|sigma^z_i sigma^z_j|\psi> averaged over all j's in the bath.
    
    Parameters:
    -----------
    corr_mat, numpy array: the full correlation matrix (it should already be connected or not).

    site_i, int: the first site at which correlation should be calculated.

    LA, int: the site where the bath begins.

    Returns
    -------
    bath_avg_corr, float: the bath-averaged correlation at site i (in z direction).


    References
    ----------

    Notes
    -----

    Examples
    --------

    """
    
    averaging_method = kwargs.get("averaging_method", "median")

    avg_corr = corr_mat[site_i, LA:]
    bath_avg_corr = utils.compute_average(obs_list=avg_corr,
                                          averaging_method=averaging_method)

    return bath_avg_corr

########################################################################

########################################################################
def bath_averaged_zz_correlation_function(corr_mat, LA, sites, **kwargs):
    """
    Compute the expectation <\psi|sigma^z_i sigma^z_j|\psi> averaged over all j's in the bath for all sites i.

    Parameters:
    -----------
    corr_mat, numpy array: the full correlation matrix (it should already be connected or not).

    LA, int: the site where the bath begins.

    sites, int: the number of sites in the system.

    Returns
    -------
    corr_func, numpy array: the correlation function g^{(2)}(i) (in z direction).


    References
    ----------

    Notes
    -----

    Examples
    --------

    """

    averaging_method = kwargs.get("averaging_method", "median")
    
    # Calculate only the elements above the diagonal since the matrix is symmetric:
    corr_func = np.zeros(sites)
    for site_i in range(sites):
            corr_func[site_i] = bath_averaged_zz_correlation_site(corr_mat=corr_mat, site_i=site_i, LA=LA, averaging_method=averaging_method)
    
    return corr_func

########################################################################

########################################################################
def correlation_decay_length(corr_func, sites, fit_func, **kwargs):
    """
    
    """

    LA = kwargs.get("LA", None)

    #TODO; Add a check for allowed fit functions, e.g. with correct number of fit parameters
    # xi is assumed to be the last parameter in the fitting function, e.g. fit.simple_exp(x, mu, c, xi)
    if fit_func=="simple-exp":
        if LA is not None:
            #simple_exp_fixed_mu = partial(fit.simple_exp, mu=LA)   # This was not working for some reason
            # FITTING EXPONENTIAL DIRECTLY:
            # def simple_exp_fixed_mu(x, c, xi):
            #     return fit.simple_exp(x, mu=LA, c=c, xi=xi)

            # popt, _ = curve_fit(f=simple_exp_fixed_mu,
            #                     xdata=np.linspace(1.0, sites, sites), 
            #                     ydata=np.abs(corr_func))
            
            # FITTING LOG OF DATA W/ LINEAR FIT:     
            def linear_fit_fixed_C(x, A, B):
                return fit.linear_fit(x, C=LA, A=A, B=B)
            
            # replace zeros in data with something very tiny:
            corr_func[corr_func == 0] = 1e-10
            ydata = np.log(np.abs(corr_func))
            popt, _ = curve_fit(f=linear_fit_fixed_C,
                                xdata=np.linspace(1.0, sites, sites), 
                                ydata=ydata)

        else:
            popt, _ = curve_fit(f=fit.simple_exp,
                                xdata=np.linspace(1.0, sites, sites), 
                                ydata=np.abs(corr_func))

    else:
        raise NotImplementedError(f"The fit function {fit_func} is currently not implemented.")
    
    return popt

########################################################################

########################################################################
def GS_IPR(Ham, gs, **kwargs):
    """
    Compute the inverse participation ratio for the ground state
    
    Parameters:
    -----------
    Ham, object of class Hamiltonian: the Hamiltonian and its properties.

    gs, list or 1-dim: the ground state in the sector basis.


    Returns
    -------
    IPR


    References
    ----------

    Notes
    -----

    Examples
    --------

    """

    if Ham.stats == "spinful-fermions" or Ham.stats == "spin-0.5":
        raise NotImplementedError("IPR for spinful fermions or spins not implemented.")
    
    N = Ham.pars["N"]
    sites = Ham.sites

    IPR = 0
    # project state into each site to get particle number at each site:
    if Ham.stats == "spinless-fermions":
        state_arr = hams.get_state_population_spinless(Ham, gs, **kwargs)
    elif Ham.stats == "bosons":
        state_arr = hams.get_state_population_bosons(Ham, gs)

    #print("state:", state)
    #print("state_arr:", state_arr)
    for i in range(sites):
        IPR += np.abs(state_arr[i])**2/N**2

    return IPR

########################################################################

########################################################################
def scaled_IPR(Ham, state, **kwargs):
    """
    Compute the scaled inverse participation ratio.
    
    Parameters:
    -----------
    Ham, object of class Hamiltonian: the Hamiltonian and its properties.

    state, list or 1-dim: the state in the sector basis.


    Returns
    -------
    IPR


    References
    ----------
    https://journals.aps.org/prl/abstract/10.1103/PhysRevLett.128.146601

    Notes
    -----
    state: |psi>
    filling: nu = N/L

    IPR = 1/(1-nu)*(1/N*sum_{i=1}^L |<psi| n_i |psi>|^2 - nu)

    Examples
    --------

    """

    if Ham.stats == "spinful-fermions" or Ham.stats == "spin-0.5":
        raise NotImplementedError("IPR for spinful fermions not implemented.")
    
    N = Ham.pars["N"]
    sites = Ham.sites
    filling = N/sites

    IPR = 0
    # project state into each site to get particle number at each site:
    if Ham.stats == "spinless-fermions":
        state_arr = hams.get_state_population_spinless(Ham, state, **kwargs)
    elif Ham.stats == "bosons":
        state_arr = hams.get_state_population_bosons(Ham, state)

    #print("state:", state)
    #print("state_arr:", state_arr)
    for i in range(sites):
        IPR += np.abs(state_arr[i])**2

    IPR = 1/(1-filling)*(IPR/N - filling)

    return IPR

########################################################################


########################################################################
def average_IPR(Ham, evecs, verbose, **kwargs):
    """
    Compute the average inverse participation ratio for all eigenstates.
    
    Parameters:
    -----------
    Ham, object of class Hamiltonian: the Hamiltonian and its properties.

    evecs, list or 1-dim: the eigenvectors of the Hamiltonian.


    Returns
    -------
    IPR


    References
    ----------
    https://journals.aps.org/prl/abstract/10.1103/PhysRevLett.128.146601

    Notes
    -----
    state: |psi>
    filling: nu = N/L
    
    IPR = 1/(1-nu)*(1/N*sum_{i=1}^L |<psi| n_i |psi>|^2 - nu)

    Examples
    --------

    """

    print("Calculating IPR...")

    dim = Ham.sector_dim
    #print("dim", dim)
    IPR_avg = 0
    for state in evecs:
        IPR = scaled_IPR(Ham, state, **kwargs)
        #print("IPR", IPR)
        IPR_avg += IPR
    #print(IPR_avg)
    IPR_avg /= dim
    
    if verbose:
        print("IPR average:", IPR_avg)
    return IPR_avg

########################################################################