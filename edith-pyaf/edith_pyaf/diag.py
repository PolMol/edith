#!/usr/bin/env python
############################################################################################
##### This file calculates the eigenvalues of a given matrix (Hamiltonian).
############################################################################################

####################################
###########  IMPORTS   #############
####################################
from __future__ import division
from __future__ import print_function
import scipy as sp
import numpy as np

# Custom modules:
from . import input as inp
####################################


########################################################################
def diagonalize(Ham,
                input, 
                **kwargs):
    """
    Compute the eigenvalues and eigenvectors of the given Hamiltonian.
    
    Parameters:
    -----------
    Ham, instance of Hamiltonian class: the (possibly sparse) Hamiltonian.
    input, instance of Input class: the input information.

    **kwargs:
    - n_evals, int: the number of lowest eigenvalues to be computed.


    Returns
    -------
    evals, list of floats: the n_evals smallest eigenvalues.


    References
    ----------

    Notes
    -----

    Examples
    --------

    """

    n_evals = input.n_lowest_eigenvalues

    # Hamiltonian is saved as sparse matrix:
    if Ham.sparse == True:

        # Assemble sparse matrix from class data:
        H = sp.sparse.csr_matrix((Ham.mat_els, (Ham.rows, Ham.cols)))

        if n_evals==None or n_evals == np.amax([Ham.cols,Ham.rows])+1:
            # Calculate all eigenvalues:
            if input.compute_evecs==True:
                if input.model=="fermionic-interacting-Hatano-Nelson" or input.model=="bosonic-interacting-Hatano-Nelson":    # Non-Hermitian models (require non-Hermitian diagonalizer)
                    evals, evecs = sp.linalg.eig(H.todense())     # sp.sparse.linalg.eigsh cannot compute ALL the eigenvalues, it's tailored to only compute a subset.
                else:
                    evals, evecs = sp.linalg.eigh(H.todense(),
                                       eigvals_only=False)     # sp.sparse.linalg.eigsh cannot compute ALL the eigenvalues, it's tailored to only compute a subset.
                #evals, evecs = sort_evals_evecs(evals=evals, evecs=evecs)
                return evals, evecs
            else:
                if input.model=="fermionic-interacting-Hatano-Nelson" or input.model=="bosonic-interacting-Hatano-Nelson":    # Non-Hermitian models (require non-Hermitian diagonalizer)
                    evals, _ = sp.linalg.eig(H.todense())
                else:
                    evals = sp.linalg.eigh(H.todense(),
                                       eigvals_only=True)
                evals, _ = sort_evals_evecs(evals)
                return evals, None

        elif n_evals <= np.amax([Ham.cols,Ham.rows]):    # This assumes the rows and columns contain the bottom right corner of the matrix
            # Calculate only lowest eigenvalues:
            if input.compute_evecs==True:
                if input.model=="fermionic-interacting-Hatano-Nelson" or input.model=="bosonic-interacting-Hatano-Nelson":    # Non-Hermitian models (require non-Hermitian diagonalizer)
                    evals, evecs = sp.sparse.linalg.eigs(H, 
                                                        k=n_evals, 
                                                        which='SM', 
                                                        return_eigenvectors=True, 
                                                        maxiter=input.maxiter_Arnoldi,
                                                        tol=input.tolerance_Arnoldi)

                else:
                    evals, evecs = sp.sparse.linalg.eigsh(H, 
                                                        k=n_evals, 
                                                        which='SM', 
                                                        return_eigenvectors=True, 
                                                        maxiter=input.maxiter_Arnoldi,
                                                        tol=input.tolerance_Arnoldi)
                evals, evecs = sort_evals_evecs(evals=evals, evecs=evecs)
                return evals, evecs

            else:
                if input.model=="fermionic-interacting-Hatano-Nelson" or input.model=="bosonic-interacting-Hatano-Nelson":    # Non-Hermitian models (require non-Hermitian diagonalizer)
                    evals = sp.sparse.linalg.eigs(H,
                                                k=n_evals,
                                                which='SA', 
                                                return_eigenvectors=False,
                                                maxiter=input.maxiter_Arnoldi,
                                                tol=input.tolerance_Arnoldi)
                else:
                    evals = sp.sparse.linalg.eigsh(H,
                                               k=n_evals,
                                               which='SM', 
                                               return_eigenvectors=False,
                                               maxiter=input.maxiter_Arnoldi,
                                               tol=input.tolerance_Arnoldi)                
                    evals, _ = sort_evals_evecs(evals)
                    return evals, None

    # Hamiltonian is full dense matrix (not stored as sparse)
    else:
        if n_evals==None:
            if input.model=="fermionic-interacting-Hatano-Nelson" or input.model=="bosonic-interacting-Hatano-Nelson":    # Non-Hermitian models (require non-Hermitian diagonalizer)
                evals = sp.linalg.eig(Ham.arr, eigvals_only=True)
            else:
                evals = sp.linalg.eigh(Ham.arr, eigvals_only=True)
            evals = sort_evals_evecs(evals)
            return evals, None
        else:
            subset = [0,n_evals-1]

            if input.compute_evecs==True:
                if input.model=="fermionic-interacting-Hatano-Nelson" or input.model=="bosonic-interacting-Hatano-Nelson":    # Non-Hermitian models (require non-Hermitian diagonalizer)
                    evals, evecs = sp.sparse.linalg.eigs(H, 
                                                        k=n_evals, 
                                                        which='SM', 
                                                        return_eigenvectors=False, 
                                                        maxiter=input.maxiter_Arnoldi,
                                                        tol=input.tolerance_Arnoldi)
                else:
                    evals, evecs = sp.sparse.linalg.eigsh(H, 
                                                      k=n_evals, 
                                                      which='SM', 
                                                      return_eigenvectors=False, 
                                                      maxiter=input.maxiter_Arnoldi,
                                                      tol=input.tolerance_Arnoldi)                
                    evals, evecs = sort_evals_evecs(evals=evals, evecs=evecs)
                    return evals, evecs
            
            else:
                if input.model=="fermionic-interacting-Hatano-Nelson" or input.model=="bosonic-interacting-Hatano-Nelson":    # Non-Hermitian models (require non-Hermitian diagonalizer)
                    evals = sp.linalg.eig(Ham.arr, 
                                        eigvals_only=True, 
                                        subset_by_index=subset)
                else:
                    evals = sp.linalg.eigh(Ham.arr, 
                                        eigvals_only=True, 
                                        subset_by_index=subset)
                    
                evals, _ = sort_evals_evecs(evals)
                return evals, None

########################################################################

########################################################################
def sort_evals_evecs(evals, **kwargs):

    evecs = kwargs.get("evecs", None)

    idx = evals.argsort()[::1]   
    evals = evals[idx]
    if evecs is not None:
        evecs = evecs[:,idx]

    return evals, evecs
########################################################################
