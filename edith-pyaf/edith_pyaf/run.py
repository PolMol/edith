#!/usr/bin/env python
##########################################################################################
# This python program performs exact diagonalization of closed systems.
###########################################################################################

####################################
###########  IMPORTS   #############
####################################
from __future__ import division
from __future__ import print_function
import numpy as np
import scipy as sp
import time 
import os
import shutil

# CUSTOM modules:
from . import input as inp 
from . import glob 
from . import hams
from . import diag
from . import obs
from . import fit
from . import utils
from . import dynamics as dyn
from . import viz
from . import input_output as ino
####################################




############################################################################################################
def main(input_filename):

      # Print logo
      logo = ino.load_logo()
      print(logo)
      time.sleep(1)

      # Get the input
      input = inp.Input(input_filename=input_filename)

      # Perform memory checks:
      utils.memory_checks(input)

      # Remove previous data:
      if input.clean_data == True:

            print("==========================================")   
            print("Cleaning out previously generated data...")
            print("==========================================")
            print("\n")   

            # Define the directories
            directories = ['data/']
            utils.clean_directories(dir=directories)

      # Remove previous data:
      if input.clean_plots == True:

            print("==========================================")   
            print("Cleaning out previously generated plots...")
            print("==========================================")
            print("\n")   

            # Define the directories
            directories = ['plots/', 'movies/']
            utils.clean_directories(dir=directories)

      ########################################################################


      if input.generate_data:     # Perform the calculations to obtain data?

            ################################################################
            ######### INPUT SELF-CONSISTENCY CHECKS
            ################################################################

            if input.model== "dipolar-Aubry-Andre":

                  if input.n_lowest_eigenvalues > utils.binomial(input.sites,int(input.Ham_par1)):
                        raise IOError(f"The given sector N={int(input.Ham_par1)} has {utils.binomial(input.sites,int(input.Ham_par1))} states! Please set \"n_lowest_eigenvalues\" to max this value in the input file.")


                  if input.dynamics==True and input.n_lowest_eigenvalues < utils.binomial(input.sites,int(input.Ham_par1)):
                        raise IOError(f"To calculate full-time dynamics of the given sector N={int(input.Ham_par1)}, you need to calculate all the eigenvalues! Please set \"n_lowest_eigenvalues\" to {utils.binomial(input.sites,int(input.Ham_par1))} in the input file.")
                  

            ################################################################
            ######### SETTING UP HAMILTONIAN
            ################################################################
            timestamp = False
            for realization in range(input.realizations):
                  if realization > 0:
                        # More than a single realization, therefore we need an extra tag to differentiate files for different realizations of the same quantities. We use the timestamp:
                        timestamp = True

                  print("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")   
                  print("===============================================================================")   
                  print(f"               Calculating realization number {realization}                   ")
                  print("===============================================================================")   
                  print("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")   
                  print("\n")
                  print("Setting up Hamiltonian...")
                  ti = time.time()
                  Ham = hams.Hamiltonian(input=input)
                  tf = time.time()
                  print(f"Time needed to set up Hamiltonian: {utils.get_time_diff(ti,tf)}")
                  print("\n") 
                  print("====================================")
                  print(f"Model: {Ham.model}")   
                  print(f"Type of particles: {Ham.stats}")   
                  print("====================================")
                  print("\n") 
                  print("====================================")
                  print("Parameters of the Hamiltonian:")
                  print("Sites:", Ham.sites)
                  print("Hilbert space dimension:", Ham.Hilbert_dim)
                  if Ham.sector is not None:
                        print("Sector:", Ham.sector)
                        print("Sector dimension:", Ham.sector_dim)
                  print("Periodic boundary conditions?", Ham.BC)
                  # Ham_par1:
                  if Ham.model=="transverse-Ising" or Ham.model=="staggered-Heisenberg":
                        print("Hopping value J:", Ham.pars["J"])
                  elif Ham.model=="dipolar-Aubry-Andre":
                        print("Total particle number N:", Ham.pars["N"])
                  elif Ham.model=="Hubbard":
                        print("Total spin z-component sz:", Ham.pars["sz"])
                  elif Ham.model=="disordered-XXZ-bath":
                        print("x and y coupling J:", Ham.pars["J"])
                  # Ham_par2:
                  if Ham.model=="transverse-Ising":
                        print("Transverse field value:", Ham.pars["hx"])
                  elif Ham.model=="staggered-Heisenberg":
                        print("Staggered field value:", Ham.pars["hs"])
                  elif Ham.model=="dipolar-Aubry-Andre":
                        print("Hopping t:", Ham.pars["t"])
                  elif Ham.model=="Hubbard":
                        print("Total particle number N:", Ham.pars["N"])
                  elif Ham.model=="disordered-XXZ-bath":
                        print("Multiplicative z coupling for region A:", Ham.pars["DeltaA"])
                  # Ham_par3:
                  if Ham.model=="staggered-Heisenberg":
                        print("Total Sz spin component:", Ham.pars["sz"])
                  elif Ham.model=="dipolar-Aubry-Andre":
                        print("Quasiperiodic potential strength Delta:", Ham.pars["Delta"])
                  elif Ham.model=="Hubbard":
                        print("Chemical potential mu:", Ham.pars["mu"])
                  elif Ham.model=="disordered-XXZ-bath":
                        print("Multiplicative z coupling for region B:", Ham.pars["DeltaB"])
                  # Ham_par4:
                  if Ham.model=="dipolar-Aubry-Andre":
                        print("Quasiperiodicity wave vector beta:", Ham.pars["beta"])
                  elif Ham.model=="Hubbard":
                        print("Hopping t:", Ham.pars["t"])
                  elif Ham.model=="disordered-XXZ-bath":
                        print("Multiplicative z coupling for region AB:", Ham.pars["DeltaAB"])            
                  # Ham_par5:
                  if Ham.model=="dipolar-Aubry-Andre":
                        print("Phase phi:", Ham.pars["phi"])
                  elif Ham.model=="Hubbard":
                        print("On-site interaction U:", Ham.pars["U"])
                  elif Ham.model=="disordered-XXZ-bath":
                        print("Disorder strength in regions A and B:", Ham.pars["Wmax"])
                  # Ham_par6:
                  if Ham.model=="dipolar-Aubry-Andre":
                        print("Nearest-neighbor density-density interaction strength eta:", Ham.pars["eta"])
                  elif Ham.model=="disordered-XXZ-bath":
                        print("Custom-given magnetic field:", Ham.pars["W"])
                        print("Magnetic field:", Ham.potential)
                  # Ham_par7:
                  if Ham.model=="disordered-XXZ-bath":
                        print("Length of region A:", Ham.pars["LA"])
                  # Ham_par8:
                  if Ham.model=="disordered-XXZ-bath":
                        print("Total spin z-component:", Ham.pars["sz"])
                  # Ham_par9:
                  # Ham_par10:      
                  print("Sparse encoding?", Ham.sparse)
                  print("====================================")
                  print("\n") 

                  # Visualize Hamiltonian matrix:
                  if input.print_Hamiltonian:
                        print("rows:", Ham.rows)
                        print("cols:", Ham.cols)
                        print("mat_els:", Ham.mat_els)

                        Hprint = sp.sparse.csr_matrix((Ham.mat_els, (Ham.rows, Ham.cols)))
                        print(Hprint.todense())
                  if input.plot_Hamiltonian:
                        viz.visualize_hamiltonian(Ham)
                  else: 
                        print("====================================")
                        print("Hamiltonian visualization skipped.")
                        print("====================================")
                  print("\n") 
                  ################################################################


                  ################################################################
                  ######### DIAGONALIZATION
                  ################################################################      
                  print("====================================")
                  print("Performing diagonalization...") 
                  print("====================================")
                  ti = time.time()
                  try:
                        eigenvalues, eigenvectors = diag.diagonalize(Ham=Ham, 
                                                                     input=input)
                  except:
                        print("Diagonalization failed! Proceeding to next realization if there is one.")
                        continue
                  tf = time.time()
                  print(f"Time needed to perform diagonalization: {utils.get_time_diff(ti,tf)}")
                  # Saving eigenvalues:
                  if input.save_evals == True:
                        print("Saving eigenvalues...")
                        ino.save_eigenvalues(model=Ham.model,
                                          sites=Ham.sites,
                                          pars=Ham.pars, 
                                          BC=Ham.BC,
                                          eigs=eigenvalues)
                  else: 
                        print("Eigenvalues NOT saved.")
                  # Saving eigenvectors:
                  if input.save_evecs == True:
                        print("Saving eigenvectors...")
                        ino.save_eigenvectors(model=Ham.model, 
                                          sites=Ham.sites, 
                                          pars=Ham.pars, 
                                          BC=Ham.BC, 
                                          evecs=eigenvectors)
                  else: 
                        print("Eigenvectors NOT saved.")
                  if input.plot_evals==True:
                        viz.visualize_eigenvalues(evals=eigenvalues,
                                                Ham=Ham)
                  else:
                        print("Eigenvalue visualization skipped.")
                  if input.plot_evecs==True:
                        for j in range(int(input.plot_evecs_num)):
                              if Ham.stats=="spinful-fermions":
                                    viz.visualize_eigenstate_multicomponent(evec_Hilbert=eigenvectors[:,j], 
                                                                        index=j, 
                                                                        Ham=Ham)
                              elif Ham.stats=="spinless-fermions" or Ham.stats=="spin-0.5" or Ham.stats=="bosons":
                                    viz.visualize_eigenstate(evec_Hilbert=eigenvectors[:,j], 
                                                            index=j, 
                                                            Ham=Ham)
                  else:
                        print("Eigenvector visualization skipped.")
                  print("====================================")
                  print("\n")   


                  # Print some info:
                  print("====================================")
                  if input.print_gs_energy==True:
                        print("Ground state energy:", min(eigenvalues))
                        if Ham.model=="Hubbard":
                              if Ham.pars["sz"]==0 and Ham.pars["N"]==Ham.sites:    #half filling -> exact solution by Lieb & Wu
                                    ground_state_exact, err = utils.ground_state_Hubbard_half_filling(t=Ham.pars["t"], U=Ham.pars["U"], N=Ham.sites)
                                    print("Ground state energy (extensive part):", ground_state_exact, ". Error:", err)
                                    print(f"Ratio of finite-size corrections: {(ground_state_exact-min(eigenvalues))/ground_state_exact*100}%")
                  else:
                        print("Ground state energy output skipped.")
                  
                  if input.n_lowest_eigenvalues>1:
                        if input.print_es_energies==True:
                              print("Excited state energies:", np.sort_complex(eigenvalues)[1:])
                  else:
                        print("Excited state energy output skipped.")
                  if input.print_gs==True:
                        print("Ground state:")
                        print(eigenvectors[:,0])
                  else:
                        print("Ground state output skipped.")
                  if input.print_es==True: 
                        print("Excited states:")
                        if input.n_lowest_eigenvalues>1:
                              for k in range(1,input.n_lowest_eigenvalues):
                                    print(eigenvectors[:,k])
                  else:
                        print("Excited state output skipped.")
                  print("====================================")
                  print("\n")   
                  ################################################################


                  ################################################################
                  ######### GROUND-STATE OBSERVABLES
                  ################################################################
                  print("====================================")
                  print("Basis:")
                  print("====================================")
                  if input.print_basis==True:
                        if Ham.stats=="spinful-fermions":
                              print("Spin up:")
                              print(Ham.basis_states_up)
                              print("Spin down:")
                              print(Ham.basis_states_down)
                        else:
                              for sector, sector_dic in Ham.basis_states.items():
                                    print(f"Sector {sector}")
                                    for state_bin, ranks in sector_dic.items():
                                          rel_rank = ranks['rel_rank']
                                          abs_rank = ranks['abs_rank']
                                          print(state_bin, f"abs_rank: {abs_rank}", f"rel_rank: {rel_rank}")
                  else:
                        print(f"Basis output skipped.")
                  print("====================================")
                  print("\n")

                  print("====================================")
                  print("Calculating ground-state observables...")
                  print("====================================")

                  if input.DOS==True:
                        print("Calculating DOS...")
                        ti = time.time()
                        E_grid, dos = obs.DOS(evals=eigenvalues, broadening=input.broadening)
                        tf = time.time()
                        print(f"Time needed to calculate DOS: {utils.get_time_diff(ti,tf)}")
                        print("Saving DOS and E_grid...")
                        ino.save_obs(model=Ham.model,
                                     sites=Ham.sites,
                                     pars=Ham.pars, 
                                     BC=Ham.BC,
                                     obs_name="DOS",
                                     obs_value=dos,
                                     timestamp=timestamp)
                        ino.save_obs(model=Ham.model,
                                     sites=Ham.sites,
                                     pars=Ham.pars, 
                                     BC=Ham.BC,
                                     obs_name="E_grid_DOS",
                                     obs_value=E_grid)      # Note: the grid is always the same for every realization, so we don't need timestamps.
                        
                        if input.plot_DOS==True:
                              dim = utils.get_dimensionality(dos)
                              if dim==0:
                                    viz.plot_observable_vs_one_parameter(obs_name="DOS", 
                                                                         obs=dos, 
                                                                         par="Energy", 
                                                                         par_list=E_grid, 
                                                                         model=Ham.model, 
                                                                         sites=Ham.sites, 
                                                                         BC=Ham.BC, 
                                                                         static_pars=Ham.pars, 
                                                                         )
                              elif dim==1:
                                    pars=["Real energy", "Imaginary energy"]
                                    E_grid_re = np.unique(np.real(E_grid))
                                    E_grid_im = np.unique(np.imag(E_grid))

                                    #print(E_grid_re)
                                    #print(E_grid_im)

                                    par_lists={}
                                    par_lists["Real energy"] = E_grid_re
                                    par_lists["Imaginary energy"] = E_grid_im

                                    viz.plot_observable_vs_two_parameters(obs_name="DOS", 
                                                                          obs=dos, 
                                                                          pars=pars, 
                                                                          par_lists=par_lists, 
                                                                          model=Ham.model, 
                                                                          sites=Ham.sites, 
                                                                          BC=Ham.BC,
                                                                          static_pars=Ham.pars,
                                                                          )                                    

                  if input.LDOS==True:
                        print("Calculating LDOS...")
                        ti = time.time()
                        E_grid, ldos_all = obs.LDOS_all_sites(Ham=Ham, evals=eigenvalues, evecs=eigenvectors, broadening=input.broadening)
                        tf = time.time()
                        print(f"Time needed to calculate LDOS: {utils.get_time_diff(ti,tf)}")
                        print("Saving LDOS and grid...")
                        ino.save_obs(model=Ham.model,
                                     sites=Ham.sites,
                                     pars=Ham.pars, 
                                     BC=Ham.BC,
                                     obs_name="LDOS",
                                     obs_value=ldos_all,
                                     timestamp=timestamp)   
                        ino.save_obs(model=Ham.model,
                                     sites=Ham.sites,
                                     pars=Ham.pars, 
                                     BC=Ham.BC,
                                     obs_name="E_grid_LDOS",
                                     obs_value=E_grid)                                            
                        #print(np.shape(ldos_all))

                        if input.plot_LDOS==True:
                              vmax=np.amax(ldos_all)
                              for site in range(Ham.sites):
                                    dim = utils.get_dimensionality(ldos_all[site])
                                    static_pars = Ham.pars.copy()
                                    static_pars["site"] = site
                                    
                                    if dim==0:
                                          viz.plot_observable_vs_one_parameter(obs_name="LDOS", 
                                                                               obs=ldos_all[site], 
                                                                               par="Energy", 
                                                                               par_list=E_grid, 
                                                                               model=Ham.model, 
                                                                               sites=Ham.sites, 
                                                                               BC=Ham.BC,
                                                                               static_pars=static_pars,
                                                                               vmax=vmax 
                                                                               )
                                    elif dim==1:
                                          pars=["Real energy", "Imaginary energy"]
                                          E_grid_re = np.unique(np.real(E_grid))
                                          E_grid_im = np.unique(np.imag(E_grid))
                                          par_lists={}
                                          par_lists["Real energy"] = E_grid_re
                                          par_lists["Imaginary energy"] = E_grid_im

                                          viz.plot_observable_vs_two_parameters(obs_name="LDOS",
                                                                                obs=ldos_all[site], 
                                                                                pars=pars,
                                                                                par_lists=par_lists,
                                                                                model=Ham.model,
                                                                                sites=Ham.sites,
                                                                                BC=Ham.BC,
                                                                                static_pars=static_pars,
                                                                                vmax=vmax
                                                                                )   

                  if input.mean_density==True:
                        print("Calculating mean density profile...")
                        ti = time.time()
                        mean_dens = obs.mean_density_localization(Ham=Ham, input=input, evecs=eigenvectors)
                        tf = time.time()
                        print(mean_dens)
                        print(f"Time needed to calculate mean density profile: {utils.get_time_diff(ti,tf)}")
                        print("Saving mean density profile...")
                        ino.save_obs(model=Ham.model,
                                     sites=Ham.sites,
                                     pars=Ham.pars, 
                                     BC=Ham.BC,
                                     obs_name="mean_density",
                                     obs_value=mean_dens,
                                     timestamp=timestamp)

                        if input.plot_mean_density==True:
                              viz.plot_observable_vs_one_parameter(obs_name="mean density",
                                                                   obs=mean_dens,
                                                                   par="site",
                                                                   par_list=np.linspace(1,Ham.sites,Ham.sites),
                                                                   model=Ham.model,
                                                                   sites=Ham.sites,
                                                                   BC=Ham.BC,
                                                                   static_pars=Ham.pars,
                                                                   plot_com=True,
                                                                   )

                  if input.GS_IPR==True:
                        print("Calculating ground-state IPR...")
                        ti = time.time()
                        GS_IPR = obs.GS_IPR(Ham=Ham, gs=eigenvectors[:,0], verbose=input.verbose)
                        tf = time.time()
                        print(f"Time needed to calculate many-body IPR: {utils.get_time_diff(ti,tf)}")
                        print("IPR value:", GS_IPR)
                        print("Saving ground-state IPR...")
                        ino.save_obs(model=Ham.model,
                                    sites=Ham.sites,
                                    pars=Ham.pars, 
                                    BC=Ham.BC,
                                    obs_name="GS_IPR",
                                    obs_value=GS_IPR,
                                    timestamp=timestamp)
                        
                  if input.MB_IPR==True:
                        print("Calculating many-body IPR...")
                        ti = time.time()
                        MB_IPR = obs.average_IPR(Ham=Ham, evecs=eigenvectors, verbose=input.verbose)
                        tf = time.time()
                        print(f"Time needed to calculate many-body IPR: {utils.get_time_diff(ti,tf)}")
                        print("IPR value:", MB_IPR)
                        print("Saving many-body IPR...")
                        ino.save_obs(model=Ham.model,
                                    sites=Ham.sites,
                                    pars=Ham.pars, 
                                    BC=Ham.BC,
                                    obs_name="MB_IPR",
                                    obs_value=MB_IPR,
                                    timestamp=timestamp)
                        
                  if input.energy_gap==True:
                        print("Calculating energy gap...")
                        ti = time.time()
                        energy_gap = obs.energy_gap(evals=eigenvalues, i=input.energy_gap_indices[0], j=input.energy_gap_indices[1], kind=input.energy_gap_kind)
                        tf = time.time()
                        print(f"Time needed to calculate energy gap: {utils.get_time_diff(ti,tf)}")
                        print("Gap value:", energy_gap)
                        print("Saving energy gap...")
                        ino.save_obs(model=Ham.model,
                                    sites=Ham.sites,
                                    pars=Ham.pars, 
                                    BC=Ham.BC,
                                    obs_name="energy_gap",
                                    obs_value=energy_gap,
                                    timestamp=timestamp)
                        
                  if input.spin_profile_z_gs==True:
                        if input.stats=="spinless-fermions" or input.stats=="spinful-fermions":
                              raise NotImplementedError("Spin profile for non-spin models not yet implemented!")
                        else:
                              print("Calculating ground state z spin profile...")
                              ti = time.time()
                              spin_prof_gs = obs.spin_profile(Ham=Ham, state=eigenvectors[:,0])
                              tf = time.time()
                              print(f"Time needed to calculate ground state z magnetization: {utils.get_time_diff(ti,tf)}")
                              #print("Magnetization:", mag_z)
                                          
                              print("Saving ground state z spin profile...")
                              ino.save_obs(model=Ham.model,
                                           sites=Ham.sites,
                                           pars=Ham.pars,
                                           BC=Ham.BC,
                                           obs_name="spin_profile_z_gs",
                                           obs_value=spin_prof_gs,
                                           timestamp=timestamp)
                                                      
                              if input.plot_spin_profile==True:
                                    print("Generating plot for the ground state magnetization...")
                                    viz.plot_observable_vs_one_parameter(obs_name="Spin profile along z",
                                                                         obs=spin_prof_gs,
                                                                         par="site",
                                                                         par_list=np.linspace(1,Ham.sites,Ham.sites),
                                                                         model=Ham.model,
                                                                         sites=Ham.sites,
                                                                         BC=Ham.BC,
                                                                         static_pars=Ham.pars,
                                                                         ) 

                  if input.correlation_zz_gs==True:
                        if input.stats=="spinless-fermions" or input.stats=="spinful-fermions":
                              raise NotImplementedError("Correlations for non-spin models not yet implemented!")
                        else:
                              print("Calculating ground state zz correlation...")
                              ti = time.time()
                              corr_mat_gs = obs.zz_correlation_matrix(Ham=Ham, state=eigenvectors[:,0], connected=input.connected)
                              tf = time.time()
                              print(f"Time needed to calculate ground state zz correlation matrix: {utils.get_time_diff(ti,tf)}")
                              #print("correlation values:", corr_mat_gs)
                                          
                              print("Saving ground state zz correlation...")
                              ino.save_obs(model=Ham.model,
                                           sites=Ham.sites,
                                           pars=Ham.pars,
                                           BC=Ham.BC,
                                           obs_name="zz_correlation_matrix_gs",
                                           obs_value=corr_mat_gs,
                                           timestamp=timestamp)
                                                      
                              if input.plot_correlation==True:
                                    print("Generating plot for the ground state correlation...")
                                    viz.plot_correlation_gs(Ham=Ham,
                                                            corr=corr_mat_gs,
                                                            )      
                        if input.bath_averaged_correlation_zz_gs==True:
                              if input.stats=="spinless-fermions" or input.stats=="spinful-fermions":
                                    raise NotImplementedError("Correlations for non-spin models not yet implemented!")
                              else:
                                    print("Calculating bath-averaged ground state zz correlation function...")
                                    ti = time.time()
                                    bath_avg_corr_func_gs = obs.bath_averaged_zz_correlation_function(corr_mat=corr_mat_gs,
                                                                                                      LA=Ham.pars["LA"],
                                                                                                      sites=input.sites,
                                                                                                      averaging_method=input.averaging_method)
                                    tf = time.time()
                                    print(f"Time needed to calculate ground state bath-averaged zz correlation function: {utils.get_time_diff(ti,tf)}")
                                    #print("correlation values:", bath_avg_corr_func_gs)
                                          
                                    print("Saving bath-averaged ground state zz correlation function...")
                                    ino.save_obs(model=Ham.model,
                                                 sites=Ham.sites,
                                                 pars=Ham.pars,
                                                 BC=Ham.BC,
                                                 obs_name="bath_averaged_zz_correlation_function_gs",
                                                 obs_value=bath_avg_corr_func_gs,
                                                 timestamp=timestamp)
                                                      
                                    if input.plot_correlation==True:
                                          print("Generating plot for the bath-averaged ground state zz correlation function...")
                                          viz.plot_observable_vs_one_parameter(obs_name="Bath-averaged ground-state correlation function",
                                                                               obs=bath_avg_corr_func_gs,
                                                                               par="site",
                                                                               par_list=np.linspace(1,Ham.sites,Ham.sites),
                                                                               model=Ham.model,
                                                                               sites=Ham.sites,
                                                                               BC=Ham.BC,
                                                                               static_pars=Ham.pars,
                                                                               plot_com=False,
                                                                               )
                              
                                    if input.correlation_length_gs==True:
                                          print("Calculating ground-state correlation decay length...")
                                          ti = time.time()
                                          corr_len_gs = obs.correlation_decay_length(sites=input.sites,
                                                                                     corr_func=bath_avg_corr_func_gs,
                                                                                     fit_func=input.correlation_length_gs_fit,
                                                                                     LA=Ham.pars["LA"])
                                          tf = time.time()
                                          print(f"Time needed to calculate correlation length zz: {utils.get_time_diff(ti,tf)}")
                                    
                                          print("Saving ground-state correlation decay length...")
                                          ino.save_obs(model=Ham.model,
                                                       sites=Ham.sites,
                                                       pars=Ham.pars,
                                                       BC=Ham.BC,
                                                       obs_name="zz_correlation_length_gs",
                                                       obs_value=corr_len_gs,
                                                       timestamp=timestamp)

                  print("====================================")
                  print("\n")

                  ################################################################



                  ################################################################
                  ######### DYNAMICS
                  ################################################################
                  if input.dynamics == True:
                        print("====================================")
                        print("Calculating time evolution...")
                        print("====================================")
                        time_evolved_state = dyn.full_time_evolution(Ham=Ham,
                                                                     input=input, 
                                                                     evals=eigenvalues, 
                                                                     evecs=eigenvectors
                                                                  )
                        if input.save_time_evol == True:
                              ino.save_obs(model=Ham.model,
                                           sites=Ham.sites,
                                           pars=Ham.pars,
                                           BC=Ham.BC,
                                           obs_name="time_evolved_state",
                                           obs_value=time_evolved_state,
                                           timestamp=timestamp)
                                                      
                        if input.plot_dynamics == True:
                              print("Generating plots of time evolution...")
                              if Ham.stats=="spinful-fermions":
                                    raise NotImplementedError()
                              else:
                                    viz.plot_time_evolution_spinless(time_evolved_state=time_evolved_state, 
                                                                     Ham=Ham,
                                                                     input=input)
                                    logx_dens_vs_t_spinless=False
                                    if input.time_spacing=="exp":
                                          logx_dens_vs_t_spinless=True

                                    viz.plot_density_vs_t_spinless(time_evolved_state=time_evolved_state, 
                                                                   Ham=Ham, 
                                                                   initial_state_name=input.initial_state_name,
                                                                   initial_time=input.initial_time, 
                                                                   time_points=input.time_points, 
                                                                   final_time=input.final_time,
                                                                   time_spacing=input.time_spacing,
                                                                   logx=logx_dens_vs_t_spinless)
                        else:             
                              print(f"Plot of dynamics skipped.")


                        if input.movie_dynamics == True:
                              print("Generating movie for time evolution...")
                              if Ham.stats=="spinful-fermions":
                                    raise NotImplementedError()
                              else:
                                    viz.movie_time_evolution_spinless(time_evolved_state=time_evolved_state,
                                                                      Ham=Ham,
                                                                      initial_state_name=input.initial_state_name,
                                                                      initial_time=input.initial_time,
                                                                      time_points=input.time_points, 
                                                                      final_time=input.final_time,
                                                                      time_spacing=input.time_spacing)
                        else:             
                              print(f"Movie for dynamics skipped.")

                        print("====================================")
                        print("\n")  

                  ################################################################


                        ################################################################
                        ######### TIME-DEPENDENT OBSERVABLES
                        ################################################################

                        print("=========================================")
                        print("Calculating time-dependent observables...")
                        print("=========================================")

                        # Initialize variables to store during time evolution:
                        corr_mat_list = []
                        spin_prof_z_list = []
                        bath_avg_corr_func_dyn_list = []
                        corr_len_dyn_arr = []
                        times = utils.get_times(initial_time=input.initial_time, 
                                                final_time=input.final_time, 
                                                time_points=input.time_points, 
                                                time_spacing=input.time_spacing)
                        for idx, t in enumerate(times):
                              state = time_evolved_state[idx]

                              ################
                              # SPIN PROFILE
                              ################
                              if input.spin_profile_z_dyn==True:
                                    if input.stats=="spinless-fermions" or input.stats=="spinful-fermions":
                                          raise NotImplementedError("Magnetization for non-spin models not yet implemented!")
                                    else:
                                          print("Calculating z spin profile...")
                                          ti = time.time()
                                          spin_prof_z_t = obs.spin_profile(Ham=Ham, state=state)
                                          spin_prof_z_list.append(spin_prof_z_t)
                                          tf = time.time()
                                          print(f"Time needed to calculate z magnetization: {utils.get_time_diff(ti,tf)}")
                                          #print("Magnetization:", mag_z_t)
                                          
                                          # UNCOMMENT THIS IF YOU WANT TO SAVE EACH TIME SEPARATELY:
                                          # print("Saving ground state z magnetization...")
                                          # ino.save_obs(model=Ham.model,
                                          #              sites=Ham.sites,
                                          #              pars=Ham.pars,
                                          #              BC=Ham.BC,
                                          #              obs_name="magnetization_z",
                                          #              obs_value=mag_z_t,
                                          #              timestamp=timestamp)
                                                      
                                          if input.plot_dynamics==True and input.plot_spin_profile==True:
                                                print("Generating plot for the ground state magnetization...")
                                                viz.plot_observable_vs_one_parameter(obs_name="Spin profile along z",
                                                                                     obs=spin_prof_z_t,
                                                                                     par="site",
                                                                                     par_list=np.linspace(1,Ham.sites,Ham.sites),
                                                                                     model=Ham.model,
                                                                                     sites=Ham.sites,
                                                                                     BC=Ham.BC,
                                                                                     static_pars=Ham.pars,
                                                                                     ) 


                              ################
                              # CORRELATIONS
                              ################
                              if input.correlation_zz_dyn==True:
                                    if input.stats=="spinless-fermions" or input.stats=="spinful-fermions":
                                          raise NotImplementedError("Correlations for non-spin models not yet implemented!")
                                    else:
                                          print("Calculating zz correlation...")
                                          ti = time.time()
                                          corr_mat = obs.zz_correlation_matrix(Ham=Ham, state=state, connected=input.connected)
                                          corr_mat_list.append(corr_mat)
                                          tf = time.time()
                                          print(f"Time needed to calculate zz correlation matrix: {utils.get_time_diff(ti,tf)}")
                                          #print("correlation values:", corr_mat_dyn)
                                          
                                          # UNCOMMENT THIS IF YOU WANT TO SAVE EACH TIME SEPARATELY:
                                          #print("Saving zz correlation...")
                                          #ino.save_obs(model=Ham.model,
                                          #            sites=Ham.sites,
                                          #            pars=Ham.pars,
                                          #            BC=Ham.BC,
                                          #            obs_name="zz_correlation_matrix_time_{0}".format(t),
                                          #            obs_value=corr_mat,
                                          #            timestamp=timestamp)
                                          #time.sleep(1)
                                                      

                                    if input.bath_averaged_correlation_zz_dyn==True:
                                          print("Calculating bath-averaged ground state zz correlation function...")
                                          ti = time.time()
                                          bath_avg_corr_func_dyn = obs.bath_averaged_zz_correlation_function(corr_mat=corr_mat,
                                                                                                             LA=Ham.pars["LA"],
                                                                                                             sites=input.sites,
                                                                                                             averaging_method=input.averaging_method)
                                          bath_avg_corr_func_dyn_list.append(bath_avg_corr_func_dyn)
                                          tf = time.time()
                                          print(f"Time needed to calculate bath-averaged zz correlation function: {utils.get_time_diff(ti,tf)}")
                                          #print("correlation values:", bath_avg_corr_func_dyn)
                                          
                                          # UNCOMMENT THIS IF YOU WANT TO SAVE EACH TIME SEPARATELY:
                                          #print("Saving bath-averaged ground state zz correlation function...")
                                          #ino.save_obs(model=Ham.model,
                                          #                   sites=Ham.sites,
                                          #                   pars=Ham.pars,
                                          #                   BC=Ham.BC,
                                          #                   obs_name="bath_averaged_zz_correlation_function_time_{0}".format(t),
                                          #                   obs_value=bath_avg_corr_func_dyn,
                                          #                   timestamp=timestamp)
                                                      
                                          if input.correlation_length_dyn==True:
                                                print("Calculating correlation decay length...")
                                                ti = time.time()
                                                corr_len_dyn = obs.correlation_decay_length(sites=input.sites,
                                                                                            corr_func=bath_avg_corr_func_dyn,
                                                                                            fit_func=input.correlation_length_dyn_fit,
                                                                                            LA=Ham.pars["LA"])[-1]
                                                corr_len_dyn_arr.append(corr_len_dyn)
                                                tf = time.time()
                                                print(f"Time needed to calculate correlation length zz: {utils.get_time_diff(ti,tf)}")
                                          
                                                # UNCOMMENT THIS IF YOU WANT TO SAVE EACH TIME SEPARATELY:
                                                #print("Saving dynamical correlation decay length...")
                                                #ino.save_obs(model=Ham.model,
                                                #             sites=Ham.sites,
                                                #             pars=Ham.pars,
                                                #             BC=Ham.BC,
                                                #             obs_name="zz_correlation_length_time_{0}".format(t),
                                                #             obs_value=corr_len_dyn,
                                                #             timestamp=timestamp)

     
                              if input.plot_dynamics==True and input.plot_correlation==True:
                                    print(f"Generating plot for the correlation matrix at time {t}...")
                                    viz.plot_dynamics_correlation(corr=corr_mat,
                                                                  input=input,
                                                                  pars=Ham.pars,
                                                                  t=t
                                                                  )
                                          
                                    print(f"Generating plot for the bath-averaged zz correlation function at time {t}...")
                                    viz.plot_observable_vs_one_parameter(obs_name="Correlation function at time {0}".format(t),
                                                                         obs=bath_avg_corr_func_dyn,
                                                                         par="site",
                                                                         par_list=np.linspace(1,Ham.sites,Ham.sites),
                                                                         model=Ham.model,
                                                                         sites=Ham.sites,
                                                                         BC=Ham.BC,
                                                                         static_pars=Ham.pars,
                                                                         plot_com=False,
                                                                         )
                        
                              elif input.plot_dynamics == False and input.plot_correlation==False:             
                                    print(f"Plot of dynamics skipped.")

                        print("Saving z spin profile...")
                        ino.save_obs(model=Ham.model,
                                     sites=Ham.sites,
                                     pars=Ham.pars,
                                     BC=Ham.BC,
                                     obs_name="spin_profile_z_dyn",
                                     obs_value=spin_prof_z_list,
                                     timestamp=timestamp)
                        print("Saving zz correlation...")
                        ino.save_obs(model=Ham.model,
                                     sites=Ham.sites,
                                     pars=Ham.pars,
                                     BC=Ham.BC,
                                     obs_name="zz_correlation_matrix",
                                     obs_value=corr_mat_list,
                                     timestamp=timestamp)
                        print("Saving bath-averaged ground state zz correlation function...")
                        ino.save_obs(model=Ham.model,
                                     sites=Ham.sites,
                                     pars=Ham.pars,
                                     BC=Ham.BC,
                                     obs_name="bath_averaged_zz_correlation_function",
                                     obs_value=bath_avg_corr_func_dyn_list,
                                     timestamp=timestamp)
                        print("Saving ground-state correlation decay length...")
                        ino.save_obs(model=Ham.model,
                                     sites=Ham.sites,
                                     pars=Ham.pars,
                                     BC=Ham.BC,
                                     obs_name="zz_correlation_length",
                                     obs_value=corr_len_dyn_arr,
                                     timestamp=timestamp)

                        if input.plot_dynamics==True and input.plot_correlation==True:

                              print(corr_len_dyn_arr)
                              print(f"Generating plot for the correlation length as a function of time...")
                              static_pars = Ham.pars.copy()
                              static_pars["t_init"] = input.initial_time
                              static_pars["t_fin"] = input.final_time
                              static_pars["t_points"] = input.time_points
                              viz.plot_observable_vs_one_parameter(obs_name="Correlation length",
                                                                   obs=corr_len_dyn_arr,
                                                                   par="time",
                                                                   par_list=times,
                                                                   model=Ham.model,
                                                                   sites=Ham.sites,
                                                                   BC=Ham.BC,
                                                                   static_pars=static_pars,
                                                                   )

                        print("====================================")
                        print("\n")


                  ################################################################

      if input.averaging==True:     # Create plots for average quantities?

            pars = hams.Hamiltonian.get_parameters(input) 
            print("=========================================")
            print("Calculating averages...")
            print("=========================================")


            if input.GS_IPR==True:
                  print(f"Generating average data for the ground-state IPR...")
                  GS_IPR_avg = ino.load_obs(model=input.model,
                                            sites=input.sites,
                                            pars=pars,
                                            BC=input.BC,
                                            obs_name="GS_IPR",
                                            format=".npy",
                                            averaging_method=input.averaging_method,
                                            verbose=input.verbose)
                  
                  ino.save_obs(model=input.model,
                               sites=input.sites,
                               pars=pars, 
                               BC=input.BC,
                               obs_name="GS_IPR_avg",
                               obs_value=GS_IPR_avg
                               )

            if input.MB_IPR==True:
                  print(f"Generating average data for the many-body IPR...")
                  MB_IPR_avg = ino.load_obs(model=input.model,
                                            sites=input.sites,
                                            pars=pars,
                                            BC=input.BC,
                                            obs_name="MB_IPR",
                                            format=".npy",
                                            averaging_method=input.averaging_method,
                                            verbose=input.verbose)
                  
                  ino.save_obs(model=input.model,
                               sites=input.sites,
                               pars=pars, 
                               BC=input.BC,
                               obs_name="MB_IPR_avg",
                               obs_value=MB_IPR_avg
                               )

            if input.energy_gap==True:
                  print(f"Generating average data for the energy gap...")
                  energy_gap_avg = ino.load_obs(model=input.model,
                                            sites=input.sites,
                                            pars=pars,
                                            BC=input.BC,
                                            obs_name="energy_gap",
                                            format=".npy",
                                            averaging_method=input.averaging_method,
                                            verbose=input.verbose)
                  
                  ino.save_obs(model=input.model,
                               sites=input.sites,
                               pars=pars, 
                               BC=input.BC,
                               obs_name="energy_gap_avg",
                               obs_value=energy_gap_avg
                               )     
                  print(f"The average energy gap is {energy_gap_avg}") 

            if input.mean_density==True:
                  print(f"Generating data for the average mean density...")
                  mean_dens_avg = ino.load_obs(model=input.model,
                                               sites=input.sites,
                                               pars=pars,
                                               BC=input.BC,
                                               obs_name="mean_density",
                                               format=".npy",
                                               averaging_method=input.averaging_method,
                                               verbose=input.verbose
                                           )
                  # PLOT:
                  print(f"Generating plots for the average mean density...")
                  viz.plot_observable_vs_one_parameter(obs_name="averaged mean density",
                                                       obs=mean_dens_avg,
                                                       par="site",
                                                       par_list=np.linspace(1,input.sites,input.sites),
                                                       model=input.model,
                                                       sites=input.sites,
                                                       BC=input.BC,
                                                       static_pars=pars,
                                                       plot_com=True
                                                       )                  
            if input.DOS==True:
                  print(f"Generating data for the average DOS...")
                  dos_avg = ino.load_obs(model=input.model,
                                         sites=input.sites,
                                         pars=pars,
                                         BC=input.BC,
                                         obs_name="DOS",
                                         format=".npy",
                                         averaging_method=input.averaging_method,
                                         verbose=input.verbose)
                  E_grid = ino.load_obs(model=input.model,
                                         sites=input.sites,
                                         pars=pars,
                                         BC=input.BC,
                                         obs_name="E_grid_DOS",
                                         format=".npy",
                                         verbose=input.verbose)            

                  # PLOT:
                  print(f"Generating plots for the average DOS...")
                  dim = utils.get_dimensionality(dos_avg)
                  if dim==0:
                        viz.plot_observable_vs_one_parameter(obs_name="average DOS",
                                                             obs=dos_avg,
                                                             par="Energy",
                                                             par_list=E_grid, 
                                                             model=input.model, 
                                                             sites=input.sites,
                                                             BC=input.BC, 
                                                             static_pars=pars,
                                                             )
                  elif dim==1:
                        axes = ["Real energy", "Imaginary energy"]
                        E_grid_re = np.unique(np.real(E_grid))
                        E_grid_im = np.unique(np.imag(E_grid))
                        par_lists={}
                        par_lists["Real energy"] = E_grid_re
                        par_lists["Imaginary energy"] = E_grid_im
                        
                        viz.plot_observable_vs_two_parameters(obs_name="average DOS",
                                                              obs=dos_avg,
                                                              pars=axes,
                                                              par_lists=par_lists,
                                                              model=input.model,
                                                              sites=input.sites, 
                                                              BC=input.BC,
                                                              static_pars=pars
                                                              )                                       
            if input.LDOS==True:
                  print(f"Generating data for the average LDOS...")
                  ldos_all_avg = ino.load_obs(model=input.model,
                                              sites=input.sites,
                                              pars=pars,
                                              BC=input.BC,
                                              obs_name="LDOS",
                                              format=".npy",
                                              averaging_method=input.averaging_method,
                                              verbose=input.verbose)
                  E_grid = ino.load_obs(model=input.model,
                                         sites=input.sites,
                                         pars=pars,
                                         BC=input.BC,
                                         obs_name="E_grid_LDOS",
                                         format=".npy",
                                         verbose=input.verbose)  
                  
                  print(f"Generating plots for the average LDOS...")
                  vmax=np.amax(ldos_all_avg)
                  for site in range(input.sites):
                        dim = utils.get_dimensionality(ldos_all_avg[site])
                        static_pars = pars.copy()
                        static_pars["site"] = site
                                    
                        if dim==0:
                              viz.plot_observable_vs_one_parameter(obs_name="average LDOS",
                                                                   obs=ldos_all_avg[site],
                                                                   par="Energy", 
                                                                   par_list=E_grid,
                                                                   model=input.model,
                                                                   sites=input.sites,
                                                                   BC=input.BC,
                                                                   static_pars=static_pars,
                                                                   vmax=vmax
                                                                   )
                        elif dim==1:
                              axes=["Real energy", "Imaginary energy"]
                              E_grid_re = np.unique(np.real(E_grid))
                              E_grid_im = np.unique(np.imag(E_grid))
                              par_lists={}
                              par_lists["Real energy"] = E_grid_re
                              par_lists["Imaginary energy"] = E_grid_im

                              viz.plot_observable_vs_two_parameters(obs_name="average LDOS",
                                                                    obs=ldos_all_avg[site], 
                                                                    pars=axes,
                                                                    par_lists=par_lists,
                                                                    model=input.model,
                                                                    sites=input.sites,
                                                                    BC=input.BC,
                                                                    static_pars=static_pars,
                                                                    vmax=vmax
                                                                    )   


            if input.plot_dynamics==True and input.plot_correlation==True:


                  # ALL TIMES IN SAME FILE:
                  #corr_mat_avg_all_times = ino.load_obs_new(model=input.model,
                  #                         sites=input.sites,
                  #                         pars=pars,
                  #                         BC=input.BC,
                  #                         obs_name="zz_correlation_matrix",
                  #                         format=".npy",
                  #                         averaging_method=input.averaging_method,
                  #                         verbose=input.verbose)

                  corr_len_dyn_avg_arr = []
                  times = utils.get_times(initial_time=input.initial_time, 
                                          final_time=input.final_time,
                                          time_points=input.time_points,
                                          time_spacing=input.time_spacing)
                  
                  for idx, t in enumerate(times):
                        s = str(t)[:str(t).find('.') + 3]
                        t = np.round(t,3)
                        print(f"Generating plots for the zz correlation matrix at time {t}...")
                        try:
                              # DIFFERENT TIMES IN SEPARATE FILES:
                              corr_mat_avg = ino.load_obs(model=input.model,
                                                     sites=input.sites,
                                                     pars=pars,
                                                     BC=input.BC,
                                                     obs_name="zz_correlation_matrix_time_{0}*".format(s),
                                                     format=".npy",
                                                     averaging_method=input.averaging_method,
                                                     verbose=input.verbose)
                                                     #verbose=True)

                              # ALL TIMES IN SAME FILE:
                              #print(np.shape(corr_mat_avg_all_times))
                              #corr_mat_avg = corr_mat_avg_all_times[idx]
                              #print(np.shape(corr_mat_avg))
                              viz.plot_dynamics_correlation(corr=corr_mat_avg,
                                                            input=input,
                                                            pars=pars,
                                                            t=t
                                                            )         
                              
                              if not input.model=="disordered-XXZ-bath":
                                    raise NotImplementedError("The model you have chosen has no bath!")
                        
                              if input.bath_averaged_correlation_zz_dyn==True:
                                    LA = int(input.Ham_par7)
                                    print(f"Calculating disorder-averaged bath-averaged correlation function at time {t}...")
                                    bath_avg_corr_func_dyn_avg = obs.bath_averaged_zz_correlation_function(corr_mat=corr_mat_avg,
                                                                                                     LA=LA,
                                                                                                     sites=input.sites,
                                                                                                     averaging_method=input.averaging_method)
                                                                              
                                    if input.correlation_length_dyn==True:
                                          print(f"Calculating disorder-averaged correlation decay length at time {t}...")
                                          popt = obs.correlation_decay_length(sites=LA,
                                                                        corr_func=np.abs(bath_avg_corr_func_dyn_avg)[:LA],
                                                                        fit_func=input.correlation_length_dyn_fit,
                                                                        LA=LA
                                                                        )
                                          #print(popt)
                                          corr_len_dyn_avg_arr.append(1.0/popt[1])
                              
                                    print(f"Generating disorder-averaged plot for the bath-averaged zz correlation function at time {t}...")
                                    # exp_fit = fit.simple_exp(x=np.linspace(1,LA,LA),
                                    #                          mu=LA,
                                    #                          c=popt[0],
                                    #                          xi=popt[1])
                                    exp_fit = np.exp(fit.linear_fit(x=np.linspace(1,LA,LA),
                                                       C=LA,
                                                       A=popt[0],
                                                       B=popt[1]))

                                    viz.plot_observable_vs_one_parameter(obs_name=["Disorder-averaged, bath-averaged correlation function at time {0}".format(t),"Exponential fit"],
                                                                   obs=[np.abs(bath_avg_corr_func_dyn_avg),exp_fit],
                                                                   par="site",
                                                                   par_list=[np.linspace(1,input.sites,input.sites), np.linspace(1,LA,LA)],
                                                                   model=input.model,
                                                                   sites=input.sites,
                                                                   BC=input.BC,
                                                                   static_pars=pars,
                                                                   logy=True,
                                                                   math_expr_y=r"$|g^{(2)}(i)|$"
                                                                   )
                        except:
                              print("Input error!")
                              corr_len_dyn_avg_arr.append(corr_len_dyn_avg_arr[-1])      
                  
                  # Save corr_len_dyn_avg_arr:
                  ino.save_obs(model=input.model,
                               sites=input.sites,
                               pars=pars, 
                               BC=input.BC,
                               obs_name="corr_len_dyn_avg_arr",
                               obs_value=corr_len_dyn_avg_arr
                               )     

                  print(f"Generating disorder-averaged plot for the correlation length as a function of time...")
                  static_pars = pars.copy()
                  static_pars["t_init"] = input.initial_time
                  static_pars["t_fin"] = input.final_time
                  static_pars["t_points"] = input.time_points
                  viz.plot_observable_vs_one_parameter(obs_name="Disorder-averaged correlation length",
                                                       obs=corr_len_dyn_avg_arr,
                                                       par="time",
                                                       par_list=times,
                                                       model=input.model,
                                                       sites=input.sites,
                                                       BC=input.BC,
                                                       static_pars=static_pars,
                                                       logx=True,
                                                       #vmin=0,
                                                       #vmax=1.5
                                                       )

      return 
############################################################################################################





############################################################################################################
if __name__=="__main__":
      Ham = main("input.dat")
############################################################################################################