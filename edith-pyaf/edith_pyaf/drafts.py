#!/usr/bin/env python
############################################################################################
##### This module is used to test out new things.
############################################################################################


####################################
###########  IMPORTS   #############
####################################
import numpy as np
from math import factorial as fac
from math import comb as binomial
####################################




# COLEXICOGRAPHIC ORDERING FOR MULTIPLE OCCUPATION (BOSONS)

# Note that we use colexicographic ordering for bosons too to be consistent with the colexicographic ordering used for fermions and spins.
# Colexicographic means that we read the entries from RIGHT (first site) to LEFT.

######################################################################################
def binomial(n,k):
    """
    Calculates the binomial coefficient n chooses k.

    """
    if n < 0 or k < 0 or k > n: 
        return 0
    b = 1
    for i in range(k): 
        b = b*(n-i)/(i+1)
    return int(b)
######################################################################################

######################################################################################
def occ_bosons(N, L):
    return binomial(N+L-1, N)
######################################################################################

######################################################################################
def lexRank_multi(vec, L):
    N = np.sum(vec)
    idx_start = occ_bosons(N=N, L=L)






######################################################################################
def lexRank(s, get_abs_rank=True, verbose=False):

    # Get string length:
    n = len(s)

    # Get maximum value in the string (to determine possible occupations):
    max_val = max(int(c) for c in s)

    # Count occurrences of each digit:
    counts = [0] * (max_val + 1)
    for j in s:
        counts[int(j)] += 1
    
    # Initialize relative rank to 0.
    rel_rank = 0
    
    # Loop over each element of the string, and check how many elements 
    # TO ITS RIGHT are smaller:
    for i in range(n):
        if verbose:
            print(f"Character at position {i}: {s[i]}")
        less_than = 0
        for j in range(i + 1, n):
            if int(s[i]) > int(s[j]): 
                less_than += 1
        if verbose:
            print(f"Number of characters with smaller order: {less_than}")

        # Count the occurrences of each digit from the current position onwards:
        d_count = [0] * (max_val + 1)
        for j in range(i, n):
            d_count[int(s[j])] += 1
        if verbose:
            print(f"Occurrences from position {i} onwards: {d_count}")

        # Calculate the total number of permutations of those elements
        d_fac = 1
        for ele in d_count:
            d_fac *= fac(ele)
        
        # Calculate the number of possible arrangements of the elements to the right
        rel_rank += (fac(n - i - 1) * less_than) // d_fac
        if verbose:
            print("\n")

    if verbose:
        print(rel_rank)

    if get_abs_rank:
        abs_rank = 0
        for i in range(sum(counts)):
            abs_rank += binomial(n + i - 1, i)

        abs_rank += rel_rank
    else:
        abs_rank = None

    return rel_rank, abs_rank
######################################################################################


######################################################################################
def get_configuration_from_index(ind0, n, m, L):

    ivec = np.zeros(L, dtype=int)
    nvec = np.zeros(L, dtype=int)

    ind = ind0
    max_i = m + n - 1

    for i in range(1, m):
        for j in range(m - i, max_i + 1):
            ivec[i] = j
            if ind <= matrix_of_binomial_coefficients[j - 1][m - i]:
                ivec[i] = j - 1
                break

        ind = ind - matrix_of_binomial_coefficients[ivec[i] - 1][m - i]
        max_i = ivec[i]

    nvec[0] = n + m - 1 - ivec[1]

    for i in range(2, m):
        nvec[i - 1] = ivec[i - 2] - ivec[i - 1] - 1

    nvec[m - 1] = ivec[m - 2] - 1

    return nvec

# Example usage:
# Define the matrix_of_binomial_coefficients, ind0, n, and m based on your specific context
######################################################################################




######################################################################################
def generate_composition(n,k):
    """
    Enumerate all the weak compositions of n into k elements. Equivalently, enumerate all possible configurations of bosons into k sites.
    
    Parameters:
    -----------
    n, int: number of particles.
    k, int: number of sites.


    Returns:
    --------
    A list of enumerated compositions.


    Notes:
    ------
    The algorithm works by iteratively shifting one from the leftmost nonzero position towards the right, 
    while also "recycling" the rightmost nonzero number. E.g. for n=3, k=4
    
    1)  3000 --> take 1 from rightmost nonzero number excluding the last (1st position), take value from last position (0, 4th position), add them to the (1st+1=2nd) position --> 2100
    2)  2100 --> take 1 from rightmost nonzero number excluding the last (2nd position), take value from last position (0, 4th position), add them to the (2nd+1=3rd) position --> 2010
    3)  2010 --> take 1 from rightmost nonzero number excluding the last (3rd position), take value from last position (0, 4th position), add them to the (3rd+1=4th) position --> 2001
    4)  2001 --> take 1 from rightmost nonzero number excluding the last (1st position), take value from last position (1, 4th position), add them to the (1st+1=2nd) position --> 1200
    5)  1200 --> take 1 from rightmost nonzero number excluding the last (2nd position), take value from last position (0, 4th position), add them to the (2nd+1=3rd) position --> 1110
    6)  1110 --> take 1 from rightmost nonzero number excluding the last (3rd position), take value from last position (0, 4th position), add them to the (3rd+1=4th) position --> 1101
    7)  1101 --> take 1 from rightmost nonzero number excluding the last (2nd position), take value from last position (1, 4th position), add them to the (2nd+1=3rd) position --> 1020
    8)  1020 --> take 1 from rightmost nonzero number excluding the last (3rd position), take value from last position (0, 4th position), add them to the (3rd+1=4th) position --> 1011
    9)  1011 --> take 1 from rightmost nonzero number excluding the last (3rd position), take value from last position (1, 4th position), add them to the (3rd+1=4th) position --> 1002
    10) 1002 --> take 1 from rightmost nonzero number excluding the last (1st position), take value from last position (2, 4th position), add them to the (1st+1=2nd) position --> 0300
    11) 0300 --> take 1 from rightmost nonzero number excluding the last (2nd position), take value from last position (0, 4th position), add them to the (2nd+1=3rd) position --> 0210
    12) 0210 --> take 1 from rightmost nonzero number excluding the last (3rd position), take value from last position (0, 4th position), add them to the (3rd+1=4th) position --> 0201
    13) 0201 --> take 1 from rightmost nonzero number excluding the last (2nd position), take value from last position (1, 4th position), add them to the (2nd+1=3rd) position --> 0120
    14) 0120 --> take 1 from rightmost nonzero number excluding the last (3rd position), take value from last position (0, 4th position), add them to the (3rd+1=4th) position --> 0111
    15) 0111 --> take 1 from rightmost nonzero number excluding the last (3rd position), take value from last position (1, 4th position), add them to the (3rd+1=4th) position --> 0102
    16) 0102 --> take 1 from rightmost nonzero number excluding the last (2nd position), take value from last position (2, 4th position), add them to the (2nd+1=3rd) position --> 0030
    17) 0030 --> take 1 from rightmost nonzero number excluding the last (3rd position), take value from last position (0, 4th position), add them to the (3rd+1=4th) position --> 0021
    18) 0021 --> take 1 from rightmost nonzero number excluding the last (3rd position), take value from last position (1, 4th position), add them to the (3rd+1=4th) position --> 0012
    19) 0012 --> take 1 from rightmost nonzero number excluding the last (3rd position), take value from last position (2, 4th position), add them to the (3rd+1=4th) position --> 0003
    20) 0003 --> end of loop

    """
    # Initialize, first entry will be [k, 0, 0, ..., 0]
    x = [0] * n
    x[0] = k

    # Loop by moving integers by one:
    while True:
        print(x)
        
        # Consider the last element:
        v = x[-1]
        # When the last element is k, we have concluded the enumeration:
        if (k==v):
            break

        # Set the last element to zero:
        x[-1] = 0
        
        # Consider the second last element: 
        j = -2

        # Check one by one if all the elements (from the rightmost) are zero.
        # Stop only when you find a nonzero element:
        while (0==x[j]):
            j -= 1

        # Reduce the rightmost nonzero element by one.
        x[j] -= 1        
        # Add the one you took from the righmost nonzero element to the element to his right plus the value of the rightmost element (v).   
        x[j+1] = 1 + v      
    
######################################################################################



######################################################################################
def generate_composition_reverse(n,k):
    """
    Enumerate all the weak compositions of n into k elements, starting from |000...0k>. Equivalently, enumerate all possible configurations of bosons into k sites.
    
    Parameters:
    -----------
    n, int: number of particles.
    k, int: number of sites.


    Returns:
    --------
    A list of enumerated compositions.


    Notes:
    ------
    The algorithm works by iteratively shifting one from the rightmost nonzero position towards the left, 
    while also "recycling" the leftmost nonzero number. E.g. for n=3, k=4:
    
    1)  0003 --> take 1 from leftmost nonzero number excluding the first (4th position), take value from first position (0, 1st position), add them to the (4th-1=3rd) position --> 0012
    2)  0012 --> take 1 from leftmost nonzero number excluding the first (3rd position), take value from first position (0, 1st position), add them to the (3rd-1=2nd) position --> 0102
    3)  0102 --> take 1 from leftmost nonzero number excluding the first (2nd position), take value from first position (0, 1st position), add them to the (2nd-1=1st) position --> 1002
    4)  1002 --> take 1 from leftmost nonzero number excluding the first (4th position), take value from first position (1, 1st position), add them to the (1st-1=3rd) position --> 0021
    5)  0021 --> take 1 from leftmost nonzero number excluding the first (3rd position), take value from first position (0, 1st position), add them to the (2nd-1=3rd) position --> 0111
    6)  0111 --> take 1 from leftmost nonzero number excluding the first (2nd position), take value from first position (0, 1st position), add them to the (2nd-1=1st) position --> 1011
    7)  1011 --> take 1 from leftmost nonzero number excluding the first (3rd position), take value from first position (1, 1st position), add them to the (3rd-1=2nd) position --> 0201
    8)  0201 --> take 1 from leftmost nonzero number excluding the first (2nd position), take value from first position (0, 1st position), add them to the (2nd-1=1st) position --> 1101
    9)  1101 --> take 1 from leftmost nonzero number excluding the first (2nd position), take value from first position (1, 1st position), add them to the (2nd-1=1st) position --> 2001
    10) 2001 --> take 1 from leftmost nonzero number excluding the first (4th position), take value from first position (2, 1st position), add them to the (4th-1=3rd) position --> 0030
    11) 0030 --> take 1 from leftmost nonzero number excluding the first (3rd position), take value from first position (0, 1st position), add them to the (3rd-1=2nd) position --> 0120
    12) 0120 --> take 1 from leftmost nonzero number excluding the first (2nd position), take value from first position (0, 1st position), add them to the (2nd-1=1st) position --> 1020
    13) 1020 --> take 1 from leftmost nonzero number excluding the first (3rd position), take value from first position (1, 1st position), add them to the (3rd-1=2nd) position --> 0210
    14) 0210 --> take 1 from leftmost nonzero number excluding the first (2nd position), take value from first position (0, 1st position), add them to the (2nd-1=1st) position --> 1110
    15) 1110 --> take 1 from leftmost nonzero number excluding the first (2nd position), take value from first position (1, 1st position), add them to the (2nd-1=1st) position --> 2010
    16) 2010 --> take 1 from leftmost nonzero number excluding the first (3rd position), take value from first position (2, 1st position), add them to the (3rd-1=2nd) position --> 0300
    17) 0300 --> take 1 from leftmost nonzero number excluding the first (2nd position), take value from first position (0, 1st position), add them to the (2nd-1=1st) position --> 1200
    18) 1200 --> take 1 from leftmost nonzero number excluding the first (2nd position), take value from first position (1, 1st position), add them to the (2nd-1=1st) position --> 2100
    19) 2100 --> take 1 from leftmost nonzero number excluding the first (2nd position), take value from first position (2, 1st position), add them to the (2nd-1=1st) position --> 3000
    20) 3000 --> end of loop

    """

    def first_state_lex_multi(n, k):
        x = [0] * k
        x[-1] = n
        return x
    
    def last_state_lex_multi(n, k):
        x = [0] * k 
        x[0] = n
        return x

    def next_state_lex_multi(x):

        v = x[0]
        x[0] = 0
        j = 1
        while (0==x[j]):
            j += 1
        x[j] -= 1
        x[j-1] = 1 + v

        return x

    x = first_state_lex_multi(n, k)    
    x_last = last_state_lex_multi(n, k)

    while x != x_last:
        print("composition:", x)  
        rel, abs = colexRank_multi(x)
        print("rel index:", rel)
        print("abs index:", abs)
      
        x = next_state_lex_multi(x)

    print("composition:", x)  
    rel, abs = colexRank_multi(x)
    print("rel index:", rel)
    print("abs index:", abs)

######################################################################################


######################################################################################
def lexRank_multi(conf):
    
    # reverse order in configuration vector to get colex rank instead of direct lex rank:
    N = np.sum(conf)
    L = len(conf)

    idx = 0
    for k in range(1,L):
        idx += binomial(N + L - 1 - k - np.sum(conf[:k]), L - k)

    return idx
######################################################################################


######################################################################################
def colexRank_multi(conf):
    """
    Returns the colexicographic rank of a multi-occupation state.

    Examples:
    ---------
    |1020> --> rel_rank = 12, abs_rank = 12 + (2+4-1 choose 2) + (1+4-1 choose 1)
    
    """
    N = np.sum(conf)
    L = len(conf)

    previous_conf_num=0
    for n in range(1,N):
        previous_conf_num += binomial(n + L - 1, n)

    rel_rank = 0
    for k in range(1,L):
        rel_rank += binomial(N + k - 1 - np.sum(conf[k:]), k)
    
    abs_rank = previous_conf_num + rel_rank

    return rel_rank, abs_rank
######################################################################################


from math import comb  # comb is a function to calculate binomial coefficients

######################################################################################
def ColexRank(C):
    rank = 0
    N = np.sum(C)
    L = len(C)

    for i in range(L, 0, -1):
        ci = C[L-i]  # Position i corresponds to index L-i in 0-indexed list
        if ci > 0:
            rank += comb(ci, L-i)
    
    return rank
######################################################################################

######################################################################################
def number_of_restricted_compositions(N, L, M):
    """
    Reference: 
    ----------
    https://math.stackexchange.com/questions/1163866/weak-k-compositions-with-each-part-less-than-j
    R. Stanley, Enumerative Combinatorics.

    """

    num = 0
    for r in range(N+1):
        for s in range(int(N/(M+1))+1):
            if r+s*(M+1) == N:
                num += (-1)**s*binomial(L + r - 1, r)*binomial(L, s)
    
    return num
######################################################################################






######################################################################################
if __name__ == "__main__":
    # L=4
    # N=5
    # print(occ_bosons(N=N, L=L))

    # print(lexRank_multi([3,0,0]))
    # print(lexRank_multi([2,1,0]))
    # print(lexRank_multi([2,0,1]))
    # print(lexRank_multi([1,2,0]))
    # print(lexRank_multi([1,1,1]))
    # print(lexRank_multi([1,0,2]))
    # print(lexRank_multi([0,3,0]))
    # print(lexRank_multi([0,2,1]))
    # print(lexRank_multi([0,1,2]))
    # print(lexRank_multi([0,0,3]))
    # print("\n")

    # print(colexRank_multi([0,0,3]))
    # print(colexRank_multi([0,1,2]))
    # print(colexRank_multi([1,0,2]))
    # print(colexRank_multi([0,2,1]))
    # print(colexRank_multi([1,1,1]))
    # print(colexRank_multi([2,0,1]))
    # print(colexRank_multi([0,3,0]))
    # print(colexRank_multi([1,2,0]))
    # print(colexRank_multi([2,1,0]))
    # print(colexRank_multi([3,0,0]))

    # print("\n")
    # generate_composition(4,3)

    print(binomial(1 + 3 - 1, 1))
    print(binomial(2 + 3 - 1, 2))
    print(binomial(3 + 3 - 1, 3))
    print(binomial(4 + 3 - 1, 4))


    print("\n")
    for n in range(1,5):
        generate_composition_reverse(n,8)
        print("\n")

    print("This number should be 7:")
    print(number_of_restricted_compositions(3, 3, 2))

    print("This number should be 16:")
    print(number_of_restricted_compositions(3, 4, 2))
    print("This number should be 20:")
    print(number_of_restricted_compositions(3, 4, 3))
    print("This number should be 20:")
    print(number_of_restricted_compositions(3, 4, 8))



    # TODO:

    # 1) Function to generate iteratively all basis vectors in colexicographic order
    
    # https://stackoverflow.com/questions/55258576/generate-lexicographic-series-efficiently-in-python

    # 2) Function to extract the rank of a given basis vector (i.e. rank of n-composition)




    # 3) Funcion to unrank, i.e. get the basis vector from the rank (inverse of 2).

    #https://www.mdpi.com/1999-4893/14/3/97
    # https://mathoverflow.net/questions/144891/computing-the-lexicographic-indices-of-integer-partition