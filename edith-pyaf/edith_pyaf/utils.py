#!/usr/bin/env python
############################################################################################
##### This file contains utility routines.
############################################################################################

####################################
###########  IMPORTS   #############
####################################
from __future__ import division
from __future__ import print_function
import platform
import subprocess
import psutil
import numpy as np
import scipy as sp
import itertools as it
import sys
import os 
import shutil
import glob
import re
from math import comb
####################################


################################
######     SUBROUTINES    ######
################################
######################################################################################
def spin_dim(spin, sites):
    """
    Returns the Hilbert space dimension of the spin problem with the given spin size.

    """
    spin_size = len(np.arange(-spin,spin+1),1)
    dim = spin_size**sites

    return dim

######################################################################################


######################################################################################
def get_full_Hamiltonian(H_sparse):
    """
    Builds full Hamiltonian matrix from sparse representation.

    Parameters:
    ----------
    H_sparse, instance of class Hamiltonian: the Hamiltonian object.
    
    """ 

    # Get data from object:
    rows = H_sparse.rows
    cols = H_sparse.cols
    mat_els = H_sparse.mat_els
    dim = np.amax(rows)+1

    # Check whether columns = rows:
    assert len(cols) == len(rows)

    # Initialize array:
    Ham = np.zeros(shape=(dim,dim), dtype=np.complex128)

    # Loop over all non-zero elements and save them into the full array:
    for i in range(len(rows)):
        Ham[rows[i],cols[i]] = mat_els[i]

    return Ham
######################################################################################

######################################################################################
def get_parameters_from_dict(pars):
    
    par_keys=[]
    par_vals=[]
    
    for par in pars:
        par_keys.append(par)
        par_vals.append(pars[par])

    return par_keys, par_vals
######################################################################################


######################################################################################
def calculate_overlap(state1, state2):
    """
    """
    
    return np.vdot(state1, state2)
######################################################################################

######################################################################################
def convert_state_from_str_to_arr(state_str):
    """
    
    Notes:
    ------

    At present, only states that are not written as a superposition are accepted, e.g.:

    "01100100110"
    "20401032001"
    etc.
    
    """

    l = len(state_str)
    state = np.zeros(l)

    for c, idx in enumerate(state_str):
        state[idx] = int(c)

    # normalize:
    state /= np.sum(np.abs(state)**2)

    return state
######################################################################################

######################################################################################
def convert_state_from_bin_to_vec(state_str, basis_dim, sector):
    """
    
    Notes:
    ------

    At present, only states that are not written as a superposition are accepted, e.g.:

    "01100100110"
    "20401032001"
    etc.
    
    """

    if sector is not None:
        rank, _ = lexRank(state_str)   # determine index (rank) in local sector
    
    elif sector is None:
        _, rank = lexRank(state_str)   # determine index (rank) in full Hilbert space
    
    vec = np.zeros(basis_dim)     # initialize vector
    vec[rank] = 1               # fill vector at position corresponding to the rank
        
    return vec
######################################################################################

######################################################################################
def convert_state_from_list_to_vec(state_list, stats, basis_dim, sector):
    """
    
    Notes:
    ------

    At present, only states that are not written as a superposition are accepted, e.g.:

    fermions, spin 1/2s: [0,1,1,0,0,1,0,0,1,1,0]
    bosons: [2,0,4,0,1,0,3,2,0,0,1]
    
    This routine does not work for spinful fermions.
    
    """

    if stats=="fermions" or stats=="spin-0.5":

        if sector is not None:
            # Convert list to binary string:
            state_str = ''.join(map(str, state_list))
            rank, _ = lexRank(state_str)   # determine index (rank) in local sector
        
        elif sector is None:
            _, rank = lexRank(state_str)   # determine index (rank) in full Hilbert space

    elif stats=="bosons":
        if sector is not None:
            rank, _ = colexRank_multi(state_list)  # determine index (rank) in local sector

        elif sector is None:
            rank, _ = colexRank_multi(state_list)  # determine index (rank) in full Hilbert space
    
    vec = np.zeros(basis_dim)     # initialize vector
    vec[rank] = 1               # fill vector at position corresponding to the rank
        
    return vec
######################################################################################


######################################################################################
def compute_average(obs_list,
                    averaging_method):
    
    #raise NotImplementedError(f"Averaging for observable {obs_name} not yet implemented.")

    # Determine if the observable is 1-dimensional (scalars) or 2-dimensional (matrices)
    ndim=get_dimensionality(obs_list)
    
    # Scalar data
    if ndim==0:
        if averaging_method=="arithmetic-mean":
            obs = np.mean(np.array(obs_list))
        elif averaging_method=="median":
            obs = np.median(np.array(obs_list))
        elif averaging_method=="geometric-mean":
            raise NotImplementedError(f"Geometric mean not yet implemented.")
        elif averaging_method=="harmonic-mean":
            raise NotImplementedError(f"Harmonic mean not yet implemented.")
    
    # Vectors or Matrices
    elif ndim==1 or ndim==2 or ndim==3:
        #print("First entry:", obs_list[0])
        # Stack matrices along a new axis
        stacked = np.stack(obs_list, axis=-1)
        if averaging_method=="arithmetic-mean":
            obs = np.mean(stacked, axis=-1)
        elif averaging_method=="median":
            obs = np.median(stacked, axis=-1)
        elif averaging_method=="geometric-mean":
            raise NotImplementedError(f"Geometric mean not yet implemented.")
        elif averaging_method=="harmonic-mean":
            raise NotImplementedError(f"Harmonic mean not yet implemented.")
    
    else:
        raise NotImplementedError(f"Averaging for tensors with rank higher than 3 not yet verified!")

    return obs

######################################################################################

######################################################################################
def get_dimensionality(lst):
    arr = np.array(lst)
    #print("Array dimensionality:", arr.ndim)
    return arr.ndim - 1
######################################################################################


######################################################################################
def binomial(n,k):
    """
    Calculates the binomial coefficient n chooses k.

    """
    if n < 0 or k < 0 or k > n: 
        return 0
    b = 1
    for i in range(k): 
        b = b*(n-i)/(i+1)
    return int(b)
######################################################################################

######################################################################################
def combo_rank(n,S):
    """
    Given a combination S with n equal elements, calculates the corresponding rank (index) 
    in a lexicographic ordering.

    """
    k = len(S)
    if k == 0 or k == n: 
        return 0
    
    j = S[0]
    if k == 1: 
        return j
    
    S = [x-1 for x in S]
    if not j: 
        return combo_rank(n-1,S[1:])
    
    return binomial(n-1,k-1)+combo_rank(n-1,S)
######################################################################################

######################################################################################
def combos_with_reps(X,k):
    """
    Given a list of elements X, calculate all possible combinations of k elements. 

    """
    n = len(X)
    if k < 0 or k > n: 
        return []
    if not k: 
        return [[]]
    if k == n: 
        return [X]
    
    c = [X[:1] + S for S in combos_with_reps(X[1:],k-1)] + combos_with_reps(X[1:],k)

    return c 
######################################################################################

######################################################################################
def combos_without_reps(X,k):
    """
    Given a list of elements X, calculate all possible combinations of k elements. 

    """
    c = combos_with_reps(X,k)
    c.sort()
    cwr =  list((k for k,_ in it.groupby(c)))

    return cwr
######################################################################################


######################################################################################
def fac(n):
    """
    Calculates factorial of n.

    """
    if n == 0 or n == 1:
        return 1
	
    return n * fac(n - 1)
######################################################################################


######################################################################################
def generate_all_binaries(n):
    
    if n==1:
        return ["0", "1"]
    
    else:
        bins = generate_all_binaries(n-1)
        
    new_bins = []   
    for b in bins:
        new_bins.append(b+"0")
        new_bins.append(b+"1")

    return new_bins
######################################################################################

######################################################################################
def number_of_restricted_compositions(N, L, M):
    """
    Calculates the number of weak compositions of integer N into L parts, each restricted to be <= M.
    
    Reference: 
    ----------
    https://math.stackexchange.com/questions/1163866/weak-k-compositions-with-each-part-less-than-j
    R. Stanley, Enumerative Combinatorics.

    """

    num = 0
    for r in range(N+1):
        for s in range(int(N/(M+1))+1):
            if r+s*(M+1) == N:
                num += (-1)**s*binomial(L + r - 1, r)*binomial(L, s)
    
    return num
######################################################################################

######################################################################################
# Python program to find all the possible combinations of
# k-bit numbers with n-bits set where 1 <= n <= k
 
 
# Function to find all combinations k-bit numbers with
# n-bits set where 1 <= n <= k
def findBitCombinations(k):
    
    # maximum allowed value of k
    assert (k < 16)
    
    # DP lookup table
    DP = [[[] for _ in range(16)] for _ in range(16)]
 
    bins = []
    # DP[k][0] will store all k-bit numbers
    # with 0 bits set (All bits are 0's)
    str = ""
    for len in range(k+1):
        DP[len][0].append(str)
        str += "0"
 
    # fill DP lookup table in bottom-up manner
    # DP[k][n] will store all k-bit numbers
    # with n-bits set
    for len in range(1, k+1):
        for n in range(1, len+1):
            # prefix 0 to all combinations of length len-1
            # with n ones
            for str in DP[len-1][n]:
                DP[len][n].append("0" + str)
 
            # prefix 1 to all combinations of length len-1
            # with n-1 ones
            for str in DP[len-1][n-1]:
                DP[len][n].append("1" + str)
 
    # print all k-bit binary strings with
    # n-bit set
    for n in range(k+1):
        for str in DP[k][n]:
            #print(str, end=" ")
            bins.append(str)
        #print()

    return bins
######################################################################################


######################################################################################
#Function to calculate 
#rank of the String.
def lexRank(s, get_abs_rank=True, verbose=False):
    """
    Calculates lexicographic rank of a string of 0s and 1s.
     
    Notes:
    ------ 
    This gives both the relative (fixed numbers of 0s and 1s for given length N) and the
    absolute (increasing number of 1s for a given length N) rank, e.g. for N=4:

    | string      | relative | absolute |
    --------------|----------|----------|
    |0000>        |    0     |    0     |
    |0001>        |    0     |    1     |
    |0010>        |    1     |    2     |
    |0100>        |    2     |    3     |
    |1000>        |    3     |    4     |
    |0011>        |    0     |    5     |
    |0101>        |    1     |    6     |
    |0110>        |    2     |    7     |
    |1001>        |    3     |    8     |
    |1010>        |    4     |    9     |
    |1100>        |    5     |    10    |
    |0111>        |    0     |    11    |
    |1011>        |    1     |    12    |
    |1101>        |    2     |    13    |
    |1110>        |    3     |    14    |
    |1111>        |    0     |    15    |
    
    Note that the number of combinations of k 1's in a string of length N is the 
    binomial coefficient, (n chooses k).
    
    """

    # Get string length:
    n = len(s)

    # Get number of 1's:
    n1=0
    for j in s:
        if j=="1":
            n1+=1
    
    # Initialize relative rank to 0.
    rel_rank = 0
    
    # Loop over each element of the string, and check how many elements 
    # TO ITS RIGHT are smaller:
    for i in range(n):
        if verbose==True:
            print(f"Character at position {i}: {s[i]}")
        less_than = 0
        for j in range(i + 1, n):
            if int(s[i]) > int(s[j]): 
                less_than += 1
        if verbose==True:
            print(f"Number of characters with smaller order: {less_than}")

        # Count the number of characters (0's and 1's) to the right of the current element (included):      
        d_count = [0] * 2
        for j in range(i, n):
            d_count[int(s[j])] += 1
        if verbose==True:
            print(f"There are {d_count[0]} 0's and {d_count[1]} 1's from position {i} onwards.")

        # Calculate the total number of permutations of those elements
        # (the 0's permute among themselves, the 1's among themselves).
        # This is needed to avoid overcounting.
        d_fac = 1
        for ele in d_count:
            d_fac *= fac(ele)
        
        # Calculate the number of possible arrangements of the elements to the right
        rel_rank += (fac(n - i - 1) * less_than) // d_fac
        if verbose==True:
            print("\n")

    if verbose==True:
        print(rel_rank)

    if get_abs_rank == True:
        abs_rank = 0
        for i in range(n1):
            abs_rank += binomial(n,i)

        abs_rank += rel_rank
    else:
        abs_rank = None

    return rel_rank, abs_rank
######################################################################################

######################################################################################
def colexRank_multi(s):
    """
    Returns the colexicographic rank of a multi-occupation state.

    Examples:
    ---------
    |1020> --> rel_rank = 12, abs_rank = 12 + (2+4-1 choose 2) + (1+4-1 choose 1)
    
    """
    N = np.sum(s)
    L = len(s)

    previous_conf_num=0
    for n in range(0,N):
        previous_conf_num += binomial(n + L - 1, n)

    rel_rank = 0
    for k in range(1,L):
        rel_rank += binomial(N + k - 1 - np.sum(s[k:]), k)
    
    abs_rank = previous_conf_num + rel_rank

    return rel_rank, abs_rank
######################################################################################


######################################################################################
def is_state_allowed(state, max_occ):
    if np.any(np.array(state) > max_occ):
        return False
    else:
        return True
######################################################################################


######################################################################################
def get_binary_from_rel_rank(rank, sites, sz): #(k,n,r)

    Nup = int(sites/2.0 + int(sz))
    restRank = rank
    bin = ''
    onesLeft = Nup
    for j in range(sites):
        binoVal = binomial(sites-1-j, onesLeft)
        if restRank >= binoVal:
            bin += '1'
            onesLeft -= 1
            restRank -= binoVal
        else:
            bin += '0'
    return bin
    #return str(tuple(ret))
######################################################################################



######################################################################################
def find_ith_combination(n, k, index):
    result = []
    for i in range(n):
        if k == 0:
            result.append('1')
        elif comb(n - 1, k) <= index:
            result.append('1')
            index -= comb(n - 1, k)
            n -= 1
        else:
            result.append('0')
            k -= 1
            n -= 1
    return ''.join(result)
######################################################################################
# # Example usage
# n = 6
# k = 3
# index = 5  # This is the index in the list of combinations

# binary_entry = find_ith_combination(n, k, index)
# print(binary_entry)

######################################################################################
def find_string_from_rank(rank, length):
    result = ''
    total_combinations = 2 ** length

    if rank >= total_combinations:
        return "Rank exceeds the total number of combinations for the given length."

    for i in range(length - 1, -1, -1):
        divisor = 2 ** i
        if rank >= divisor:
            result += '1'
            rank -= divisor
        else:
            result += '0'

    return result

######################################################################################

from math import comb
######################################################################################

def find_string_from_rank2(rank, num_zeros, num_ones):
    total_length = num_zeros + num_ones
    result = []

    for i in range(total_length):
        zeros_remaining = num_zeros
        combinations_without_zero = comb(zeros_remaining + num_ones - 1, num_ones - 1)

        if rank < combinations_without_zero:
            result.append('0')
            num_zeros -= 1
        else:
            result.append('1')
            rank -= combinations_without_zero
            num_ones -= 1

    return ''.join(result)
######################################################################################


######################################################################################
def ground_state_Hubbard_half_filling(t, U, N):
    """
    Evaluates the formula for the ground state energy of the 1D Hubbard model at 
    half filling by Lieb and Wu.

    References:
    -----------
    * E. H. Lieb and F. Y. Wu, 
      Absence of Mott transition in an exact solution of the short-range one-band model in one dimension, 
      Phys. Rev. Lett. 20, 1445 (1968).

    """
    def integrand(x):
        return sp.special.j0(x)*sp.special.j1(x)/(x*(1+np.exp(x*U/(2*t))))
    
    I, err = sp.integrate.quad(integrand,0,100)
    E = -4*t*N*I
    err = -4*t*N*err

    return E, err
######################################################################################

######################################################################################
def enumerated_product(*args):
    yield from zip(it.product(*(range(len(x)) for x in args)), it.product(*args))
######################################################################################

######################################################################################
def check_dir(dir_name):
    """
    Creates the given folder if missing.

    """
    if not os.path.exists(dir_name):
        os.makedirs(dir_name)

    return
######################################################################################

######################################################################################
def get_time_diff(ti,tf):

    diff = np.abs(tf-ti)
    # seconds:
    if diff<60:
        return str(diff)+" seconds"
    # minutes:
    elif diff>60 and diff<=3600:
        return str(diff/60.0)+" minutes"
    # hours:
    elif diff>3600 and diff<=86400:
        return str(diff/3600.0)+" hours"
    # days:
    elif diff>86400:
        return str(diff/86400.0)+" days"
######################################################################################

######################################################################################
def get_matching_files(directory, pattern):

    # Escape square brackets in the pattern because glob uses square brackets [] as special characters to match any one of the enclosed characters.
    pattern = re.sub('([\[\]])','[\\1]', pattern)
    # Use glob to find all files matching the pattern
    search_pattern = os.path.join(directory, pattern)
    #print(search_pattern)
    matching_files = glob.glob(search_pattern)
    #print(matching_files)

    # Extract filenames from the full paths
    matching_files = [file for file in matching_files]
    return matching_files

######################################################################################


######################################################################################
def truncate_string(str, length=251):
    return str[:length]
######################################################################################

######################################################################################
def center_of_mass(x, y):
    """
    Calculates the center of mass of a 1-dimensional array y
    with weights on the points specified by the array x.

    Parameters:
    -----------
    x, array-like: The positions of the points.
    y, array-like: The weights (masses) at each point.

    Returns:
    --------
    com, float: The center of mass.

    """
    x = np.asarray(x)
    y = np.asarray(y)
    
    if len(x) != len(y):
        raise ValueError("The length of x and y must be the same")
    
    total_mass = np.sum(y)
    if total_mass == 0:
        raise ValueError("Total mass cannot be zero")
    
    com = np.sum(x * y) / total_mass

    return com
    
######################################################################################

######################################################################################
def clean_directories(dir):

    # Iterate through each directory
    for directory in dir:
            if os.path.exists(directory):
                for filename in os.listdir(directory):
                        file_path = os.path.join(directory, filename)
                        try:
                            if os.path.isfile(file_path) or os.path.islink(file_path):
                                os.unlink(file_path)
                            elif os.path.isdir(file_path):
                                shutil.rmtree(file_path)
                        except Exception as e:
                            print(f'Failed to delete {file_path}. Reason: {e}')
            else:
                print(f'Directory {directory} does not exist.')

######################################################################################

######################################################################################
def ensure_list(input_data):
    if isinstance(input_data, list):
        return input_data
    else:
        return [input_data]
######################################################################################

######################################################################################
def ensure_list_of_lists(input_data):
    if isinstance(input_data, list) or isinstance(input_data, np.ndarray):        #[[1,2,3],[4,5,6]]
        if isinstance(input_data[0], list) or isinstance(input_data[0], np.ndarray):
            return input_data
        else:
            return [input_data]             # [1,2,3] --> [[1,2,3]]
    else:
        return [[input_data]]
######################################################################################

######################################################################################
def get_times(initial_time, final_time, time_points, time_spacing):
    if time_spacing == "linear":
        times = np.linspace(initial_time, final_time, time_points)
    elif time_spacing == "exp":
        times = 10**(np.linspace(initial_time, final_time, time_points))
    return times
######################################################################################

######################################################################################
def memory_checks(input):

    system, available_memory = check_available_memory()
    print(f"Available Memory on system {system}: {available_memory} GB")

    required_memory = check_required_memory(input)
    print(f"Memory requested for the computation (estimate): {required_memory} GB")

    if required_memory >  available_memory:
        print("It looks like you don't have enough local memory to run such a computation! The computation will be aborted now.")
        try:
            # Some code that might want to exit
            sys.exit(1)
        except SystemExit as e:
            print(f"Exiting with status: {e.code}")
    return
######################################################################################


######################################################################################
def check_required_memory(input):

    model = input.model
    sites = input.sites

    if model=="transverse-Ising" or model=="staggered-Heisenberg":
        mem = (2**sites)**2*24/1024/1024/1024
    elif model=="dipolar-Aubry-Andre" or model=="fermionic-interacting-Hatano-Nelson":
        N=input.Ham_par1
        mem = binomial(sites, N)**2*24/1024/1024/1024
    elif model=="Hubbard":
        sz=input.Ham_par1
        N=input.Ham_par2
        mem = binomial(sites, int(N/2 + sz))*binomial(sites, N/2 - sz)*24/1024/1024/1024
    elif model=="disordered-XXZ-bath":
        sz=input.Ham_par8
        mem = binomial(sites, int(sites/2 + sz))**2*24/1024/1024/1024
    elif model=="XXZ-DM":
        sz=input.Ham_par1
        mem = binomial(sites, int(sites/2 + sz))**2*24/1024/1024/1024
    elif model=="bosonic-interacting-Hatano-Nelson":
        N=int(input.Ham_par1)
        max_occ=input.Ham_par8
        if max_occ is None or max_occ >= N:
            mem = binomial(sites + N - 1, N)**2*24/1024/1024/1024
        else:
            max_occ = int(max_occ)    
            mem = number_of_restricted_compositions(N=N, L=sites, M=max_occ)**2*24/1024/1024/1024

    return mem

######################################################################################

######################################################################################
def check_available_memory():
    # Detect the operating system
    system = platform.system()
    available_memory = None

    if system == "Linux" or system == "Darwin":  # Darwin is macOS

        # Use subprocess to read memory information
        # WARNING: this might be buggy!
        if system == "Linux":
            with open('/proc/meminfo', 'r') as mem:
                lines = mem.readlines()

            meminfo = {}
            for line in lines:
                parts = line.split(':')
                if len(parts) == 2:
                    key = parts[0].strip()
                    value = parts[1].strip().split()[0]
                    meminfo[key] = int(value)

            available_memory_kb = meminfo.get('MemAvailable', 0)
            available_memory_gb = available_memory_kb / 1024 /1024

        elif system == "Darwin":  # macOS

            # Detect the operating system
            system = platform.system()
            
            # Get memory information using psutil
            memory_info = psutil.virtual_memory()

            # Extract relevant details
            available_memory_gb = memory_info.available / (1024 ** 3)

    elif system == "Windows":

        raise NotImplemented("Memory check for Windows not yet implemented!")

    else:
        print("Unsupported Operating System")
    
    return system, available_memory_gb
######################################################################################

######################################################################################
def initial_state_checks(stats,
                         bath,
                         mode_sys,
                         mode_bath,
                         state,
                         Ham,
                         **kwargs):
    """

    Parameters:
    -----------
    stats, str: the quantum statistics of the problem.
    bath, bool: flag to distinguish cases with and without bath.
    mode_sys, str: the type of input for the initial state of the (sub)system.
    mode_bath, str: the type of input for the initial state of the bath (only relevant is bath=True).
    state, list or numpy array: the initial state for the (sub)system.
    Ham, instance of Hamiltonian class: the Hamiltonian object containing information.

    **kwargs:
    - state_bath, list or numpy array: the initial state for the (sub)system.
    
    """
    if not bath:
        if mode_sys=="list":
            # check that the density adds up to the particle number:
            if stats=="spinless-fermions" or stats=="bosons":
                if any(x < 0 for x in state):
                    raise IOError(f"The density in the provided initial state list {state} cannot be negative!")

                N_init_state = np.sum(state)
                if int(N_init_state) != int(Ham.pars["N"]):
                    N=Ham.pars["N"]
                    raise IOError(f"The total particle number {N_init_state} in the provided initial state density list does not match the chosen particle number {N}!")
            
            elif stats=="spin-0.5":
                if any(x != 0 and x != 1 for x in state):
                    raise IOError(f"For spin 1/2's, the initial state can only contain zeros or ones, e.g. 11010110!")


        elif mode_sys=="file":
            # check that the initial state has the correct length and is normalized:
            if np.abs(np.linalg.norm(state) - 1) > 1e-16:
                raise IOError(f"The provided initial state is not normalized to one!")
            if Ham.sector is not None:
                if len(state) != Ham.sector_dim:
                    raise IOError(f"The provided initial state in the file of length {len(state)} does not match the sector dimension {Ham.sector_dim}!")
            else:
                if len(state) != Ham.Hilbert_dim:
                    raise IOError(f"The provided initial state in the file of length {len(state)} does not match the Hilbert space dimension {Ham.Hilbert_dim}!")


    else:
        state_bath = kwargs.get("state_bath", None)
        L_bath = kwargs.get("L_bath", 0)

        if mode_sys=="list":

            # check that the density adds up to the particle number:
            if stats=="spinless-fermions" or stats=="bosons":
                if any(x < 0 for x in state):
                    raise IOError(f"The density in the provided initial state list {state} cannot be negative!")

                N_init_state = np.sum(state)
                if int(N_init_state) > int(Ham.pars["N"]):
                    N=Ham.pars["N"]
                    raise IOError(f"The total particle number {N_init_state} in the provided initial state density list is bigger than the chosen particle number {N}!")
            
            elif stats=="spin-0.5":
                if any(x != 0 and x != 1 for x in state):
                    raise IOError(f"For spin 1/2's, the initial state can only contain zeros or ones, e.g. 11010110!")
            
            if mode_bath=="list":

                # Check bath length:
                if L_bath != len(state_bath):
                    raise IOError("The bath length and the length of the initial state for the bath do not coincide!")
                
                # Check that the density adds up to the particle number:
                if stats=="spinless-fermions" or stats=="bosons":
                    if any(x < 0 for x in state):
                        raise IOError(f"The density in the provided initial state list {state} cannot be negative!")
                    if any(x < 0 for x in state_bath):
                        raise IOError(f"The density in the provided initial state list {state_bath} cannot be negative!")   
                                
                    N_init_state_sys = np.sum(state)
                    N_init_state_bath = np.sum(state_bath)

                    if int(N_init_state_sys + N_init_state_bath) != int(Ham.pars["N"]):
                        N=Ham.pars["N"]
                        raise IOError(f"The total particle number {N_init_state_sys + N_init_state_bath} in the provided initial state density lists does not match the chosen particle number {N}! Make sure to allocate the correct number of particles across system and bath.")

                elif stats=="spin-0.5":
                    if any(x != 0 and x != 1 for x in state):
                        raise IOError(f"For spin 1/2's, the initial state list {state} can only contain zeros or ones, e.g. 11010110!")
                    if any(x != 0 and x != 1 for x in state_bath):
                        raise IOError(f"For spin 1/2's, the initial state list {state_bath} can only contain zeros or ones, e.g. 11010110!")
                    
            elif mode_bath=="file":
                # Check normalization of bath vector:
                if np.abs(np.linalg.norm(state_bath) - 1) > 1e-8:
                    raise IOError(f"The provided initial state is not normalized to one but has instead norm {np.linalg.norm(state_bath)}!")
                
                # Get initial number of particles in the system and its length:
                N_sys = np.sum(state)
                L_sys = len(state)

                # Get compatible number of particles in the bath and corresponding space dimension:
                L_b = Ham.sites - L_sys

                if L_b != L_bath:
                    raise IOError("The bath length and the length of the initial state for the bath (calculated from the initial state of the system) do not coincide!")

                if stats=="bosons":
                    if Ham.pars["N"] is not None:
                        N_b = int(Ham.pars["N"] - N_sys)
                        if Ham.pars["max_occ"] is None:
                            dim_b = binomial(L_b + N_b - 1, N_b)

                        else:
                            dim_b = number_of_restricted_compositions(N=N_b, L=L_b, M=int(Ham.pars["max_occ"]))
                    else:
                        raise NotImplementedError("Initial bath state definition from file for all N sectors not yet implemented!")
                    
                elif stats=="spinless-fermions":
                    if Ham.pars["N"] is not None:
                        N_b = int(Ham.pars["N"] - N_sys)
                        dim_b = binomial(L_b, N_b)
                    else:
                        raise NotImplementedError("Initial bath state definition from file for all N sectors not yet implemented!")
                    
                elif stats=="spin-0.5":
                    if Ham.pars["sz"] is not None: # Only calculate things for one sz-sector:
                        Nup = int(L_b/2.0 + int(Ham.pars["sz"]))
                        dim_b = binomial(L_b, Nup)
                    else:   # Calculate all sz-sectors:
                        raise NotImplementedError("Initial bath state definition from file for all sz sectors not yet implemented!")

                if len(state_bath) != dim_b:
                    raise IOError(f"The initial state for the bath has a length {len(state_bath)} incompatible with required dimension {dim_b} obtained from the initial state of the system!")

        elif mode_sys=="file" or mode_sys=="self":
            
            # Check normalization of system vector:
            if np.abs(np.linalg.norm(state) - 1) > 1e-8:
                raise IOError(f"The provided initial state is not normalized to one but has instead norm {np.linalg.norm(state)}!")
            
            if mode_bath=="list":
                
                # Get initial number of particles in the bath and its length:
                N_b = np.sum(state_bath) 
                L_b = len(state_bath)

                # Check bath length:
                if L_bath != L_b:
                    raise IOError("The bath length and the length of the initial state for the bath do not coincide!")
                
                # Get compatible number of particles in the system and corresponding space dimension:
                L_sys = Ham.sites - L_b
                if stats=="bosons":
                    if Ham.pars["N"] is not None:
                        N_sys = int(Ham.pars["N"] - N_b)
                        if Ham.pars["max_occ"] is None:
                            dim_sys = binomial(L_sys + N_sys - 1, N_sys)
                        else:
                            dim_sys = number_of_restricted_compositions(N=N_sys, L=L_sys, M=int(Ham.pars["max_occ"]))
                    else:
                        raise NotImplementedError("Initial bath state definition from file for all N sectors not yet implemented!")
                    
                elif stats=="spinless-fermions":
                    if Ham.pars["N"] is not None:
                        dim_sys = binomial(L_sys, N_sys)
                    else:
                        raise NotImplementedError("Initial bath state definition from file for all N sectors not yet implemented!")
                    
                elif stats=="spin-0.5":
                    if Ham.pars["sz"] is not None: # Only calculate things for one sz-sector:
                        Nup = int(L_sys/2.0 + int(Ham.pars["sz"]))
                        dim_sys = binomial(L_sys, Nup)
                    else:   # Calculate all sz-sectors:
                        raise NotImplementedError("Initial bath state definition from file for all sz sectors not yet implemented!")

                if len(state) != dim_sys:
                    raise IOError(f"The initial state for the system has a length {len(state)} incompatible with required dimension {dim_sys} obtained from the initial state of the bath!")


            elif mode_bath=="file":

                # Check normalization of system vector:
                if np.abs(np.linalg.norm(state_bath) - 1) > 1e-8:
                    raise IOError(f"The provided initial state is not normalized to one but has instead norm {np.linalg.norm(state_bath)}!")
                
                print("You are reading initial states for both the system and the bath as vectors saved in two separate files. With access to these two vectors alone, it is difficult to perform meaningful checks that they are compatible with each other. Proceed with caution.")





######################################################################################
