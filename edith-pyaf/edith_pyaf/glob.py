#!/usr/bin/env python
############################################################################################
##### This file collects global parameters and metadata.
############################################################################################

####################################
###########  IMPORTS   #############
####################################
import numpy as np
import sys
####################################


####################################
##########   METADATA   ############
####################################
__author__      =   "Paolo Molignini"
__copyright__   =   "Copyright 2023, University of Stockholm"
__version__     =   "1.0.1"
####################################


######################################
##### GLOBAL OBJECTS AND OPTIONS #####
######################################
######################################################################################
np.set_printoptions(threshold=sys.maxsize)
np.set_printoptions(linewidth=1000)
disc_clist = ['fuchsia', 'purple', 'mediumblue',  'lightskyblue', 'aqua', 'mediumspringgreen', 'green', 'yellowgreen', 'gold', 'orange', 'peru', 'red', 'brown' ]
######################################################################################
