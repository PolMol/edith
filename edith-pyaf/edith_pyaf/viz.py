#!/usr/bin/env python
############################################################################################
##### This file collects visualization routines.
############################################################################################


####################################
###########  IMPORTS   #############
####################################
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm, Normalize
import matplotlib as mpl
import numpy as np
import datetime
 
from pylab import * #for movies
import matplotlib.animation as animation #for movies
from mpl_toolkits.axes_grid1 import make_axes_locatable

# Custom modules:
from . import hams
from . import utils
from . import input_output as ino
from . import input as inp
from . import dynamics as dyn
####################################

######################################################################################
def visualize_hamiltonian(Ham):
    """
    Plots the matrix Hamiltonian in the occupation basis for the given sector.

    """

    # Checks:
    utils.check_dir("plots")

    # Get filename and title
    filename = assemble_viz_filename(model=Ham.model, 
                                     pars=Ham.pars, 
                                     sites=Ham.sites, 
                                     BC=Ham.BC,
                                     folder="plots/",
                                     quantity="Hamiltonian")
    title = assemble_title(model=Ham.model, 
                           sites=Ham.sites, 
                           BC=Ham.BC, 
                           pars=Ham.pars,
                           quantity="Hamiltonian")

    # Get Hamiltonian
    if Ham.sparse:
        H = utils.get_full_Hamiltonian(Ham)
    else:
        H = Ham.matrix

    # Plot(s):
    if np.amax(np.imag(H)) > 1e-14:  # Complex Hamiltonian
        fig, ax = plt.subplots(1, 2)
        plot_re = ax[0].imshow(np.real(H))
        ax[0].set_title("Real part")
        divider_re = make_axes_locatable(ax[0])
        cax_re = divider_re.append_axes("right", size="5%", pad=0.1)
        cbar_re = plt.colorbar(plot_re, cax=cax_re)

        plot_im = ax[1].imshow(np.imag(H))
        ax[1].set_title("Imaginary part")
        divider_im = make_axes_locatable(ax[1])
        cax_im = divider_im.append_axes("right", size="5%", pad=0.1)
        cbar_im = plt.colorbar(plot_im, cax=cax_im)   

        plt.suptitle(title)     
        plt.tight_layout(rect=[0, 0, 1, 0.95])  # Adjusted for title space

        # Adjust layout to add space between the plots
        plt.subplots_adjust(wspace=0.4)

    else:
        fig = plt.figure()
        ax = fig.add_subplot(111)
        plot = ax.imshow(np.real(H))
        cbar = plt.colorbar(plot, ax=ax)
        plt.suptitle(title)
        plt.tight_layout()

    # Save/show and close:
    plt.savefig(filename, bbox_inches='tight')
    #plt.show()
    plt.close()

    return
######################################################################################


######################################################################################
def visualize_eigenvalues(evals, Ham):
    """
    Plots the eigenvalues in the complex plane.

    Notes:
    ------

    """

    # Checks:
    utils.check_dir("plots")

    # Sort eigenvalues according to real part, then imaginary part:
    sorted_evals = np.sort(evals)

    # Get filename and title
    filename = assemble_viz_filename(model=Ham.model, 
                                     sites=Ham.sites, 
                                     BC=Ham.BC,
                                     pars=Ham.pars, 
                                     folder="plots/",
                                     quantity="Eigenvalues")
    title = assemble_title(model=Ham.model, 
                           sites=Ham.sites, 
                           BC=Ham.BC, 
                           pars=Ham.pars,
                           quantity="Eigenvalue distribution")
    
    # Colors for the scatterplot:
    colormap = plt.cm.rainbow
    colors = [colormap(i) for i in np.linspace(0, 1, len(evals))]

    # Plot
    fig = plt.figure()
    ax1 = fig.add_subplot(111)
    for idx, eval in enumerate(sorted_evals):
        r = np.real(eval)
        i = np.imag(eval)
        plot = ax1.scatter(r, i, s=60, marker='o', color=colors[idx])
    #ax1.set_ylim([-1.5, 1.5])

    # Colorbar
    sm = mpl.cm.ScalarMappable(cmap=colormap, norm=mpl.colors.Normalize(0, len(evals)-1))
    sm.set_array([])
    cbar = plt.colorbar(sm, ax=ax1)
    cbar.set_label('Eigenvalue index')

    # Labels:
    ax1.set_xlabel(r"Real part")
    ax1.set_ylabel(r"Imaginary part")
    plt.suptitle(title)

    # Save/show and close:
    plt.savefig(filename, bbox_inches='tight')
    #plt.show()
    plt.close()

    return
######################################################################################



######################################################################################
def visualize_eigenstate(evec_Hilbert, index, Ham):
    """
    Plots the amplitude squared for the index-th eigenstate.

    Notes:
    ------
    This only works for spinless fermions or spin systems.

    """

    # Checks:
    utils.check_dir("plots")
    
    # Get filename and title
    filename = assemble_viz_filename(model=Ham.model, 
                                     sites=Ham.sites, 
                                     BC=Ham.BC,
                                     pars=Ham.pars, 
                                     folder="plots/", 
                                     quantity="{0}-th eigenstate".format(index))
    title = assemble_title(model=Ham.model, 
                           sites=Ham.sites, 
                           BC=Ham.BC, 
                           pars=Ham.pars,
                           quantity="{0}-th eigenstate".format(index))

    # Get eigenvector in visualizable format:
    if Ham.stats=="spinless-fermions" or Ham.stats=="spin-0.5":
        state = hams.get_state_population_spinless(Ham=Ham, state=evec_Hilbert)
    elif Ham.stats=="bosons":
        state = hams.get_state_population_bosons(Ham=Ham, state=evec_Hilbert)

    # Plot
    fig = plt.figure()
    ax1 = fig.add_subplot(111)
    if Ham.stats=="spinless-fermions":
        label="Absolute value squared"
        ax1.set_ylabel(r"Eigenstate probability")    
    elif Ham.stats=="bosons":
        label="Density"
        ax1.set_ylabel(r"Eigenstate population")
    elif Ham.stats=="spin-0.5":
        label="spin component"
        ax1.set_ylabel(r"Spin z-component")
    plot1 = ax1.plot(np.linspace(1,len(state),len(state)), state, marker='o', color='blue', ms=5, label=label)
    ax1.set_xlabel(r"site $j$")
    plt.legend(loc='best')
    plt.suptitle(title)

    # Save/show and close:
    plt.savefig(filename, bbox_inches='tight')
    #plt.show()
    plt.close()

    #print("Sum check:", np.sum(np.abs(evec)**2))

    return
######################################################################################


######################################################################################
def visualize_eigenstate_multicomponent(evec_Hilbert, index, Ham):

    """
    
    Notes:
    -----
    This only works for multicomponent fermionic or bosonic systems, e.g. spinful fermions.

    """

    # Checks:
    utils.check_dir("plots")

    # Get filename and title
    filename = assemble_viz_filename(model=Ham.model, 
                                     sites=Ham.sites, 
                                     BC=Ham.BC,
                                     pars=Ham.pars, 
                                     folder="plots/", 
                                     quantity="{0}-th eigenstate".format(index))
    title = assemble_title(model=Ham.model, 
                           sites=Ham.sites, 
                           BC=Ham.BC, 
                           pars=Ham.pars,
                           quantity="{0}-th eigenstate".format(index))

    # Get eigenvector in visualizable format:
    #print(evec_Hilbert)
    evec_up, evec_down = hams.get_state_population_spinful(evec_Hilbert, Ham=Ham)
    
    # Plot:
    fig = plt.figure()
    ax1 = fig.add_subplot(111)
    plot_up = ax1.plot(np.linspace(1,len(evec_up),len(evec_up)), evec_up, marker='o', color='blue', ms=5, label="Spin up (absolute value squared)")
    plot_down = ax1.plot(np.linspace(1,len(evec_down),len(evec_down)), evec_down, marker='x', color='crimson', ms=5, label="Spin down (absolute value squared)")
    ax1.set_xlabel(r"site $j$")
    ax1.set_ylabel(r"Eigenstate probability")
    plt.legend(loc='best')
    plt.suptitle(title)

    # Save/show and close:
    plt.savefig(filename, bbox_inches='tight')
    #plt.show()
    plt.close()

    #print("Sum check:", np.sum(np.abs(evec)**2))

    return
######################################################################################

######################################################################################
def plot_time_evolution_spinless(time_evolved_state, Ham, input):
    """
    Creates a plot of the time evolution of a many-body state. Each projection onto different basis states is plotted.

    Parameters:
    ----------
    time_evolved_state, list of (list of) floats: the many-body state in the eigenbasis as time evolves.

    Ham, instance of Hamiltonian class: the (possibly sparse) Hamiltonian.

    input, object of class Input: the input parameters.


    Notes:
    ------
    
    
    """

    # Checks:
    utils.check_dir("plots")
    sector=Ham.sector
    if sector is None:
        raise NotImplementedError("Time evolution for all sectors not implemented!")

    # Extracting input:
    initial_state_name=input.initial_state_name
    initial_time=input.initial_time
    final_time=input.final_time
    time_points=input.time_points
    time_spacing=input.time_spacing
    sector_dic = Ham.basis_states[sector]

    # Get filename and title
    filename = assemble_viz_filename(model=Ham.model, 
                                     sites=Ham.sites, 
                                     BC=Ham.BC,
                                     pars=Ham.pars, 
                                     folder="plots/", 
                                     quantity="time-evolution",
                                     dynamics=True,
                                     initial_state_name=initial_state_name,
                                     initial_time=initial_time,
                                     final_time=final_time,
                                     time_points=time_points,
                                     time_spacing=time_spacing)
    title = assemble_title(model=Ham.model, 
                           sites=Ham.sites, 
                           BC=Ham.BC, 
                           pars=Ham.pars,
                           quantity="Many-body dynamics",
                           initial_state_name=initial_state_name)

    # Set up x values:
    times = utils.get_times(initial_time=initial_time, 
                            final_time=final_time, 
                            time_points=time_points, 
                            time_spacing=time_spacing)

    # Create figure
    fig = plt.figure()
    # Plot of real/imaginary part:    
    ax1 = fig.add_subplot(211)
    ax2 = fig.add_subplot(212)
    colormap = plt.cm.rainbow #nipy_spectral, Set1,Paired 
    colors = [colormap(i) for i in np.linspace(0, 1, Ham.sector_dim)]
    #print("Ham.sector_dim", Ham.sector_dim)
    
    for bin, ranks in sector_dic.items():

        idx_b = ranks['rel_rank']   # index of basis state in the given sector
        #print("idx_b", idx_b)
        #print("time evolved state:", time_evolved_state)
        #print("time evolved state:", time_evolved_state[:, idx_b])
        plot1_re = ax1.plot(times, np.real(time_evolved_state[:,idx_b]), marker='o', color=colors[idx_b], ms=5, lw=2, linestyle='solid', label=bin+"(Real part)")
        plot1_im = ax1.plot(times, np.imag(time_evolved_state[:,idx_b]), marker='x', color=colors[idx_b], ms=5, lw=2, linestyle='dashed', label=bin+"(Imaginary part)")
        plot2 = ax2.plot(times, np.abs(time_evolved_state[:,idx_b])**2, marker='o', color=colors[idx_b], ms=5, lw=2, linestyle='solid', label=bin)
    
    # Analytical results for benchmark:
    if Ham.model=="dipolar-Aubry-Andre" and Ham.sites==2 and Ham.pars["N"]==1:
        time_evolved_state_anal = dyn.dipolar_Aubry_Andre_dynamics_check(Delta=Ham.pars["Delta"],
                                                                         beta=Ham.pars["beta"],
                                                                         phi=Ham.pars["phi"],
                                                                         hopp=Ham.pars["t"], 
                                                                         initial_time=initial_time,
                                                                         final_time=final_time,
                                                                         time_points=time_points,
                                                                         time_spacing=time_spacing)
        plot1_comp1_re_anal = ax1.plot(times, np.real(time_evolved_state_anal[:,0]), marker='o', color="dimgray", ms=5, lw=2, linestyle='solid', label="01"+"(Real part - analytical)")
        plot1_comp1_im_anal = ax1.plot(times, np.imag(time_evolved_state_anal[:,0]), marker='x', color="gray", ms=5, lw=2, linestyle='dashed', label="01"+"(Imaginary part - analytical)")
        plot1_comp2_re_anal = ax1.plot(times, np.real(time_evolved_state_anal[:,1]), marker='o', color="gainsboro", ms=5, lw=2, linestyle='solid', label="10"+"(Real part - analytical)")
        plot1_comp2_im_anal = ax1.plot(times, np.imag(time_evolved_state_anal[:,1]), marker='x', color="whitesmoke", ms=5, lw=2, linestyle='dashed', label="10"+"(Imaginary part - analytical)")
        plot2_comp1_anal = ax2.plot(times, np.abs(time_evolved_state_anal[:,0])**2, marker='o', color="dimgray", ms=5, lw=2, linestyle='solid', label="01 (analytical)")
        plot2_comp2_anal = ax2.plot(times, np.abs(time_evolved_state_anal[:,1])**2, marker='o', color="gainsboro", ms=5, lw=2, linestyle='solid', label="10 (analytical)")

    
    elif Ham.model=="dipolar-Aubry-Andre" and Ham.sites==4 and Ham.pars["N"]==1 and Ham.pars["Delta"]==1.0 and Ham.pars["beta"]==0.1591549431 and Ham.pars["phi"]==1.0 and Ham.pars["t"]==1.0:
        time_evolved_state_anal = dyn.dipolar_Aubry_Andre_dynamics_check2(initial_time=initial_time,
                                                                          final_time=final_time,
                                                                          time_points=time_points,
                                                                          time_spacing=time_spacing)
        plot2_comp1_anal = ax2.plot(times, np.abs(time_evolved_state_anal[:,0])**2, marker='o', color="dimgray", ms=5, lw=2, linestyle='solid', label="0001 (analytical)")
        plot2_comp2_anal = ax2.plot(times, np.abs(time_evolved_state_anal[:,1])**2, marker='o', color="gray", ms=5, lw=2, linestyle='solid', label="0010 (analytical)")
        plot2_comp3_anal = ax2.plot(times, np.abs(time_evolved_state_anal[:,2])**2, marker='o', color="gainsboro", ms=5, lw=2, linestyle='solid', label="0100 (analytical)")
        plot2_comp4_anal = ax2.plot(times, np.abs(time_evolved_state_anal[:,3])**2, marker='o', color="whitesmoke", ms=5, lw=2, linestyle='solid', label="1000 (analytical)")

    # Labels
    ax1.set_xlabel(r"Time $t$")
    ax1.set_ylabel(r"State amplitude")
    ax2.set_xlabel(r"Time $t$")
    ax2.set_ylabel(r"State probability")
    if Ham.sector_dim <= 16:
        ax1.legend(loc='best', ncols=4)
        ax2.legend(loc='best', ncols=4)
    plt.suptitle(title)

    # Save/show and close:
    plt.savefig(filename, bbox_inches='tight')
    #plt.show()
    plt.close()

    if Ham.model=="dipolar-Aubry-Andre":
        if (Ham.sites==2 and Ham.pars["N"]==1) or (Ham.sites==4 and Ham.pars["N"]==1):
            print("time_evolved_state:")
            print(time_evolved_state)
            print("time_evolved_state_anal:")
            print(time_evolved_state_anal)

    return
######################################################################################


######################################################################################
def plot_density_vs_t_spinless(time_evolved_state, Ham, initial_state_name, initial_time, final_time, time_points, time_spacing, **kwargs):
    """
    Creates a plot of the time evolution of the many-body state density at each site as a heatmap.

    Parameters:
    ----------
    time_evolved_state, list of (list of) floats: the many-body state in the eigenbasis as time evolves.

    Ham, instance of Hamiltonian class: the (possibly sparse) Hamiltonian.

    initial_state_name, str: the string encoding of the type of initial state.

    initial_time, float: the initial time in the propagation.

    time_points, int: number of times in the dynamics.

    time_spacing, str: type of time spacing to use in the time interval.

    final_time, float: the final time in the propagation.


    Notes:
    ------
    
    
    """
    # Checks
    utils.check_dir("plots")

    #Kwargs:
    logx = kwargs.get("logx", False)

    # Get filename and title
    filename = assemble_viz_filename(model=Ham.model, 
                                     sites=Ham.sites, 
                                     BC=Ham.BC,
                                     pars=Ham.pars, 
                                     folder="plots/", 
                                     quantity="density-dynamics",
                                     dynamics=True,
                                     initial_state_name=initial_state_name,
                                     initial_time=initial_time,
                                     final_time=final_time,
                                     time_points=time_points,
                                     time_spacing=time_spacing
                                     )
    title = assemble_title(model=Ham.model, 
                           sites=Ham.sites, 
                           BC=Ham.BC, 
                           pars=Ham.pars,
                           quantity="Many-body state",
                           initial_state_name=initial_state_name)

    dens_arr = []
    for state in time_evolved_state:
        # Get state in visualizable format:
        if Ham.stats=="spinless-fermions" or Ham.stats=="spin-0.5":
            dens_arr.append(hams.get_state_population_spinless(Ham=Ham,state=state))
        elif Ham.stats=="bosons":
            dens_arr.append(hams.get_state_population_bosons(Ham=Ham,state=state))

    dens_arr = np.real(np.array(dens_arr))

    # Plot and labels:
    fig = plt.figure()
    ax1 = fig.add_subplot(111)
    times = utils.get_times(initial_time=initial_time, 
                            final_time=final_time, 
                            time_points=time_points, 
                            time_spacing=time_spacing)
    X, Y = np.meshgrid(times, np.arange(1,Ham.sites+1,1))
    plot = ax1.pcolormesh(X, Y, dens_arr.T, cmap='inferno', shading='auto')
    plt.colorbar(plot, ax=ax1)
    ax1.set_ylim([0.5, Ham.sites+0.5])
    ax1.set_xlabel(r"Time $t$")
    ax1.set_ylabel(r"Site $j$")
    if logx:
        ax1.set_xscale("log")
    plt.suptitle(title)
    
    # Save/show and close:
    #plt.show()
    plt.savefig(filename, bbox_inches='tight')    
    plt.close()

    return
######################################################################################

######################################################################################
def plot_density_vs_t_spinful(time_evolved_state, Ham, initial_state_name, initial_time, final_time, time_points, time_spacing):
    """
    Creates a plot of the time evolution of the many-body state density at each site as a heatmap.

    Parameters:
    ----------
    time_evolved_state, list of (list of) floats: the many-body state in the eigenbasis as time evolves.

    Ham, instance of Hamiltonian class: the (possibly sparse) Hamiltonian.

    initial_state_name, str: the string encoding the type of initial state.

    initial_time, float: the initial time in the propagation.

    final_time, float: the final time in the propagation.

    time_points, int: number of times in the dynamics.

    time_spacing, str: type of time spacing to use in the time interval.


    Notes:
    ------
    
    
    """
    # Checks
    utils.check_dir("plots")

    # Get filename and title
    filename = assemble_viz_filename(model=Ham.model, 
                                     sites=Ham.sites, 
                                     BC=Ham.BC,
                                     pars=Ham.pars, 
                                     folder="plots/", 
                                     quantity="density-dynamics",
                                     dynamics=True,
                                     initial_state_name=initial_state_name,
                                     initial_time=initial_time,
                                     final_time=final_time,
                                     time_points=time_points,
                                     time_spacing=time_spacing)
    title = assemble_title(model=Ham.model, 
                           sites=Ham.sites,
                           BC=Ham.BC, 
                           pars=Ham.pars,
                           quantity="Many-body state",
                           initial_state_name=initial_state_name)

    # Get the density for spin up and spin down components separately
    dens_arr_up = []
    dens_arr_down = []
    for state in time_evolved_state:
        # Get state in visualizable format:
        dens_up, dens_down = hams.get_state_population_spinful(Ham=Ham,state=state)
        dens_arr_up.append(dens_up)
        dens_arr_down.append(dens_down)

    dens_arr_up = np.array(dens_arr_up)
    dens_arr_down = np.array(dens_arr_down)

    # Plot:
    fig = plt.figure(figsize=(14,8))
    ax1 = fig.add_subplot(211)
    times = utils.get_times(initial_time=initial_time, 
                            final_time=final_time, 
                            time_points=time_points, 
                            time_spacing=time_spacing)
    X, Y = np.meshgrid(times, np.arange(1,Ham.sites+1,1))
    plot = ax1.pcolormesh(X, Y, dens_arr_up.T, cmap='inferno')
    plt.colorbar(plot, ax=ax1)
    ax1.set_ylim([1, Ham.sites])
    ax1.set_xlabel(r"Time $t$")
    ax1.set_ylabel(r"Site $j$")
    ax1.set_title("Spin up component")

    ax2 = fig.add_subplot(212)
    plot2 = ax2.pcolormesh(X, Y, dens_arr_down.T, cmap='inferno')
    plt.colorbar(plot2, ax=ax2)
    ax2.set_ylim([1, Ham.sites])
    ax2.set_xlabel(r"Time $t$")
    ax2.set_ylabel(r"Site $j$")
    ax2.set_title("Spin down component")

    plt.suptitle(title)
    
    # Save/show and close:
    #plt.show()
    plt.savefig(filename, bbox_inches='tight')    
    plt.close()

    return
######################################################################################

# TODO: kind of duplicate of Hamiltonian visualization and time-dependent correlation matrix visualization --> merge
######################################################################################
def plot_correlation_gs(Ham, corr):
    """
    Plots the ground state correlation matrix at every site.

    """

    # Checks:
    utils.check_dir("plots")

    # Get filename and title
    filename = assemble_viz_filename(model=Ham.model, 
                                     pars=Ham.pars, 
                                     sites=Ham.sites, 
                                     BC=Ham.BC,
                                     folder="plots/",
                                     quantity="Correlation-matrix-gs")
    title = assemble_title(model=Ham.model, 
                           sites=Ham.sites, 
                           BC=Ham.BC, 
                           pars=Ham.pars,
                           quantity="Ground state correlation matrix")

    # Plot:
    fig = plt.figure()
    ax = fig.add_subplot(111)
    plot = ax.imshow(corr)
    ax.set_xlabel(r"site $i$")
    ax.set_ylabel(r"site $j$")
    plt.suptitle(title)
    cbar = plt.colorbar(plot, ax=ax)
    
    # Save/show and close:
    plt.savefig(filename, bbox_inches='tight')
    #plt.show()
    plt.close()

    return
######################################################################################

######################################################################################
def plot_dynamics_correlation(corr, input, pars, t):
    """
    Creates a plot of the correlation matrix at time t as a heatmap.

    Parameters:
    ----------
    corr, numpy array: the correlation matrix at time t.

    input, object of class Input: the input parameters.

    pars, list of str: the names of the Hamiltonian parameters.

    t, float: the time at which to plot the correlation.

    Notes:
    ------
    
    """
    # Extracting metadata for labels etc:
    model = input.model
    sites = input.sites
    par_keys, par_vals = utils.get_parameters_from_dict(pars)
    averaging_method = input.averaging_method
    initial_state_name = input.initial_state_name

    # visualization parameters:
    vmin = input.zz_corr_vmin
    if vmin is None:
        vmin = np.amin(corr)
    vmax = input.zz_corr_vmax
    if vmax is None:
        vmax = np.amax(corr)
    log_plot = input.zz_corr_log_plot
    abs_plot = input.zz_corr_abs_plot

    if averaging_method is not None:
        averaging = True
    else:
        averaging = False
    
    # Setting normalization and title:
    if abs_plot:
        corr = np.abs(corr)
        if log_plot:
            if vmin<0:
                raise ValueError("I cannot plot logarithms of negative correlation!")
            norm = LogNorm(vmin=vmin, vmax=vmax)
            if averaging:
                title = r"$\log \left( |\overline{\left<\sigma_i^z \sigma_j^z \right>_c}| \right)$, $t=$%s"%(np.round(t,3))
            else:
                title = r"$\log \left( |\left<\sigma_i^z \sigma_j^z \right>_c| \right)$, $t=$%s"%(np.round(t,3))
        else:
            if averaging:
                title = r"$|\overline{\left<\sigma_i^z \sigma_j^z \right>_c}|$, $t=$%s"%(np.round(t,3))
            else:
                title = r"$|\left<\sigma_i^z \sigma_j^z \right>_c|$, $t=$%s"%(np.round(t,3))
            norm = Normalize(vmin=vmin, vmax=vmax)
    else:
        if log_plot:
            if averaging:
                title = r"$\log \left( \overline{\left<\sigma_i^z \sigma_j^z \right>_c} \right)$, $t=$%s"%(np.round(t,3))
            else:
                title = r"$\log \left( \left<\sigma_i^z \sigma_j^z \right>_c \right)$, $t=$%s"%(np.round(t,3))

            norm = LogNorm(vmin=vmin, vmax=vmax)
        else:
            if averaging:
                title = r"$\overline{\left<\sigma_i^z \sigma_j^z \right>_c}$, $t=$%s"%(np.round(t,3))
            else:
                title = r"$\left<\sigma_i^z \sigma_j^z \right>_c$, $t=$%s"%(np.round(t,3))
            norm = Normalize(vmin=vmin, vmax=vmax)

    # Plot:
    fig = plt.figure()
    ax = fig.add_subplot(111)
    plot = ax.imshow(corr, norm=norm, cmap='RdBu_r')
    cbar = plt.colorbar(plot, ax=ax)
    ax.set_title(title)

    if averaging:
        filename = "plots/correlation-dynamics-{0}-{1}".format(averaging_method, model)
    else:
        filename = "plots/correlation-dynamics-{0}-".format(model)

    for i in range(len(par_keys)):
        if i==len(par_keys)-1:
            filename = filename + "{0}-{1}-".format(par_keys[i], par_vals[i])
        else:
            filename = filename + "{0}-{1}, ".format(par_keys[i], par_vals[i])
        
    # Add timestamp to make each correlation plot unique:
    curr_dt = datetime.datetime.now()
    timestamp = int(round(curr_dt.timestamp()))
    filename = filename + "sites-{0}-initial-state-{1}-time-{2}-timestamp-{3}".format(sites, initial_state_name, t, timestamp)   
    filename = utils.truncate_string(filename, length=251)    # make sure that the filename is not longer than 255 characters.
    filename = filename + ".png"

    plt.savefig(filename, bbox_inches='tight')
    plt.close()

    return
######################################################################################


######################
####### MOVIES #######
######################
######################################################################################
def movie_time_evolution_spinless(time_evolved_state, Ham, initial_state_name, initial_time, final_time, time_points, time_spacing):
    """
    Creates a movie of the time evolution of a many-body state.

    Parameters:
    ----------
    time_evolved_state, list of (list of) floats: the many-body state in the eigenbasis as time evolves.

    Ham, instance of Hamiltonian class: the (possibly sparse) Hamiltonian.

    initial_state_name, str: the string encoding the type of initial state.

    initial_time, float: the initial time in the propagation.

    final_time, float: the final time in the propagation.

    time_points, int: number of times in the dynamics.

    time_spacing, str: type of time spacing to use in the time interval.

    Notes:
    ------
    
    
    """
    # Checks:
    utils.check_dir("movies")

    # Get filename and title
    filename = assemble_viz_filename(model=Ham.model, 
                                     sites=Ham.sites, 
                                     BC=Ham.BC,
                                     pars=Ham.pars,
                                     folder="movies/",
                                     quantity="time-evolution",
                                     dynamics=True,
                                     initial_state_name=initial_state_name,
                                     initial_time=initial_time,
                                     final_time=final_time,
                                     time_points=time_points,
                                     time_spacing=time_spacing,
                                     extension=".mp4")
    title = assemble_title(model=Ham.model, 
                           sites=Ham.sites, 
                           BC=Ham.BC, 
                           pars=Ham.pars,
                           quantity="Many-body state at time t={0}".format(initial_time),
                           initial_state_name=initial_state_name)

    # Get initial state in visualizable format:
    if Ham.stats=="spinless-fermions":
        state_pop = hams.get_state_population_spinless(Ham=Ham, state=time_evolved_state[0])
    elif Ham.stats=="bosons":
        state_pop = hams.get_state_population_bosons(Ham=Ham, state=time_evolved_state[0])
        
    # Get times:
    times = utils.get_times(initial_time=initial_time, 
                            final_time=final_time, 
                            time_points=time_points, 
                            time_spacing=time_spacing)
    ##############
    # movie part #
    ##############

    # Set up formatting for the movie files
    Writer = animation.writers['ffmpeg']
    writer = Writer(fps=15, metadata=dict(artist='PaoloMolignini'), bitrate=1800)
        
    # Create figure
    fig = plt.figure(figsize=(12,8))

    # First plot:
    ax1 = fig.add_subplot(111)
    plot = ax1.plot(np.linspace(1,len(state_pop),len(state_pop)), state_pop, marker='o', color='blue', ms=5, lw=3, label="Absolute value squared")
    if Ham.stats=="spinless-fermions":
        ax1.set_ylim([-0.1, 1.1])
    elif Ham.stats=="spin-0.5":
        ax1.set_ylim([-0.6, 0.6])
    elif Ham.stats=="bosons":
        ax1.set_ylim([-0.1, Ham.pars["N"]+0.1])
    ax1.set_xlabel(r"site $j$")
    ax1.set_ylabel(r"State probability")
    plt.legend(loc='best')
    plt.suptitle(title)
    plt.tight_layout()

    # Total number of frames:
    frame_number = len(time_evolved_state)
    # Defines the frame speed of the movie (interval between frames in ms):
    speed_movie = 50
    #values of the function to be updated:
    fargs = [ax1, times, time_evolved_state, Ham, initial_state_name]
            
    # Animation iteration routine:
    ani = animation.FuncAnimation(fig, animate_time_evol_spinless, frames=np.arange(0,frame_number), fargs=[fargs], interval=speed_movie, blit=False, repeat=False)    
    ani.save(filename, writer=writer, dpi=500)
    plt.close()

    return

########################################################################################################
#callable function for the iteration of the frames in the animation:
def animate_time_evol_spinless(frames, fargs):
    
    # Get fargs:
    ax1 = fargs[0]
    times = fargs[1]
    state = fargs[2]
    Ham = fargs[3]
    initial_state_name = fargs[4]
    if Ham.stats=="spinless-fermions":
        current_state_pop = hams.get_state_population_spinless(Ham=Ham, state=state[frames])
    elif Ham.stats=="bosons":
        current_state_pop = hams.get_state_population_bosons(Ham=Ham, state=state[frames])

    # Assemble the title:
    title = assemble_title(model=Ham.model, 
                           sites=Ham.sites, 
                           BC=Ham.BC,
                           pars=Ham.pars,
                           quantity="Many-body state at time t={0}".format(np.round(times[frames],2)),
                           initial_state_name=initial_state_name)

    # Print status:    
    print(f"Working on frame: {frames}")
    
    # Clear canvas and plot current data:
    ax1.cla()

    # plot
    plot = ax1.plot(np.linspace(1,len(current_state_pop),len(current_state_pop)), current_state_pop, marker='o', color='blue', ms=5, lw=3, label="Absolute value squared")
    ax1.set_xlabel(r"site $j$")
    ax1.set_ylabel(r"State probability")
    ax1.legend(loc='best')
    if Ham.stats=="spinless-fermions":
        ax1.set_ylim([-0.1, 1.1])
    elif Ham.stats=="spin-0.5":
        ax1.set_ylim([-0.6, 0.6])
    elif Ham.stats=="bosons":
        ax1.set_ylim([-0.1, Ham.pars["N"] + 0.1])
    plt.suptitle(title)
    plt.tight_layout()

    return ax1,
########################################################################################################


######################################################################################
def movie_time_evolution_spinful(time_evolved_state, Ham, initial_state_name, initial_time, final_time, time_points, time_spacing):
    raise NotImplementedError()
######################################################################################








######################################################################################
def plot_eigenvalues_vs_parameter(evals, 
                                  p_list, 
                                  **kwargs):
    """
    Plots the spectrum as a function of a tuning parameter.

    Parameters:
    ----------
    evals, list of (list of) floats: the eigenvalues as the parameter is tuned.

    p_list, list of floats: the values of the parameter.

    Notes:
    ------
    This assumes that the eigenvalues are sorted.
    
    """
    # Checks:
    utils.check_dir("plots")

    # Extracting parameters and labels:
    param_len, evals_len = np.shape(evals)
    print("evals_len:", evals_len)
    print("param_len:", param_len)    
    param_name = kwargs.get("param_name")
    model = kwargs.get("model")
    sites = kwargs.get("sites")
    BC = kwargs.get("BC")
    pars = kwargs.get("pars")
    del pars[param_name]    # removing the parameter we are looping over from namelist!
    pi = p_list[0]
    pf = p_list[-1]

    # Get filename and title
    filename = assemble_viz_filename(model=model, 
                                     sites=sites,
                                     pars=pars, 
                                     folder="plots/", 
                                     quantity="evals-{0}".format(inp.n_lowest_eigenvalues),
                                     parameter_scan_dict={"param_name": param_name,
                                                          "pi": p_list[0],
                                                          "pf": p_list[-1],
                                                          "param_len": param_len})
    
    # Setting up the colors:
    colormap = plt.cm.rainbow #nipy_spectral, Set1,Paired 
    colors = [colormap(i) for i in np.linspace(0, 1, evals_len)]

    # Normalizing wrt to ground state energy:
    for i in range(param_len):
        evals[i,:] = evals[i,:] - np.amin(evals[i,:])

    # Plot(s):
    fig = plt.figure()
    ax = fig.add_subplot(111)
    for i in range(evals_len):
        ax.plot(np.array(p_list), evals[:,i], color=colors[i], linestyle="None", marker='o', ms=5)
    plt.xlabel(param_name)
    plt.ylabel("$E_i - E_0$")
    plt.title(f"Lowest {inp.n_lowest_eigenvalues} eigenvalues of the {model} model")

    plt.savefig(filename, bbox_inches='tight')
    # Saving plot:
    #plt.show()
    plt.close()

    return
######################################################################################


######################################################################################
def plot_observable_vs_one_parameter(obs_name, obs, par, par_list, model, sites, BC, static_pars, **kwargs):
    """
    Plots a real observable as a curve (function of one tuning parameter).

    Parameters:
    ----------
    obs_name, str: the name of the observable.

    obs, numpy array: the observable to plot.

    par, dict: the name of the parameter that is being tuned.

    par_list, list of floats: the values of the parameters being tuned.

    model, str: the name of the model.

    site, int: the number of sites in the system.

    BC, str: flag for boundary conditions.

    static_pars, dict: the other parameters that are NOT being tuned.

    Notes:
    ------
    
    """

    # Turn input observables and observable labels into lists if they're not lists already:
    obs_name = utils.ensure_list(obs_name)
    obs = utils.ensure_list_of_lists(obs)
    par_list = utils.ensure_list_of_lists(par_list)

    # Kwargs input:
    logx = kwargs.get("logx", False)
    logy = kwargs.get("logy", False)
    energy_gap_indices = kwargs.get("energy_gap_indices", None)
    vmin = kwargs.get("vmin", None)
    vmax = kwargs.get("vmax", None)
    plot_com = kwargs.get("plot_com", False)
    math_expr_y = kwargs.get("math_expr_y", None)

    # vmin/vmax:
    if vmin is None:
        #vmin = np.amin(obs)
        vmin = min([item for sublist in obs for item in sublist])
    if vmax is None:
        #vmax = np.amax(obs)
        vmax = max([item for sublist in obs for item in sublist])

    # vmin=np.round(vmin, 2)
    # vmax=np.round(vmax, 2)
    # if np.abs(vmin-vmax) < 1e-6:
    #     vmin = vmin-0.001
    #     vmax = vmax+0.001

    # Constructing the filename and the title:
    filename="plots/{0}-model-{1}-sites-{2}-{3}-pars".format(obs_name[0], model, sites, BC)
    if energy_gap_indices is not None:
        title = f"{obs_name[0]} between state {energy_gap_indices[0]} and {energy_gap_indices[1]} for {model} model with {sites} sites and parameters"+"\n"
    else:
        title = f"{obs_name[0]} for {model} model with {sites} sites and parameters"+"\n"
    
    par_keys=[]
    par_vals=[]
    for p in static_pars:
        par_keys.append(p)
        par_vals.append(static_pars[p])
    for i in range(len(static_pars)):
        filename += "-{0}-{1}".format(par_keys[i],par_vals[i])
        title += "{0}={1}, ".format(par_keys[i],par_vals[i])

    filename=filename+"-{0}-{1}-{2}-{3}".format(par, 
                                                np.round(par_list[0][0],2), 
                                                np.round(par_list[0][-1],2), 
                                                len(par_list[0]))  

    if energy_gap_indices is not None:
        filename=filename+"-energy_gap_indices-{0}-{1}".format(energy_gap_indices[0],energy_gap_indices[1])
    
    filename=filename+"-vmin-{0}-vmax-{1}".format(np.round(vmin,3),np.round(vmax,3))

    curr_dt = datetime.datetime.now()
    timestamp = int(round(curr_dt.timestamp()))
    filename = filename+"-"+str(timestamp)

    filename = utils.truncate_string(filename, length=251)    # make sure that the filename is not longer than 255 characters.
    filename = filename + ".png"

    # Plot(s):
    fig = plt.figure(figsize=(12,8))
    ax = fig.add_subplot(111)
    for i in range(len(obs)):

        if len(obs) == len(obs_name):
            plot = ax.plot(np.array(par_list[i]), obs[i], lw=4, label=obs_name[i]) # Plot each curve with a label
        else:
            plot = ax.plot(np.array(par_list[i]), obs[i], lw=4) # Plot each curve without a label

    # Calculate and plot center of mass of the curve?
    if plot_com==True:
        if len(obs)!=1:
            print("You passed more than one input (y-data) to be visualized. I don't know for which y-values to calculate the com. I will only do it for the first.")
        com = utils.center_of_mass(x=np.array(par_list[0]), y=obs[0])
        print(vmin)
        print(vmax)
        plot2 = ax.vlines(x=com, ymin=vmin-0.01*(vmax-vmin), ymax=vmax+0.01*(vmax-vmin), color='red', linestyle="dashed", lw=2)

    if logx:
        ax.set_xscale("log")    
    if logy:
        ax.set_yscale("log")
        ax.set_ylim([vmin-(vmax-vmin), vmax+(vmax-vmin)])

    else:
        ax.set_ylim([vmin-0.01*(vmax-vmin), vmax+0.01*(vmax-vmin)])
    
    plt.xlabel(par)
    if math_expr_y is not None:
        plt.ylabel(math_expr_y)
    else:
        plt.ylabel(obs_name[0])
    plt.title(title)
    if len(obs_name)>1:
        plt.legend(loc='best')

    # Saving plot:
    plt.savefig(filename, bbox_inches='tight')
    #plt.show()
    plt.close()

    return


######################################################################################

######################################################################################
def plot_observable_vs_two_parameters(obs_name, obs, pars, par_lists, model, sites, BC, static_pars, **kwargs):
    """
    Plots a real observable as a heatmap (function of two tuning parameters).

    Parameters:
    ----------
    obs_name, str: the name of the observable.

    obs, numpy array: the observable to plot.

    pars, dict: the names of the parameters that are being tuned.

    par_lists, dict of lists: the values of the parameters being tuned.

    model, str: the name of the model.

    site, int: the number of sites in the system.

    BC, str: flag for boundary conditions.

    static_pars, dict: the other parameters that are NOT being tuned.

    Notes:
    ------
    
    """
    log_opt = kwargs.get("log_opt", "")
    log_plot = kwargs.get("log_plot", False)
    energy_gap_indices = kwargs.get("energy_gap_indices", None)
    vmin = kwargs.get("vmin", None)
    vmax = kwargs.get("vmax", None)

    # vmin/vmax:
    if vmin is None:
        vmin = np.amin(obs)
    if vmax is None:
        vmax = np.amax(obs)

    print("vmin", vmin)
    print("vmax", vmax)
    # Constructing the filename and the title:
    filename="plots/{0}-model-{1}-sites-{2}-{3}-pars".format(obs_name, model, sites, BC)
    if energy_gap_indices is not None:
        title = f"{obs_name} between state {energy_gap_indices[0]} and {energy_gap_indices[1]} for {model} model with {sites} sites and parameters"+"\n"
    else:
        title = f"{obs_name} for {model} model with {sites} sites and parameters"+"\n"
    
    par_keys=[]
    par_vals=[]
    for par in static_pars:
        par_keys.append(par)
        par_vals.append(static_pars[par])
    for i in range(len(static_pars)):
        filename += "-{0}-{1}".format(par_keys[i],par_vals[i])
        title += "{0}={1}, ".format(par_keys[i],par_vals[i])

    filename=filename+"-{0}-{1}-{2}-{3}-{4}-{5}-{6}-{7}".format(pars[0], 
                                                                np.round(par_lists[pars[0]][0],2), 
                                                                np.round(par_lists[pars[0]][-1],2), 
                                                                len(par_lists[pars[0]]), 
                                                                pars[1], 
                                                                np.round(par_lists[pars[1]][0],2), 
                                                                np.round(par_lists[pars[1]][-1],2), 
                                                                len(par_lists[pars[1]]))  

    if energy_gap_indices is not None:
        filename=filename+"-energy_gap_indices-{0}-{1}".format(energy_gap_indices[0],energy_gap_indices[1])
    
    filename=filename+"-vmin-{0}-vmax-{1}".format(np.round(vmin,3),np.round(vmax,3))

    filename = utils.truncate_string(filename, length=251)    # make sure that the filename is not longer than 255 characters.
    filename = filename + ".png"

    # Plot(s):
    fig = plt.figure(figsize=(12,8))
    ax = fig.add_subplot(111)
    X, Y = np.meshgrid(par_lists[pars[0]], par_lists[pars[1]])
    Z = obs
    if log_plot:
        if vmin==0.0:
            vmin=1e-10
        norm = LogNorm(vmin=vmin, vmax=vmax)
    else:
        norm = Normalize(vmin=vmin, vmax=vmax)
    heatmap = ax.pcolormesh(X, Y, Z.T, cmap="inferno", shading="auto", norm=norm)
    
    if pars[0] in log_opt:
        ax.set_xscale("log")    
    if pars[1] in log_opt:
        ax.set_yscale("log")    
    plt.colorbar(heatmap, ax=ax)
    plt.xlabel(pars[0])
    plt.ylabel(pars[1])
    plt.title(title)

    # Saving plot:
    plt.savefig(filename, bbox_inches='tight')
    #plt.show()
    plt.close()

    return


######################################################################################

######################################################################################
def assemble_viz_filename(model, sites, BC, pars, folder, quantity, **kwargs):
    """
    Assembles the filename for a plot of the given quantity.

    Parameters:
    -----------
    model, str: the name of the model.

    sites, str: the number of sites in the system.

    BC, str: the type of boundary conditions,

    pars, dict: the name and values of the model parameters.

    folder, str: the name of the folder where to save the data.

    quantity, str: the name of the quantity that is being plotted.

    Returns:
    --------
    filename, str: the filename for the plot including .png extension.
    """

    # kwargs:
    dynamics = kwargs.get("dynamics", False)
    initial_time = kwargs.get("initial_time", None)
    final_time = kwargs.get("final_time", None)
    time_points = kwargs.get("time_points", None)
    time_spacing = kwargs.get("time_spacing", None)

    initial_state_name = kwargs.get("initial_state_name", None)
    extension = kwargs.get("extension", ".png")
    parameter_scan_dict = kwargs.get("parameter_scan_dict", None)
    if parameter_scan_dict is not None:
        param_name = parameter_scan_dict["param_name"]
        pi = parameter_scan_dict["pi"]
        pf = parameter_scan_dict["pf"]
        param_len = parameter_scan_dict["param_len"]

    # Extracting metadata for labels etc:
    par_keys, par_vals = utils.get_parameters_from_dict(pars)

    # Creating a descriptive filename for the plot
    filename = "{0}{1}-{2}-sites-{3}-{4}-".format(folder, quantity, model, sites, BC)
    for i in range(len(par_keys)):
        if i==len(par_keys)-1:
            filename = filename + "{0}-{1}-".format(par_keys[i], par_vals[i])
        else:
            filename = filename + "{0}-{1}, ".format(par_keys[i], par_vals[i])
    #filename = filename + "-sector-{0}".format(sector) #redundant

    # Add details about dynamics:
    if dynamics:
        filename = filename + "tinitial-{0}-tfinal-{1}-tpoints-{2}-{3}-initial-state-{4}".format(initial_time, final_time, time_points, time_spacing, initial_state_name)
    # Add details about parameter scan:
    if parameter_scan_dict is not None:
        filename = filename+"-{0}-{1}-{2}-{3}".format(param_name, pi, pf, param_len)

    # Add timestamp to make each plot unique once we need to average over many disorder realizations.
    curr_dt = datetime.datetime.now()
    timestamp = int(round(curr_dt.timestamp()))
    filename = filename+"-"+str(timestamp)
    
    # Make sure that the filename is not longer than 255 characters.
    filename = utils.truncate_string(filename, length=251)    
    filename = filename + extension

    return filename

######################################################################################

######################################################################################
def assemble_title(model, sites, BC, pars, quantity, **kwargs):
    """
    Assembles the title for a plot of the given quantity.

    Parameters:
    -----------
    Ham, instance of Hamiltonian class: the information about the given Hamiltonian.

    quantity, str: the name of the quantity that is being plotted.

    Returns:
    --------
    title, str: the title for the plot.
    """

    # Kwargs:
    initial_state_name=kwargs.get("initial_state_name", None)

    # Extracting metadata for labels etc:
    par_keys, par_vals = utils.get_parameters_from_dict(pars)

    # Assemble the title:
    title = "{0} for {1} with {2} sites ({3}) with parameters ".format(quantity, model, sites, BC)+"\n"
    for i in range(len(par_keys)):
        if i==len(par_keys)-1:
            title = title + "{0}={1}.".format(par_keys[i], par_vals[i])
        else:
            title = title + "{0}={1}, ".format(par_keys[i], par_vals[i])

    if initial_state_name is not None:
        title = title + " and initial state "+initial_state_name

    return title
######################################################################################

