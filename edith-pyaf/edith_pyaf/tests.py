#!/usr/bin/env python
############################################################################################
##### This module is used for checks and tests.
############################################################################################


####################################
###########  IMPORTS   #############
####################################
from __future__ import division
from __future__ import print_function
from itertools import permutations, combinations
import itertools
import numpy as np
import scipy as sp
import sys
import shutil
import os
import time

# CUSTOM modules:
from . import input as inp
from . import glob 
from . import hams
from . import diag
from . import obs
from . import utils
from . import dynamics as dyn
from . import viz
from . import input_output as ino
####################################
####################################

#########################################################################
#########################################################################
######################## TESTS FOR HAMILTONIANS #########################
#########################################################################
#########################################################################
def test_Hamiltonian(Ham, model, testfile):

    if model=="transverse-Ising":
        print("Hamiltonian test NOT YET IMPLEMENTED.")

    elif model=="staggered-Heisenberg":
        print("Hamiltonian test NOT YET IMPLEMENTED.")
        
    elif model=="dipolar-Aubry-Andre":
        print("Hamiltonian test NOT YET IMPLEMENTED.")

    elif model=="Hubbard":
        print("Hamiltonian test NOT YET IMPLEMENTED.")

    elif model=="disordered-XXZ-bath":

        if testfile==1:
            J = Ham.pars["J"]
            W1 = Ham.pars["W"][0]
            W2 = Ham.pars["W"][1]
            W3 = Ham.pars["W"][2]
            W4 = Ham.pars["W"][3]
            DeltaA = Ham.pars["DeltaA"]
            DeltaAB = Ham.pars["DeltaAB"]
            DeltaB = Ham.pars["DeltaB"]
            
            D0011 = (W1 + W2 - W3 - W4)/2 + J/4*(DeltaA - DeltaAB + DeltaB)
            D0101 = (W1 - W2 + W3 - W4)/2 + J/4*(-DeltaA - DeltaAB - DeltaB)
            D0110 = (-W1 + W2 + W3 - W4)/2 + J/4*(-DeltaA + DeltaAB - DeltaB)
            D1001 = (W1 - W2 - W3 + W4)/2 + J/4*(-DeltaA + DeltaAB - DeltaB)
            D1010 = (-W1 + W2 - W3 + W4)/2 + J/4*(-DeltaA - DeltaAB - DeltaB)
            D1100 = (-W1 - W2 + W3 + W4)/2 + J/4*(DeltaA - DeltaAB + DeltaB)

            Htest = np.array([[D0011, J/2, 0.0, 0.0, 0.0, 0.0],
                            [J/2, D0101, J/2, J/2, 0.0, 0.0],
                            [0.0, J/2, D0110, 0.0, J/2, 0.0],
                            [0.0, J/2, 0.0, D1001, J/2, 0.0],
                            [0.0, 0.0, J/2, J/2, D1010, J/2],
                            [0.0, 0.0, 0.0, 0.0, J/2, D1100]]
                            )
            H = sp.sparse.csr_matrix((Ham.mat_els, (Ham.rows, Ham.cols))).todense()
            
            assert np.linalg.norm(H - Htest) < 1e-12

        print("Hamiltonian test PASSED.")


    elif model=="fermionic-interacting-Hatano-Nelson":
        print("Hamiltonian test NOT YET IMPLEMENTED.")

    elif model=="bosonic-interacting-Hatano-Nelson":

        N = int(Ham.pars["N"])
        JL = Ham.pars["J"][1]
        JR = Ham.pars["J"][0]
        gL = Ham.pars["gamma"][0]
        gR = Ham.pars["gamma"][1]
        mu1 = Ham.pars["V"][0]
        mu2 = Ham.pars["V"][1]
        mu3 = Ham.pars["V"][2]
        eta1 = Ham.pars["eta"][0]
        eta2 = Ham.pars["eta"][1]
        max_occ = Ham.pars["max_occ"]
    
        D002 = 2*mu1
        D011 = mu1 + mu2 + eta1 + eta2
        D101 = mu1 + mu3 + eta1 + eta2
        D020 = 2*mu2
        D110 = mu2 + mu3 + eta1 + eta2
        D200 = 2*mu3
        sq2=np.sqrt(2)

        if testfile==1:

            Htest = np.array([[D002,     sq2*JR,    sq2*gL,    0.0,       0.0,      0.0],
                            [sq2*JL,   D011,      JR,        sq2*JR,    gL,       0.0],
                            [sq2*gR,   JL,        D101,      0.0,       JR,       sq2*gL],
                            [0.0,      sq2*JL,    0.0,       D020,      sq2*JR,   0.0],
                            [0.0,      gR,        JL,        sq2*JL,    D110,     sq2*JR],
                            [0.0,      0.0,       sq2*gR,    0.0,       sq2*JL,   D200]]
                            )
            H = sp.sparse.csr_matrix((Ham.mat_els, (Ham.rows, Ham.cols))).todense()

            #print(H)
            #print(Htest)

            assert np.linalg.norm(H - Htest) < 1e-12

        
        elif testfile==2:

            Htest = np.array([[D011,      JR,        gL],
                            [JL,        D101,      JR],
                            [gR,        JL,        D110]]
                            )
            H = sp.sparse.csr_matrix((Ham.mat_els, (Ham.rows, Ham.cols))).todense()

            assert np.linalg.norm(H - Htest) < 1e-12

    print("Hamiltonian test PASSED.")
    
    return
#########################################################################

#########################################################################
def test_get_state_population_spinless_old(Ham, model, testfile):

    if model=="disordered-XXZ-bath":
        if testfile==1:
            state = np.array([0.4, 0.3, -0.3, 0.7*1j, 0.4, 0.1*1j])
            pop_vec = hams.get_state_population_spinless_old(Ham, state)
            assert np.abs(pop_vec[0] - 0.24) < 1e-12
            assert np.abs(pop_vec[1] + 0.09) < 1e-12
            assert np.abs(pop_vec[2] + 0.31) < 1e-12
            assert np.abs(pop_vec[3] - 0.16) < 1e-12
            assert np.sum(pop_vec) == 0.0

    print("get_state_population_spinless test PASSED!")
    return
#########################################################################

#########################################################################
def test_get_state_population_spinless(Ham, model, testfile):

    if model=="disordered-XXZ-bath":
        if testfile==1:
            state = np.array([0.4, 0.3, -0.3, 0.7*1j, 0.4, 0.1*1j])
            pop_vec = hams.get_state_population_spinless(Ham, state)
            assert np.abs(pop_vec[0] - 0.24) < 1e-12
            assert np.abs(pop_vec[1] + 0.09) < 1e-12
            assert np.abs(pop_vec[2] + 0.31) < 1e-12
            assert np.abs(pop_vec[3] - 0.16) < 1e-12
            assert np.sum(pop_vec) == 0.0

    print("get_state_population_spinless test PASSED!")
    return
#########################################################################

#########################################################################
def test_get_state_population_spinless_site(Ham, model, testfile):

    if model=="disordered-XXZ-bath":
        if testfile==1:
            state = np.array([0.4, 0.3, -0.3, 0.7*1j, 0.4, 0.1*1j])
            pop1 = hams.get_state_population_spinless_site(Ham, state, site=0)
            assert np.abs(pop1 - 0.24) < 1e-12
            pop2 = hams.get_state_population_spinless_site(Ham, state, site=1)
            assert np.abs(pop2 + 0.09) < 1e-12
            pop3 = hams.get_state_population_spinless_site(Ham, state, site=2)
            assert np.abs(pop3 + 0.31) < 1e-12
            pop4 = hams.get_state_population_spinless_site(Ham, state, site=3)
            assert np.abs(pop4 - 0.16) < 1e-12

    print("get_state_population_spinless_site test PASSED!")
    return
#########################################################################

#########################################################################
def test_get_state_population_bosons_site(Ham, model, testfile):
    if model=="bosonic-interacting-Hatano-Nelson":
        if testfile==1:
            #state = np.array([0.3, 0.1, -0.2, 0.5*1j, 0.6, -0.5])
            state = np.array([-0.5, 0.6, 0.5*1j, -0.2, 0.1, 0.3])            
            pop1 = hams.get_state_population_bosons_site(Ham, state, site=0)
            pop2 = hams.get_state_population_bosons_site(Ham, state, site=1)
            pop3 = hams.get_state_population_bosons_site(Ham, state, site=2)
            #print(pop1)
            #print(pop2)
            #print(pop3)
            assert np.abs(pop1 - 1.11) < 1e-12
            assert np.abs(pop2 - 0.45) < 1e-12
            assert np.abs(pop3 - 0.44) < 1e-12
        elif testfile==2:
            state = np.array([-2/7, 6/7, 3/7])
            pop1 = hams.get_state_population_bosons_site(Ham, state, site=0)
            assert np.abs(pop1 - 0.8163265306122448979) < 1e-12
            pop2 = hams.get_state_population_bosons_site(Ham, state, site=1)
            assert np.abs(pop2 - 0.2653061224489795918) < 1e-12
            pop3 = hams.get_state_population_bosons_site(Ham, state, site=2)
            assert np.abs(pop3 - 0.9183673469387755102) < 1e-12

    print("get_state_population_bosons_site test PASSED!")
    return
#########################################################################



#########################################################################
def test_get_state_population_spinful(Ham, model, testfile):
    print("get_state_population_spinful test NOT YET IMPLEMENTED!")
    return
#########################################################################








#########################################################################
#########################################################################
###################### TESTS FOR DIAGONALIZATION ########################
#########################################################################
#########################################################################
def test_diagonalization(Ham, model):
    print("Diagonalization test NOT YET IMPLEMENTED")
#########################################################################






















#########################################################################
#########################################################################
######################    TESTS FOR DYNAMICS    #########################
#########################################################################
#########################################################################

#########################################################################
def test_time_evolve():
    print("time_evolve test NOT YET IMPLEMENTED")
    return
#########################################################################

#########################################################################
def test_full_time_evolution():
    print("full_time_evolution test NOT YET IMPLEMENTED")
    return
#########################################################################


















#########################################################################
#########################################################################
######################### TESTS FOR OBSERVABLES #########################
#########################################################################
#########################################################################
######################################################################################
def test_energy_gap():
    print("energy_gap test NOT YET IMPLEMENTED")
    return
######################################################################################

######################################################################################
def test_DOS():
    print("DOS test NOT YET IMPLEMENTED")
    return
######################################################################################

######################################################################################
def test_LDOS():
    print("LDOS test NOT YET IMPLEMENTED")
    return
######################################################################################

######################################################################################
def test_LDOS_all_sites():
    print("LDOS_all_sites test NOT YET IMPLEMENTED")
    return
######################################################################################

######################################################################################
def test_DOS():
    print("DOS test NOT YET IMPLEMENTED")
    return
######################################################################################

######################################################################################
def test_mean_density_localization():
    print("DOS test NOT YET IMPLEMENTED")
    return
######################################################################################

######################################################################################
def test_spin_z(Ham, model, testfile):

    if model=="transverse-Ising":
        print("spin_exp_z test NOT IMPLEMENTED.")

    elif model=="staggered-Heisenberg":
        print("spin_exp_z test NOT IMPLEMENTED.")

    elif model=="disordered-XXZ-bath":
        if testfile==1:
            assert np.abs(obs.spin_exp_z(Ham=Ham, state=[0.7071067812, 0.0, 0.0, 0.0, 0.0, 0.7071067812], site=3)) < 1e-10
            assert np.abs(obs.spin_exp_z(Ham=Ham, state=[0.5, 0.0, 0.5, 0.5, 0.5, 0.0], site=3)) < 1e-10
            assert np.abs(obs.spin_exp_z(Ham=Ham, state=[0.0, 0.0, 1.0, 0.0, 0.0, 0.0], site=3) + 0.5) < 1e-10
            assert np.abs(obs.spin_exp_z(Ham=Ham, state=[0.5773502692, 0.5773502692, 0.5773502692, 0.0, 0.0, 0.0], site=3) + 0.5) < 1e-10
            assert np.abs(obs.spin_exp_z(Ham=Ham, state=[0.0, 0.0, 0.0, 0.5773502692, 0.5773502692, 0.5773502692], site=3) - 0.5) < 1e-10

        print("spin_exp_z test PASSED.")
    
    else:
        print(f"spin_exp_z is not a valid observable for {model}.")

    return
######################################################################################

######################################################################################
def test_magnetization_z(Ham, model, testfile):

    if model=="transverse-Ising" or model=="staggered-Heisenberg" or model=="disordered-XXZ-bath":

        if testfile==1:
            assert np.abs(obs.magnetization_z(Ham=Ham, state=[0.7071067812, 0.0, 0.0, 0.0, 0.0, 0.7071067812], site=3)) < 1e-10
            assert np.abs(obs.magnetization_z(Ham=Ham, state=[0.5, 0.0, 0.5, 0.5, 0.5, 0.0], site=3)) < 1e-10
            assert np.abs(obs.magnetization_z(Ham=Ham, state=[0.0, 0.0, 1.0, 0.0, 0.0, 0.0], site=3)) < 1e-10
            assert np.abs(obs.magnetization_z(Ham=Ham, state=[0.5773502692, 0.5773502692, 0.5773502692, 0.0, 0.0, 0.0], site=3)) < 1e-10
            assert np.abs(obs.magnetization_z(Ham=Ham, state=[0.0, 0.0, 0.0, 0.5773502692, 0.5773502692, 0.5773502692], site=3)) < 1e-10

        print("magnetization_z test PASSED.")
    
    else:
        print(f"magnetization_z is not a valid observable for {model}.")

    return
######################################################################################

######################################################################################
def test_corr_mat_zz(Ham, model, testfile):

    if model=="transverse-Ising" or model=="staggered-Heisenberg" or model=="disordered-XXZ-bath": 
        if testfile==1:    
            m1 = 0.25*np.array([[1, -1, -1, 1], [-1, 1, 1, -1], [-1, 1, 1, -1], [1, -1, -1, 1]])
            assert np.linalg.norm(obs.zz_correlation_matrix(Ham=Ham, state=[0.0, 0.0, 1.0, 0.0, 0.0, 0.0], connected=False) - m1 ) < 1e-10   # |0110>
            
            m2 = 0.25*np.array([[1, 0, -1, 0], [0, 1, 0, -1], [-1, 0, 1, 0], [0, -1, 0, 1]])
            assert np.linalg.norm(obs.zz_correlation_matrix(Ham=Ham, state=[0.7071067812, 0.0, 0.7071067812, 0.0, 0.0, 0.0], connected=False) - m2) < 1e-10  # 1/sqrt(2)*(|0011> + |0110>)

            m3 = 0.25*np.array([[1, -1/3, -1/3, -1/3], [-1/3, 1, -1/3, -1/3], [-1/3, -1/3, 1, -1/3], [-1/3, -1/3, -1/3, 1]])
            assert np.linalg.norm(obs.zz_correlation_matrix(Ham=Ham, state=[0.5773502692, 0.0, 0.5773502692, 0.0, 0.5773502692, 0.0], connected=False) - m3) < 1e-10

            print("zz_correlation_mat test PASSED.")
    
    else:
        print(f"magnetization_z is not a valid observable for {model}.")

    return
######################################################################################

######################################################################################            
def test_bath_averaged_zz_correlation_site():
    print("bath_averaged_zz_correlation_site test NOT YET IMPLEMENTED")
    return
######################################################################################            

######################################################################################            
def test_bath_averaged_zz_correlation_function():
    print("bath_averaged_zz_correlation_function test NOT YET IMPLEMENTED")
    return
######################################################################################  
          
######################################################################################
def test_correlation_decay_length():
    print("correlation_decay_length test NOT YET IMPLEMENTED")
    return
######################################################################################

######################################################################################
def test_scaled_IPR():
    print("scaled_IPR test NOT YET IMPLEMENTED")
    return
######################################################################################

######################################################################################
def test_average_IPR():
    print("average_IPR test NOT YET IMPLEMENTED")
    return
######################################################################################












#########################################################################
#########################################################################
###################      TESTS FOR INPUT/OUTPUT     #####################
#########################################################################
#########################################################################

#########################################################################
def test_save_eigenvalues():
    print("save_eigenvalues test NOT YET IMPLEMENTED")
    return
#########################################################################

#########################################################################
def test_save_eigenvectors():
    print("save_eigenvectors test NOT YET IMPLEMENTED")
    return
#########################################################################            

#########################################################################            
def test_save_obs():
    print("save_obs test NOT YET IMPLEMENTED")
    return
#########################################################################

#########################################################################
def test_load_initial_state_dynamics(Ham, input, model, testfile):

    if model=="disordered-XXZ-bath":

        if testfile==2:
            # list-list loading (|01>_B x |0101>_S)

            # The initial state vector is |010101> = [0,0,0,0,1,0,0,...,0] --> only index 5 of the 
            # vector should be populated.
            print("Testing list-list input.")
            initial_state_vec = ino.load_initial_state_dynamics(Ham=Ham, input=input, evecs="doesntmatter")

            assert np.abs(initial_state_vec[5]-1) < 1e-10
            for i in [0,1,2,3,4,6,7,8,9,10,11,12,13,14,15,16,17,18,19]:
                assert np.abs(initial_state_vec[i] - 0) < 1e-10

        elif testfile==3 or testfile==4:
            
            if testfile==3:
                # list-file loading ([-0.28978415  0.95709203]_B x |0101>_S)
                # The bath initial state contained in the file is [-0.28978415  0.95709203] (same as ground state), see below.

                copy_template_file(input=model+"-initial_state_bath.dat")
                print("Testing list-self input.")

            elif testfile==4:
                # list-self loading (|GS>_B x |0101>_S)

                # The bath ground state is [-0.28978415  0.95709203] (corresponding to |01>, |10> respectively)
                # The initial state vector should have nonzero elements at |010101> (5th) and |100101> (11th).
                print("Testing list-self input.")
            
            initial_state_vec = ino.load_initial_state_dynamics(Ham=Ham, input=input, evecs="doesntmatter")
            print(initial_state_vec)

            assert np.abs(initial_state_vec[5] + 0.28978415) < 1e-8
            assert np.abs(initial_state_vec[11] - 0.95709203) < 1e-8
            
            for i in [0,1,2,3,4,6,7,8,9,10,12,13,14,15,16,17,18,19]:
                assert np.abs(initial_state_vec[i] - 0) < 1e-10
        
        elif testfile==5 or testfile==6:

            if testfile==5:
                # file-list (|01>_B x [0.12458219 -0.59683427  0.40142721  0.36368356 -0.56356608  0.13134636]_S)
                # The subsystem initial state contained in the file is [0.12458219 -0.59683427  0.40142721  0.36368356 -0.56356608  0.13134636]_S (same as ground state), see below.

                print("Testing file-list input.")
                copy_template_file(input=model+"-initial_state.dat")

            elif testfile==6:
                # self-list (|01>_B x |GS>_S)
                
                # The subsystem ground state is [0.12458219 -0.59683427  0.40142721  0.36368356 -0.56356608  0.13134636]
                # (with basis |0011>, |0101>, |0110>, |1001>, |1010>, |1100>)
                # The initial state vector should have nonzero elements at: 
                # -|010011> (4th) 
                # -|010101> (5th) 
                # -|010110> (6th) 
                # -|011001> (7th)
                # -|011010> (8th)
                # -|011100> (9th)

                print("Testing self-list input.")

            initial_state_vec = ino.load_initial_state_dynamics(Ham=Ham, input=input, evecs="doesntmatter")
            
            print(initial_state_vec)

            assert np.abs(initial_state_vec[4] - 0.12458219) < 1e-8
            assert np.abs(initial_state_vec[5] + 0.59683427) < 1e-8
            assert np.abs(initial_state_vec[6] - 0.40142721) < 1e-8
            assert np.abs(initial_state_vec[7] - 0.36368356) < 1e-8
            assert np.abs(initial_state_vec[8] + 0.56356608) < 1e-8
            assert np.abs(initial_state_vec[9] - 0.13134636) < 1e-8

            for i in [0,1,2,3,10,11,12,13,14,15,16,17,18,19]:
                assert np.abs(initial_state_vec[i] - 0) < 1e-10

        elif testfile==7 or testfile==8 or testfile==9 or testfile==10:

            if testfile==7:
                # file-file ([-0.28978415  0.95709203]_B x [0.12458219 -0.59683427  0.40142721  0.36368356 -0.56356608  0.13134636]_S)

                print("Testing file-file input.")
                copy_template_file(input=model+"-initial_state.dat")
                copy_template_file(input=model+"-initial_state_bath.dat")

            elif testfile==8:
                # file-self (|GS>_B x [0.12458219 -0.59683427  0.40142721  0.36368356 -0.56356608  0.13134636]_S)

                print("Testing file-self input.")
                copy_template_file(input=model+"-initial_state.dat")

            elif testfile==9:
                # self-file ([-0.28978415  0.95709203]_B x |GS>_S)

                print("Testing self-file input.")
                copy_template_file(input=model+"-initial_state_bath.dat")

            elif testfile==10:
                # self-self (|01>_B x |GS>_S)

                # The subsystem ground state is [0.12458219 -0.59683427  0.40142721  0.36368356 -0.56356608  0.13134636]
                # (with basis |0011>, |0101>, |0110>, |1001>, |1010>, |1100>)
                # The bath ground state is  [-0.28978415  0.95709203] (corresponding to |01>, |10> respectively)
                # The initial state vector should have nonzero elements at: 
                # -|010011> (4th) 
                # -|100011> (10th)
                # -|010101> (5th) 
                # -|100101> (11th)
                # -|010110> (6th) 
                # -|100110> (12th)
                # -|011001> (7th)
                # -|101001> (13th)
                # -|011010> (8th)
                # -|101010> (14th)
                # -|011100> (9th)
                # -|101100> (15th)

                print("Testing self-self input.")

            initial_state_vec = ino.load_initial_state_dynamics(Ham=Ham, input=input, evecs="doesntmatter")
            
            print(initial_state_vec)

            assert np.abs(initial_state_vec[4] + 0.28978415*0.12458219) < 1e-8
            assert np.abs(initial_state_vec[10] - 0.95709203*0.12458219) < 1e-8
            assert np.abs(initial_state_vec[5] + 0.28978415*(-0.59683427)) < 1e-8
            assert np.abs(initial_state_vec[11] - 0.95709203*(-0.59683427)) < 1e-8
            assert np.abs(initial_state_vec[6] + 0.28978415*0.40142721) < 1e-8
            assert np.abs(initial_state_vec[12] - 0.95709203*0.40142721) < 1e-8
            assert np.abs(initial_state_vec[7] + 0.28978415*0.36368356) < 1e-8
            assert np.abs(initial_state_vec[13] - 0.95709203*0.36368356) < 1e-8
            assert np.abs(initial_state_vec[8] + 0.28978415*(-0.56356608)) < 1e-8
            assert np.abs(initial_state_vec[14] - 0.95709203*(-0.56356608)) < 1e-8
            assert np.abs(initial_state_vec[9] + 0.28978415*0.13134636) < 1e-8
            assert np.abs(initial_state_vec[15] - 0.95709203*0.13134636) < 1e-8            

            for i in [0,1,2,3,16,17,18,19]:
                assert np.abs(initial_state_vec[i] - 0) < 1e-10



        # OLD STUFF
        # if testfile==1:
        #     # with bath
        #     v2 = np.zeros(6)
        #     v2[1] = 0.707
        #     v2[3] = 0.707
        #     assert np.linalg.norm(ino.load_initial_state_dynamics(Ham=Ham, input=input) - v2) < 1e-10
        
        # elif testfile==2:
        #     # without bath
        #     v1 = np.zeros(6)
        #     v1[4] = 1.0
        #     assert np.linalg.norm(ino.load_initial_state_dynamics(Ham=Ham, input=input) - v1) < 1e-10

        # elif testfile==3:
        #     # This requires different input file with 8 sites
        #     v3 = np.zeros(70)
        #     v3[39] = 0.5
        #     v3[40] = 0.5
        #     v3[42] = 0.5
        #     v3[44] = 0.5 
        #     bath_initial_state = np.array([0.5, 0.5, 0.0, 0.5, 0.0, 0.5])   # 0.5(|0011> + |0101> + |1001> + |1100>)
        #     assert np.linalg.norm(ino.load_initial_state_dynamics(Ham=Ham, initial_state='1001bbbb', bath_initial_state=bath_initial_state) - v3) < 1e-10

        print("load_initial_state_dynamics test PASSED.")




    return
#########################################################################

#########################################################################
def test_load_eigenvalues():
    print("load_eigenvalues test NOT YET IMPLEMENTED")
    return
#########################################################################

#########################################################################
def test_load_obs():
    print("load_obs test NOT YET IMPLEMENTED")
    return
#########################################################################

#########################################################################
def test_get_filename():
    print("get_filename test NOT YET IMPLEMENTED")
    return
#########################################################################

#########################################################################
def test_modify_input():
    print("modify_input test NOT YET IMPLEMENTED")
    return
#########################################################################






#########################################################################
#########################################################################
######################      TESTS FOR UTILS     #########################
#########################################################################
#########################################################################

#########################################################################
def test_spin_dim():
    print("spin_dim test NOT YET IMPLEMENTED!")
    return
#########################################################################

#########################################################################
def test_get_full_Hamiltonian():
    print("get_full_Hamiltonian test NOT YET IMPLEMENTED!")
    return
#########################################################################

#########################################################################
def test_get_parameters_from_dict():
    print("get_parameters_from_dict test NOT YET IMPLEMENTED!")
    return
#########################################################################

#########################################################################
def test_calculate_overlap():
    print("calculate_overlap test NOT YET IMPLEMENTED!")
    return
#########################################################################

#########################################################################
def test_convert_state_from_str_to_arr():
    print("convert_state_from_str_to_arr test NOT YET IMPLEMENTED!")
    return
#########################################################################

#########################################################################
def test_convert_state_from_bin_to_vec():
    print("conver_state_from_bin_to_vec test NOT YET IMPLEMENTED!")
    return
#########################################################################

#########################################################################
def test_compute_average():

    obs_list1 = [1,2,3,4,5,6]
    obs_list2 = [np.array([1,2,3]), np.array([4,5,6]), np.array([7,8,9])]
    obs_list3 = [np.array([[1,2],[3,4]]), np.array([[5,6],[7,8]]), np.array([[9,10],[11,12]])]
    obs_list4 = [np.array([[1,0],[2,-1]]), np.array([[6,5],[0,1]]), np.array([[-1,1],[4,0]])]

    assert utils.compute_average(obs_list=obs_list1, averaging_method="arithmetic-mean") == 3.5
    assert utils.compute_average(obs_list=obs_list1, averaging_method="median") == 3.5
    assert np.linalg.norm(utils.compute_average(obs_list=obs_list2, averaging_method="arithmetic-mean") - np.array([4,5,6])) < 1e-20
    assert np.linalg.norm(utils.compute_average(obs_list=obs_list2, averaging_method="median") - np.array([4,5,6])) < 1e-20
    assert np.linalg.norm(utils.compute_average(obs_list=obs_list3, averaging_method="arithmetic-mean") - np.array([[5,6],[7,8]])) < 1e-20
    assert np.linalg.norm(utils.compute_average(obs_list=obs_list3, averaging_method="median") - np.array([[5,6],[7,8]])) < 1e-20
    assert np.linalg.norm(utils.compute_average(obs_list=obs_list4, averaging_method="arithmetic-mean") - np.array([[2,2],[2,0]])) < 1e-20
    assert np.linalg.norm(utils.compute_average(obs_list=obs_list4, averaging_method="median") - np.array([[1,1],[2,0]])) < 1e-20

    print("compute_average test PASSED.")
    
    return
#########################################################################

#########################################################################
def test_get_dimensionality():

    lst1 = [1,2,3]
    lst2 = [np.array([1,2,3]), np.array([4,5,6]), np.array([7,8,9])]
    lst3 = [np.array([[1,2],[3,4]]), np.array([[5,6],[7,8]]), np.array([[9,10],[11,12]])]

    assert utils.get_dimensionality(lst1) == 0
    assert utils.get_dimensionality(lst2) == 1
    assert utils.get_dimensionality(lst3) == 2

    print("get_dimensionality test PASSED.")
    return
#########################################################################

#########################################################################
def test_binomial():
    
    assert utils.binomial(8,4)==70
    assert utils.binomial(16,8)==12870
    assert utils.binomial(32,16)==601080390

    print("binomial test PASSED.")

    return
#########################################################################

#########################################################################
def test_combo_rank():
    print("combo_rank test NOT YET IMPLEMENTED!")
    return
#########################################################################

#########################################################################
def test_combos_with_reps():
    print("combos_with_reps test NOT YET IMPLEMENTED!")
    return
#########################################################################

#########################################################################
def test_combos_without_reps():
    print("combos_without_reps test NOT YET IMPLEMENTED!")
    return
#########################################################################

#########################################################################
def test_generate_all_binaries():
    print("generate_all_binaries test NOT YET IMPLEMENTED!")
    return
#########################################################################

#########################################################################
def test_number_of_restricted_compositions():
    print("number_of_restricted_compositions test NOT YET IMPLEMENTED!")
    return
#########################################################################

#########################################################################
def test_findBitCombinations():
    print("findBitCombinations test NOT YET IMPLEMENTED!")
    return
#########################################################################

#########################################################################
def test_lexRank():
    print("lexRank test NOT YET IMPLEMENTED!")
    return
#########################################################################

#########################################################################
def test_colexRank_multi():
    print("colexRank_multi test NOT YET IMPLEMENTED!")
    return
#########################################################################

#########################################################################
def test_is_state_allowed():
    print("is_state_allowed test NOT YET IMPLEMENTED!")
    return
#########################################################################

#########################################################################
def test_get_binary_from_rel_rank():

    assert utils.get_binary_from_rel_rank(rank=13, sites=6, sz=0) == '101001'
    assert utils.get_binary_from_rel_rank(rank=3, sites=6, sz=0) == '001110'
    assert utils.get_binary_from_rel_rank(rank=36, sites=16, sz=0) == '0000001111101011'
    
    print("get_binary_from_rel_rank test PASSED.")

    return
#########################################################################

#########################################################################
def test_find_ith_combination():
    print("find_ith_combination test NOT YET IMPLEMENTED!")
    return
#########################################################################

#########################################################################
def test_find_string_from_rank():
    print("find_string_from_rank test NOT YET IMPLEMENTED!")
    return
#########################################################################

#########################################################################
def test_find_string_from_rank2():
    print("find_string_from_rank2 test NOT YET IMPLEMENTED!")
    return
#########################################################################

#########################################################################
def test_ground_state_Hubbard_half_filling():
    print("ground_state_Hubbard_half_filling test NOT YET IMPLEMENTED!")
    return
#########################################################################

#########################################################################
def test_enumerated_product():
    print("enumerated_product test NOT YET IMPLEMENTED!")
    return
#########################################################################

#########################################################################
def test_check_dir():
    print("check_dir test NOT YET IMPLEMENTED!")
    return
#########################################################################

#########################################################################
def test_get_time_diff():
    print("get_time_diff test NOT YET IMPLEMENTED!")
    return
#########################################################################

#########################################################################
def test_get_matching_files():
    print("get_matching_files test NOT YET IMPLEMENTED!")
    return
#########################################################################

#########################################################################
def test_truncate_string():

    mystr="1298askj2b398hs9d3jn4lskdjb"
    truncated_str = utils.truncate_string(str=mystr, length=13)

    assert len(truncated_str)==13

    print("truncate_string test PASSED.")
    return
#########################################################################

#########################################################################
def test_center_of_mass():

    x = [1,2,3,4,5]
    y1 = [5,0,0,0,0]
    y2 = [0,0,0,0,5]
    y3 = [5,3,1,3,5]

    com1 = utils.center_of_mass(x=x, y=y1)
    com2 = utils.center_of_mass(x=x, y=y2)
    com3 = utils.center_of_mass(x=x, y=y3)

    assert np.abs(com1 - 1.0) < 1e-12
    assert np.abs(com2 - 5.0) < 1e-12
    assert np.abs(com3 - 3.0) < 1e-12

    print("truncate_center_of_mass test PASSED.")

    return
#########################################################################

#########################################################################
def test_clean_directories():

    dir="bogus/"

    # Create bogus file in directory:
    if not os.path.isdir(dir):
        os.mkdir(dir)

    file_path = os.path.join(dir, "bogus.txt")
    with open(file_path, 'w') as file:
        file.write("This is a bogus file.")

    # Remove the bogus file in the directory:
    utils.clean_directories([dir])

    files_in_dir = os.listdir(dir)
    print(files_in_dir)
    if not files_in_dir:
        print("clean_directories test PASSED.")
    else:
        print("clean_directories test FAILED.")
    
    os.rmdir(dir)

    return
#########################################################################

#########################################################################
def test_ensure_list():

    not_a_list = "hello"
    its_now_a_list = utils.ensure_list(not_a_list)

    if isinstance(its_now_a_list, list):
        print("ensure_list test PASSED.")
    else:
        print("ensure_list test FAILED.")
#########################################################################

#########################################################################
def test_ensure_list_of_lists():

    print("ensure_list_of_lists test NOT YET IMPLEMENTED!")
    return
#########################################################################

#########################################################################
def test_get_times():

    lin_times1 = [0,1,2,3,4,5,6,7,8,9,10]
    exp_times1 = [1., 1.25892541, 1.58489319, 1.99526231, 2.51188643, 3.16227766, 3.98107171, 5.01187234, 6.30957344, 7.94328235, 10.]

    lin_times2 = utils.get_times(0, 10, 11, "linear")
    exp_times2 = utils.get_times(0, 1, 11, "exp")

    for i in range(11):
        assert np.abs(lin_times1[i] - lin_times2[i]) < 1e-8
        assert np.abs(exp_times1[i] - exp_times2[i]) < 1e-8

    print("get_times test PASSED.")
#########################################################################

#########################################################################
def test_memory_checks():
    print("memory_checks test NOT YET IMPLEMENTED!")
    return
#########################################################################

#########################################################################
def test_check_available_memory():
    print("check_available_memory test NOT YET IMPLEMENTED!")
    return
#########################################################################

#########################################################################
def test_check_required_memory():
    print("check_required_memory test NOT YET IMPLEMENTED!")
    return
#########################################################################






#########################################################################
#########################################################################
###################      TESTS FOR VISUALIZATION     ####################
#########################################################################
#########################################################################
# TODO: use Structural Similarity Index (SSIM) to compare generated .png's 
# with benchmarked .pngs. 
#########################################################################
def test_visualize_Hamiltonian():
    print("visualize_Hamiltonian test NOT YET IMPLEMENTED")
    return
#########################################################################

#########################################################################
def test_visualize_eigenvalues():
    print("visualize_eigenvalues test NOT YET IMPLEMENTED")
    return
#########################################################################

#########################################################################
def test_visualize_eigenstate():
    print("visualize_eigenstate test NOT YET IMPLEMENTED")
    return
#########################################################################

#########################################################################
def test_visualize_eigenstate_multicomponent():
    print("visualize_eigenstate_multicomponent test NOT YET IMPLEMENTED")
    return
#########################################################################

#########################################################################
def test_plot_time_evolution_spinless():
    print("plot_time_evolution_spinless test NOT YET IMPLEMENTED")
    return
#########################################################################

#########################################################################
def test_plot_density_vs_t_spinless():
    print("plot_density_vs_t_spinless test NOT YET IMPLEMENTED")
    return
#########################################################################

#########################################################################
def test_plot_density_vs_t_spinful():
    print("plot_density_vs_t_spinful test NOT YET IMPLEMENTED")
    return
#########################################################################

#########################################################################
def test_plot_dynamics_correlation():
    print("plot_dynamics_correlation test NOT YET IMPLEMENTED")
    return
#########################################################################

#########################################################################
def test_movie_time_evolution_spinless():
    print("movie_time_evolution_spinless test NOT YET IMPLEMENTED")
    return
#########################################################################

#########################################################################
def test_movie_time_evolution_spinful():
    print("movie_time_evolution_spinful test NOT YET IMPLEMENTED")
    return
#########################################################################

#########################################################################
def test_plot_eigenvalues_vs_parameter():
    print("plot_eigenvalues_vs_parameter test NOT YET IMPLEMENTED")
    return
#########################################################################

#########################################################################
def test_plot_observable_vs_one_parameter():
    print("plot_observable_vs_one_parameter test NOT YET IMPLEMENTED")
    return
#########################################################################

#########################################################################
def test_plot_observable_vs_two_parameters():
    print("plot_observable_vs_two_parameters test NOT YET IMPLEMENTED")
    return
#########################################################################


























#########################################################################
#########################################################################
###################     SUBROUTINES FOR TESTS.PY     ####################
#########################################################################
#########################################################################
######################################################################################
def copy_template_file(input):
      package_directory = os.path.dirname(__file__)
      templates_directory = os.path.join(package_directory, 'templates')
      template_path = os.path.join(templates_directory, input)
    
      if not os.path.exists(templates_directory):
            print(f"Error: The 'templates' directory does not exist in {package_directory}.")
            return

      current_dir = os.getcwd()
      destination_path = os.path.join(current_dir, input)

      shutil.copy(template_path, destination_path)
      print(f"Template input file copied to: {destination_path}")
######################################################################################

######################################################################################
def cleaner(input_file):
    current_dir = os.getcwd()
    destination_path = os.path.join(current_dir, input_file)

    if os.path.exists(destination_path):
        os.remove(destination_path)
        print(f"File removed from: {destination_path}")
    else:
        print(f"No file found at: {destination_path}")

######################################################################################










#########################################################################
#########################################################################
######################### OTHER RANDOM STUFF ############################
#########################################################################
#########################################################################
##############################################################
def generate_binaries(N, M):
    binary_digits = ['0'] * (N - M) + ['1'] * M
    binary_permutations = set(permutations(binary_digits))
    sorted_binaries = sorted([''.join(permutation) for permutation in binary_permutations])
    return sorted_binaries
##############################################################
##############################################################
def map_to_integers(N, M):
    binary_strings = generate_binaries(N, M)
    integer_mapping = {binary_string: index for index, binary_string in enumerate(binary_strings)}
    return integer_mapping
##############################################################
##############################################################
def nested_loops():
      
    parameters = ["eta", "Delta", "N"]
    start_values = {"eta": -2, "Delta": 0.0, "N": 4}
    end_values = {"eta": 2, "Delta": 10.0, "N": 8}
    n_points = {"eta": 11, "Delta": 11, "N": 3}

    
    par_lists = {}
    args = []
    for par in parameters:
        if par=="Delta" or par=="N":
            par_lists[par] = np.linspace(start_values[par], end_values[par], n_points[par])
        elif par=="eta":
            par_lists[par] = 10**(np.linspace(start_values[par], end_values[par], n_points[par]))

        args.append(par_lists[par])

    shape = tuple(n_points[parameters[i]] for i in range(len(parameters)))
    print(shape)
    IPR_arr = np.zeros(shape=shape)
    
    for idx, combination in enumerated_product(*args):

    # Generating output string and dictionary for current parameter selection 
    # (merged with static parameters)
        print(idx,combination)

        shape = tuple(n_points[parameters[i]] for i in range(len(parameters)))     
        IPR_arr[idx]=np.sum(idx)
        
    print(IPR_arr)

    return
##############################################################

##############################################################
def enumerated_product(*args):
    yield from zip(itertools.product(*(range(len(x)) for x in args)), itertools.product(*args))
##############################################################
######################################################################################














######################################################################################
######################################################################################
######################################################################################
######################################################################################
if __name__=="__main__":

    print("==================================")
    print("==================================")
    print("==================================")
    print(" STARTING UNIT TESTING FOR EDITH ")
    print("==================================")
    print("==================================")
    print("==================================")
    print("\n")

    #models_list = ["transverse-Ising", "staggered-Heisenberg", "dipolar-Aubry-Andre", "Hubbard", "disordered-XXZ-bath", "fermionic-interacting-Hatano-Nelson"]
    #testfiles_numbers = [1, 1, 1, 1, 3, 1]

    models_list = ["disordered-XXZ-bath", "bosonic-interacting-Hatano-Nelson"]
    testfiles_numbers = [10, 2]

    for idx in range(len(models_list)):
        model = models_list[idx]
        testfiles = testfiles_numbers[idx]
        
        print("======================================================")
        print(f"Performing unit testing for model {model}")
        print("======================================================")
        print("\n")

        for testfile in range(1, testfiles+1):

            # Copy test input file:
            print("Loading test input file...")
            input_filename="input_test_"+str(model)+"_"+str(testfile)+".dat"
            copy_template_file(input=input_filename)
    
            # Initialize input and Hamiltonian
            print("Initializing input and Hamiltonian...")
            input = inp.Input(input_filename=input_filename)
            Ham = hams.Hamiltonian(input=input)
            
            # Tests for Hamiltonian assembly:
            print("\n")
            print("==================================")
            print("Tests for Hamiltonian assembly: ")
            print("==================================")
            test_Hamiltonian(Ham=Ham, model=model, testfile=testfile)
            test_get_state_population_spinless_old(Ham=Ham, model=model, testfile=testfile)
            test_get_state_population_spinless_site(Ham=Ham, model=model, testfile=testfile)
            test_get_state_population_spinless(Ham=Ham, model=model, testfile=testfile)
            test_get_state_population_spinful(Ham=Ham, model=model, testfile=testfile)
            test_get_state_population_bosons_site(Ham=Ham, model=model, testfile=testfile)
            print("==================================")
            print("\n")

            # Tests for diagonalization:
            print("==================================")
            print("Tests for diagonalization: ")
            print("==================================")    
            test_diagonalization(Ham=Ham, model=model)
            print("==================================")
            print("\n")

            # Tests for dynamics:
            print("==================================")
            print("Tests for dynamics: ")
            print("==================================")
            test_time_evolve()
            test_full_time_evolution()
            print("==================================")
            print("\n")

            # Tests for observable calculation:
            print("==================================")
            print("Tests for observable calculation: ")
            print("==================================")
            test_energy_gap()
            test_DOS()
            test_LDOS()
            test_LDOS_all_sites()
            test_mean_density_localization()
            test_spin_z(Ham=Ham, model=model, testfile=testfile)
            test_magnetization_z(Ham=Ham, model=model, testfile=testfile)
            test_corr_mat_zz(Ham=Ham, model=model, testfile=testfile)
            test_bath_averaged_zz_correlation_site()
            test_bath_averaged_zz_correlation_function()
            test_correlation_decay_length()
            test_scaled_IPR()
            test_average_IPR()
            print("==================================")
            print("\n")

            # Tests for input/output:
            print("==================================")
            print("Input/output testing: ")
            print("==================================")
            test_save_eigenvalues()
            test_save_eigenvectors()
            test_save_obs()
            test_load_initial_state_dynamics(Ham=Ham, input=input, model=model, testfile=testfile)
            test_load_eigenvalues()
            test_load_obs()
            test_get_filename()
            test_modify_input()
            print("==================================")
            print("\n")

            print("==================================")
            print("Cleaning out test input files... ")
            print("==================================")
            cleaner(input_file=input_filename)
            cleaner(input_file=model+"-initial_state.dat")
            cleaner(input_file=model+"-initial_state_bath.dat")
            print("\n")



    # Tests for utils:
    print("==================================")
    print("Utils testing: ")
    print("==================================")
    test_spin_dim()
    test_get_full_Hamiltonian()
    test_get_parameters_from_dict()
    test_calculate_overlap()
    test_convert_state_from_str_to_arr()
    test_convert_state_from_bin_to_vec()
    test_compute_average()
    test_get_dimensionality()
    test_binomial()
    test_combo_rank()
    test_combos_with_reps()
    test_combos_without_reps()
    test_generate_all_binaries()
    test_number_of_restricted_compositions()
    test_findBitCombinations()
    test_lexRank()
    test_colexRank_multi()
    test_is_state_allowed()
    test_get_binary_from_rel_rank()
    test_find_ith_combination()
    test_find_string_from_rank()
    test_find_string_from_rank2()
    test_ground_state_Hubbard_half_filling()
    test_enumerated_product()
    test_check_dir()
    test_get_time_diff()
    test_get_matching_files()
    test_truncate_string()
    test_center_of_mass()
    test_clean_directories()
    test_ensure_list()
    test_ensure_list_of_lists() 
    test_get_times()
    test_memory_checks()
    test_check_available_memory()
    test_check_required_memory()
    print("==================================")
    print("\n")

    # Tests for visualization:
    print("==================================")
    print("Visualization testing: ")
    print("==================================")
    test_visualize_Hamiltonian()
    test_visualize_eigenvalues()
    test_visualize_eigenstate()
    test_visualize_eigenstate_multicomponent()
    test_plot_time_evolution_spinless()
    test_plot_density_vs_t_spinless()
    test_plot_density_vs_t_spinful()
    test_plot_dynamics_correlation()
    test_movie_time_evolution_spinless()
    test_movie_time_evolution_spinful()
    test_plot_eigenvalues_vs_parameter()
    test_plot_observable_vs_one_parameter()
    test_plot_observable_vs_two_parameters()
    print("==================================")
    print("\n")

    print("==================================")
    print("==================================")
    print("==================================")
    print(" UNIT TESTING FOR EDITH COMPLETED ")
    print("==================================")
    print("==================================")
    print("==================================")

######################################################################################
######################################################################################
######################################################################################
######################################################################################
