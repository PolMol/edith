#!/usr/bin/env python

####################################
###########  IMPORTS   #############
####################################
from __future__ import division
from __future__ import print_function
import numpy as np
import ast

# Custom modules:
from . import input as inp
from . import utils
####################################


######################################################################################
class Hamiltonian:
    """
    Methods
    ----------
     

    Parameters
    ----------
    

    Examples
    --------
  
    """
  
  
    def __init__(self, 
                 input,
                 **kwargs):
        
        """
        Parameters:
        ----------
        input, instance of Input class

        """
        
        self.model = input.model
        self.stats = input.stats
        self.sites = input.sites
        self.BC = input.BC
        self.sparse = input.sparse
        self.pars = self.get_parameters(input=input)
        if self.stats=="spinless-fermions" or self.stats=="anyons" or self.stats=="spin-0.5":
            self.Hilbert_dim = 2**self.sites
        elif self.stats=="spinful-fermions":
            self.Hilbert_dim = 4**self.sites
        elif self.stats=="bosons":
            n=0
            for N in range(0,self.sites):
                if self.pars["max_occ"] is None:
                    n += utils.binomial(self.sites + N - 1, N)
                else:
                    n += utils.number_of_restricted_compositions(N=N, L=self.sites, M=int(self.pars["max_occ"]))            
            self.Hilbert_dim = n
        else:
            spin = int(self.stats[5:])
            self.Hilbert_dim = input.spin_dim(spin, input.sites)    # not defined!

        #self.plot_flag = kwargs.get("plot_flag", True)
        self.input_checks(input)

        if self.sparse==True:
            self.rows, self.cols, self.mat_els = self.load_Hamiltonian_sparse(input)
        else:
            raise NotImplementedError
            self.matrix = self.load_Hamiltonian()

        #########################################

    ######################################################################################

    ######################################################################################
    def input_checks(self,
                     input):
        if self.model=="staggered-Heisenberg" and self.sites%2==1:
            raise NotImplementedError("Staggered Heisenberg model for odd number of sites not implemented!")
        
        if input.n_lowest_eigenvalues > self.Hilbert_dim:
            raise ArithmeticError(f"The number of lowest eigenvalues {input.n_lowest_eigenvalues} should be less (or equal) than the Hilbert space dimension {self.Hilbert_dim}!")
        
        if self.model=="staggered-Heisenberg" and self.pars["sz"] is not None and input.plot_evecs == True:
            raise NotImplementedError(f"Displaying eigenvectors for {self.model} presently requires calculation of all the sz spin sectors!")
        
        if self.model=="fermionic-interacting-Hatano-Nelson" and self.BC=="OBC" and not (np.any(self.pars["gamma"])==0.0 or self.pars["gamma"] is None):
            raise NotImplementedError(f"You have selected open boundary conditions but set a nonzero value of parameter gamma!")

    ######################################################################################

    ######################################################################################
    def load_Hamiltonian_sparse(self,
                                input):
        """
        Creates and stores the matrix elements of the given Hamiltonian.

        Parameters:
        -----------
        All through self

        Returns:
        --------
        (rows, cols, mat_el), where
        rows (list of ints): row index of non-zero matrix elements
        cols (list of ints): column index of non-zero matrix elements
        mat_el (list of floats): value of non-zero matrix elements

        """

        # Empty lists for sparse Hamiltonian
        rows = []
        cols = []
        mat_els = []

        ######################################################################################################################################


        ######################################################################################################################################
        ######################################################################################################################################
        ######### SPIN 1/2 MODELS
        ######################################################################################################################################
        ######################################################################################################################################
        if self.model=="transverse-Ising":
            self.sector = None
            
            bonds = self.get_bonds(d=1)

            # Run through all the spin configurations
            for state in range(self.Hilbert_dim):

                # Apply Ising coupling:
                ising_diagonal = 0
                for bond in bonds:

                    # parallel Ising bonds have energy +J, antiparallel Ising bonds have energy -J:
                    if self.get_site_value(state, bond[0]) == self.get_site_value(state, bond[1]):
                        ising_diagonal += self.pars["J"]
                    else:
                        ising_diagonal -= self.pars["J"]
                
                # Store data:    
                rows.append(state)
                cols.append(state)
                mat_els.append(ising_diagonal)

                # Apply transverse field
                for site in range(self.sites):

                    # The states are coupled (and corresponding Hamiltonian element is nonzero) 
                    # only if they can be mapped onto each other via a spin flip,
                    # e.g.: 1110111 <-> 1111111.
                    # This can be encoded with XOR operator (^ in python):
                    # 119 = 1110111 ^ 0001000 = 8 ==> 1111111 = 127

                    flipped_state = state ^ (1 << site) # "1 << site" will put a unique one in the binary representation of the state at the given site.
                    rows.append(flipped_state)
                    cols.append(state)
                    mat_els.append(self.pars["hx"])
        
            # Add empty matrix element in the last row and column (if it's not nonempty)
            # otherwise the sparse matrix diagonalizer will think the dimension is smaller
            if (self.Hilbert_dim - 1) not in rows and (self.Hilbert_dim - 1) not in cols:
                rows.append(self.Hilbert_dim - 1)
                cols.append(self.Hilbert_dim - 1)
                mat_els.append(0)  
        ######################################################################################################################################



        ######################################################################################################################################
        elif self.model=="staggered-Heisenberg":

            if self.pars["sz"] is not None: # Only calculate things for one sz-sector:
                sz_list = [int(self.pars["sz"])]
                self.sector = self.pars["sz"]
            else:   # Calculate all sz-sectors:
                sz_list = range(-self.sites//2, self.sites // 2 + self.sites % 2 + 1)
                self.sector = None
            #print(sz_list)

            # Generate basis states in lexicographic order:
            self.basis_states = []
            for sz in sz_list:  

                # check if sz is valid
                assert (sz <= (self.sites // 2 + self.sites % 2)) and (sz >= -self.sites//2)

                #self.basis_states = np.concatenate((self.basis_states,self.generate_basis_states_lex(sz)))   # Problem: np.concatenate somehow converts ints into floats!
                self.basis_states += self.generate_basis_states_lex(sz)

                if input.verbose==True:
                    print("basis states:", self.basis_states)

            # Assemble Hamiltonian action per sz-sector:
            for sz in sz_list:
            
                if input.verbose == True:
                    print(f"Working on sz={sz} sector...")

                # Define chain lattice
                bonds = self.get_bonds(d=1)

                # Run through all spin configurations with fixed total sz
                for state in self.basis_states:

                    # Apply diagonal Ising bonds
                    diagonal = 0
                    for bond in bonds:
                        if self.get_site_value(state, bond[0]) == self.get_site_value(state, bond[1]):
                            diagonal += self.pars["J"]/4
                        else:
                            diagonal -= self.pars["J"]/4

                    # Apply diagonal staggered Sz field
                    for site in range(0, self.sites, 2):
                        diagonal += self.pars["hs"]*(2*self.get_site_value(state, site) - 1)
                        diagonal -= self.pars["hs"]*(2*self.get_site_value(state, site+1) - 1)

                    rows.append(state)
                    cols.append(state)
                    mat_els.append(diagonal)

                    if input.verbose==True:
                        print("basis state:", state)

                    # Apply exchange interaction
                    for bond in bonds:
                        if input.verbose==True:
                            print("bond:", bond)
                        flipmask = (1 << bond[0]) | (1 << bond[1])  # This gives two ones at the positions where the spins should be flipped
                        if self.get_site_value(state, bond[0]) != self.get_site_value(state, bond[1]):
                            new_state = state ^ flipmask
                            if input.verbose==True:
                                print("new state:", new_state)
                            #new_state_index = basis_states.index(new_state)

                            rows.append(state)
                            cols.append(new_state)
                            mat_els.append(self.pars["J"]/2)

            # Add empty matrix element in the last row and column (if it's not nonempty)
            # otherwise the sparse matrix diagonalizer will think the dimension is smaller
            if self.pars["sz"] is None:
                if (self.Hilbert_dim - 1) not in rows and (self.Hilbert_dim - 1) not in cols:
                    rows.append(self.Hilbert_dim - 1)
                    cols.append(self.Hilbert_dim - 1)
                    mat_els.append(0)
            else:
                if (self.sector_dim - 1) not in rows and (self.sector_dim - 1) not in cols: 
                    rows.append(self.sector_dim - 1)
                    cols.append(self.sector_dim - 1)
                    mat_els.append(0)
        ######################################################################################################################################


        ######################################################################################################################################
        elif self.model=="disordered-XXZ-bath":

            if self.pars["sz"] is not None: # Only calculate things for one sz-sector:
                sz_list = [int(self.pars["sz"])]
                self.sector = self.pars["sz"]
                Nup = int(self.sites/2.0 + int(self.pars["sz"]))
                self.sector_dim = utils.binomial(self.sites, Nup)

            else:   # Calculate all sz-sectors:
                sz_list = range(-self.sites//2, self.sites // 2 + self.sites % 2 + 1)
                self.sector = None
                self.sector_dim = None
            
            # Generate disordered magnetic field:
            pot = np.zeros(self.sites)
            if np.any(self.pars["W"]) is None:
                for site in range(0,int(self.pars["LA"])):
                    pot[site] = (1-2*np.random.rand())*self.pars["Wmax"][0]    # subsystem A

                for site in range(int(self.pars["LA"]), self.sites):
                    pot[site] = (1-2*np.random.rand())*self.pars["Wmax"][1]    # subsystem B (bath)
            else:
                for i in range(len(pot)):
                    pot[i] = self.pars["W"][i]
            self.potential = pot

            # Generate basis states in colexicographic order:
            self.basis_states = {}
            for sz in sz_list:  

                # check if sz is valid
                assert (sz <= (self.sites // 2 + self.sites % 2)) and (sz >= -self.sites//2)
                self.basis_states.update(self.generate_basis_states_lex(sz))

                if input.verbose==True:
                    print("basis states:", self.basis_states)

            # Assemble Hamiltonian action per sz-sector:
            for sz in sz_list:
            
                if input.verbose == True:
                    print(f"Working on sz={sz} sector...")

                # Define chain lattice
                bonds = self.get_bonds(d=1)

                # Run through all spin configurations with fixed total sz
                sector_dic = self.basis_states[sz]

                for _, ranks in sector_dic.items():
                    state = ranks['abs_rank']       # The state as integer representation of the binary (e.g. '1010' -> 10)

                    # Apply diagonal Ising bonds for A and B subsystems
                    diagonal = 0
                    for bond in bonds:

                        if self.get_site_value(state, bond[0]) == self.get_site_value(state, bond[1]):
                            if bond[1] < int(self.pars["LA"]):
                                diagonal += self.pars["J"]*self.pars["DeltaA"]/4
                            elif bond[1] == int(self.pars["LA"]):
                                diagonal += self.pars["J"]*self.pars["DeltaAB"]/4
                            else:
                                diagonal += self.pars["J"]*self.pars["DeltaB"]/4
                        else:
                            if bond[1] < int(self.pars["LA"]):
                                diagonal -= self.pars["J"]*self.pars["DeltaA"]/4
                            elif bond[1] == int(self.pars["LA"]):
                                diagonal -= self.pars["J"]*self.pars["DeltaAB"]/4
                            else:
                                diagonal -= self.pars["J"]*self.pars["DeltaB"]/4

                    # Apply disordered Sz field
                    for site in range(0, self.sites):
                        diagonal += self.potential[site]*0.5*(2*self.get_site_value(state, site) - 1)

                    if self.pars["sz"] is not None: # Only calculate things for one sz-sector:
                        state_idx = ranks['rel_rank']    # The state index in the current sz-sector basis (e.g. '1010' -> 4)
                        rows.append(state_idx)
                        cols.append(state_idx)
                        mat_els.append(diagonal)

                    else:   # Calculate all magnetization sectors:
                        rows.append(state)
                        cols.append(state)
                        mat_els.append(diagonal)

                    if input.verbose==True:
                        print("basis state:", state)

                    # Apply exchange interaction
                    for bond in bonds:
                        if input.verbose==True:
                            print("bond:", bond)
                        flipmask = (1 << bond[0]) | (1 << bond[1])  # This gives two ones at the positions where the spins should be flipped
                        if self.get_site_value(state, bond[0]) != self.get_site_value(state, bond[1]):
                            new_state = state ^ flipmask
                            if input.verbose==True:
                                print("new state:", new_state)
                            #new_state_index = basis_states.index(new_state)


                            if self.pars["sz"] is not None: # Only calculate things for one sz-sector:                        
                                state_idx = ranks['rel_rank']    # The state index in the current sz-sector basis (e.g. '1010' -> 4)
                                new_state_idx = sector_dic[np.binary_repr(new_state, width=self.sites)]['rel_rank']
                                rows.append(state_idx)
                                cols.append(new_state_idx)
                                mat_els.append(self.pars["J"]/2)

                            else:   # Calculate all magnetization sectors:
                                rows.append(state)
                                cols.append(new_state)
                                mat_els.append(self.pars["J"]/2)

            # Add empty matrix element in the last row and column (if it's not nonempty)
            # otherwise the sparse matrix diagonalizer will think the dimension is smaller
            if self.pars["sz"] is None:
                if (self.Hilbert_dim - 1) not in rows and (self.Hilbert_dim - 1) not in cols:
                    rows.append(self.Hilbert_dim - 1)
                    cols.append(self.Hilbert_dim - 1)
                    mat_els.append(0)
            else:
                if (self.sector_dim - 1) not in rows and (self.sector_dim - 1) not in cols: 
                    rows.append(self.sector_dim - 1)
                    cols.append(self.sector_dim - 1)
                    mat_els.append(0)
        ######################################################################################################################################

        ######################################################################################################################################
        elif self.model=="XXZ-DM":

            # Consistency check regarding sz sector:
            if np.any(self.pars["D"][:4])!=0 and self.pars["sz"] is not None:
                print("The sz component is NOT conserved if Dx or Dy is nonzero! Performing full exact diagonalization instead.")
                self.pars["sz"]=None


            if self.pars["sz"] is not None: # Only calculate things for one sz-sector:
                sz_list = [int(self.pars["sz"])]
                self.sector = self.pars["sz"]
                Nup = int(self.sites/2.0 + int(self.pars["sz"]))
                self.sector_dim = utils.binomial(self.sites, Nup)

            else:   # Calculate all sz-sectors:
                sz_list = range(-self.sites//2, self.sites // 2 + self.sites % 2 + 1)
                self.sector = None
                self.sector_dim = None

            # Generate basis states in colexicographic order:
            self.basis_states = {}
            for sz in sz_list:  

                # check if sz is valid
                assert (sz <= (self.sites // 2 + self.sites % 2)) and (sz >= -self.sites//2)
                self.basis_states.update(self.generate_basis_states_lex(sz))

                if input.verbose==True:
                    print("basis states:", self.basis_states)

            # Assemble Hamiltonian action per sz-sector:
            for sz in sz_list:
            
                if input.verbose == True:
                    print(f"Working on sz={sz} sector...")

                # Define chain lattice
                bonds = self.get_bonds(d=1)

                # Run through all spin configurations with fixed total sz
                sector_dic = self.basis_states[sz]

                for _, ranks in sector_dic.items():
                    state = ranks['abs_rank']       # The state as integer representation of the binary (e.g. '1010' -> 10)

                    # Apply diagonal Ising bonds:
                    diagonal = 0
                    for bond in bonds:

                        if self.get_site_value(state, bond[0]) == self.get_site_value(state, bond[1]):
                            diagonal += self.pars["J"]*self.pars["Delta"]/4
                        else:
                            diagonal -= self.pars["J"]*self.pars["Delta"]/4

                    if self.pars["sz"] is not None: # Only calculate things for one sz-sector:
                        state_idx = ranks['rel_rank']    # The state index in the current sz-sector basis (e.g. '1010' -> 4)
                        rows.append(state_idx)
                        cols.append(state_idx)
                        mat_els.append(diagonal)

                    else:   # Calculate all magnetization sectors:
                        rows.append(state)
                        cols.append(state)
                        mat_els.append(diagonal)

                    if input.verbose==True:
                        print("basis state:", state)

                    # Apply exchange interactions and DM interactions
                    for bond in bonds:
                        if input.verbose==True:
                            print("bond:", bond)
                        
                        inter = 0
                        if self.get_site_value(state, bond[0])==self.get_site_value(state, bond[1]):
                            # Note that Dx and Dy interactions do not conserve sz component!
                            if self.pars["sz"] is None:
                                flipmask = 1 << bond[0]  # only flip first spin:
                                new_state = state ^ flipmask
                                if self.get_site_value(state, bond[0])==1:
                                    dm = (self.pars["D"][2] - 1j*self.pars["D"][3] + 1j*self.pars["D"][0] + self.pars["D"][1])/4 #(Dy-i*Dx) interaction first summand for |11>
                                else:
                                    dm = -(self.pars["D"][2] - 1j*self.pars["D"][3] - 1j*self.pars["D"][0] - self.pars["D"][1])/4 #(Dy+i*Dx) interaction second summand for |00>
                                    
                                rows.append(state)
                                cols.append(new_state)
                                mat_els.append(dm)

                                flipmask = 1 << bond[1]  # only flip second spin:
                                new_state = state ^ flipmask 
                                if self.get_site_value(state, bond[0])==1:
                                    dm = -(self.pars["D"][2] - 1j*self.pars["D"][3] + 1j*self.pars["D"][0] + self.pars["D"][1])/4 #(Dy-i*Dx) interaction second summand for |11>
                                else:
                                    dm = (self.pars["D"][2] - 1j*self.pars["D"][3] - 1j*self.pars["D"][0] - self.pars["D"][1])/4 #(Dy+i*Dx) interaction first summand for |00>

                                rows.append(state)
                                cols.append(new_state)
                                mat_els.append(dm)

                        elif self.get_site_value(state, bond[0]) != self.get_site_value(state, bond[1]):    # |...10...> or |...01...>

                            # Heisenberg exchange + z-component of DM:
                            flipmask = (1 << bond[0]) | (1 << bond[1])  # This gives two ones at the positions where the spins should be flipped
                            new_state = state ^ flipmask
                            if input.verbose==True:
                                print("new state:", new_state)
                            #new_state_index = basis_states.index(new_state)
                            exchange = self.pars["J"]/2
                            if self.get_site_value(state, bond[0]) == 0: # action on |10>
                                dm = 1j*(self.pars["D"][4] + 1j*self.pars["D"][5])/2
                            else: # action on |01>
                                dm = -1j*(self.pars["D"][4] + 1j*self.pars["D"][5])/2
                            inter += exchange + dm
                        
                            if self.pars["sz"] is not None: # Only calculate things for one sz-sector:                        
                                state_idx = ranks['rel_rank']    # The state index in the current sz-sector basis (e.g. '1010' -> 4)
                                new_state_idx = sector_dic[np.binary_repr(new_state, width=self.sites)]['rel_rank']
                                rows.append(state_idx)
                                cols.append(new_state_idx)
                                mat_els.append(inter)

                            else:   # Calculate all magnetization sectors:
                                rows.append(state)
                                cols.append(new_state)
                                mat_els.append(inter)

                            # Additional DM interaction only (|10> -> |00>, |11> and |01> -> |00>, |11>)
                            # Note that Dx and Dy interactions do not conserve sz component!
                            if self.pars["sz"] is None:                      
                                flipmask = 1 << bond[0]  # only flip first spin:
                                new_state = state ^ flipmask
                                if self.get_site_value(state, bond[0])==0:
                                    dm = (self.pars["D"][2] - 1j*self.pars["D"][3] - 1j*self.pars["D"][0] - self.pars["D"][1])/4 #(Dy+i*Dx) interaction first summand for |10>
                                else:
                                    dm = -(self.pars["D"][2] - 1j*self.pars["D"][3] + 1j*self.pars["D"][0] + self.pars["D"][1])/4 #-(Dy-i*Dx) interaction second summand for |01>
                                    
                                rows.append(state)
                                cols.append(new_state)
                                mat_els.append(dm)

                                flipmask = 1 << bond[1]  # only flip second spin:
                                new_state = state ^ flipmask
                                if self.get_site_value(state, bond[0])==0:
                                    dm = (self.pars["D"][2] - 1j*self.pars["D"][3] + 1j*self.pars["D"][0] + self.pars["D"][1])/4 #(Dy-i*Dx) interaction second summand for |10>
                                else:
                                    dm = -(self.pars["D"][2] - 1j*self.pars["D"][3] - 1j*self.pars["D"][0] - self.pars["D"][1])/4 #-(Dy+i*Dx) interaction first summand for |01>
                                    
                                rows.append(state)
                                cols.append(new_state)
                                mat_els.append(dm)

            # Add empty matrix element in the last row and column (if it's not nonempty)
            # otherwise the sparse matrix diagonalizer will think the dimension is smaller
            if self.pars["sz"] is None:
                if (self.Hilbert_dim - 1) not in rows and (self.Hilbert_dim - 1) not in cols:
                    rows.append(self.Hilbert_dim - 1)
                    cols.append(self.Hilbert_dim - 1)
                    mat_els.append(0)
            else:
                if (self.sector_dim - 1) not in rows and (self.sector_dim - 1) not in cols: 
                    rows.append(self.sector_dim - 1)
                    cols.append(self.sector_dim - 1)
                    mat_els.append(0)
        ######################################################################################################################################


        ######################################################################################################################################
        ######################################################################################################################################
        ######### BOSONIC MODELS
        ######################################################################################################################################
        ######################################################################################################################################
        ######################################################################################################################################
        elif self.model=="bosonic-interacting-Hatano-Nelson":

            if self.pars["N"] is not None: # Only calculate things for one N-sector:
                N_list = [int(self.pars["N"])]
                self.sector = int(self.pars["N"])

                if self.pars["max_occ"] is None:
                    self.sector_dim = utils.binomial(self.sites + int(self.pars["N"]) - 1, int(self.pars["N"]))
                else:
                    self.sector_dim = utils.number_of_restricted_compositions(N=int(self.pars["N"]), L=self.sites, M=int(self.pars["max_occ"]))

            else:   # Calculate all N-particle sectors:
                N_list = range(0, self.sites, 1) 
                self.sector = None
                self.sector_dim = self.Hilbert_dim

            # Generate disordered on-site potential:
            pot = np.zeros(self.sites)
            if np.any(self.pars["V"]) is None:
                for site in range(self.sites):
                    pot[site] = (1-2*np.random.rand())*self.pars["Vmax"]
            else:
                for i in range(len(pot)):
                    pot[i] = self.pars["V"][i]
            self.potential = pot

            # Generate basis states in colexicographic order:
            self.basis_states = {}
            for N in N_list:  

                # check if N is valid
                assert N >= 0
                self.basis_states.update(self.generate_basis_states_lex_bosons(N, max_occ=self.pars["max_occ"]))

                if input.verbose==True:
                    print("basis states:", self.basis_states)

            # Assemble Hamiltonian action per N-sector:
            for N in N_list:
            
                if input.verbose == True:
                    print(f"Working on N={N} sector...")

                # Run through all configurations with fixed total particle number N
                sector_dic = self.basis_states[N]

                for state_str, ranks in sector_dic.items():

                    if input.verbose==True:
                        print("Working on state:", state_str)

                    # !!!!!!!!! FOR BOSONS THE STATE CONFIGURATION IS A PYTHON LIST (SAVED AS STRING) !!!!!!!!!!
                    state = ast.literal_eval(state_str)      # The state as vector representation (list)
                    
                    # Apply diagonal terms (on-site potential, interactions)
                    diagonal = 0

                    ############################################
                    # on-site potential:
                    ############################################
                    if input.verbose==True:
                        print(f"Assembling on-site disordered potential part for state {state_str}...")

                    onsite_pot=0
                    for site in range(self.sites):
                        if input.verbose==True:
                            print("site:", site)
                        
                        # check whether current site is occupied or empty in current state.
                        # If it's occupied (!=0), add the on-site potential for that site:
                        if self.get_site_value(state, site) != 0:
                            onsite_pot += self.potential[site]*self.get_site_value(state, site)
                    
                    if input.verbose==True:
                        print("Potential:", onsite_pot)
                    ############################################

                    ############################################
                    # on-site interactions:
                    ############################################
                    if input.verbose==True:
                        print(f"Assembling on-site interaction part for state {state_str}...")

                    U = self.pars["U"]*0.5

                    inter_onsite=0              # variable that stores the total interaction strength for the current state
                    for site in range(self.sites):
                        if input.verbose==True:
                            print("site:", site)
                        inter_onsite += U*self.get_site_value(state, site)*(self.get_site_value(state, site)-1)


                    if input.verbose==True:
                        print(f"Total on-site interaction strength:", inter_onsite)
                    ############################################


                    ############################################
                    # long-range interactions:
                    ############################################
                    if input.verbose==True:
                        print(f"Assembling nn interaction part for state {state_str}...")

                    eta = self.pars["eta"]

                    bond_dict = {}
                    for d in range(1,len(eta)+1):
                        bond_dict["bond_"+str(d)] = self.get_bonds(d=d)
                    
                    inter=0     # variable that stores the total interaction strength for the current state
                    for key in bond_dict:
                        bonds_lr = bond_dict[key]
                        d = int(key[5:])
                        
                        inter_d=0   # variable that stores the interaction strength between particles at distance d
                        for bond_lr in bonds_lr:
                            if input.verbose==True:
                                print("long-range bond:", bond_lr)

                            # check whether current site and other site connected by long-range bond are both occupied.
                            # If they are, add the long-range interaction:
                            if self.get_site_value(state, bond_lr[0]) != 0 and self.get_site_value(state, bond_lr[1]) != 0:
                                inter_d += eta[d-1]*self.get_site_value(state, bond_lr[0])*self.get_site_value(state, bond_lr[1])

                                if input.verbose==True:
                                    print("Local interaction strength:", eta[d-1])
                        if input.verbose==True:
                            print(f"Total interaction strength at distance {d}:", inter_d)
                        inter += inter_d
                    ############################################
                    if input.verbose==True:
                        print(f"Total interaction strength (sum over all distances with weights):", inter)

                    if self.pars["N"] is not None: # Only calculate things for one N-sector:
                        state_idx = ranks['rel_rank']
                    else:   # Calculate all N-particle sectors:
                        state_idx = ranks['abs_rank']

                    rows.append(state_idx)
                    cols.append(state_idx)
                    mat_els.append(onsite_pot + inter + inter_onsite)

                    ############################################
                    # hopping:
                    ############################################
                    if input.verbose==True:
                        print("Assembling hopping part...")

                    bonds_nn = self.get_bonds(d=1)  #If PBC/APBC, this will also include hopping from first to last site
                    
                    for bond_nn in bonds_nn:
                        if input.verbose==True:
                            print("nn bond:", bond_nn)

                        site_occ_ann = self.get_site_value(state, bond_nn[0]) 
                        site_occ_cre = self.get_site_value(state, bond_nn[1]) 
                        # RIGHT hopping (i -> i+1) !!! Note that in the way we save the Hamiltonian (by rows), i.e. we save H_{i,i+1}, the action *produces* the starting state.
                        if site_occ_ann != 0 and site_occ_cre != self.pars["max_occ"]:
                            
                            # Generate the new state by performing the hopping in particle number:
                            new_state = state.copy()
                            new_state[-(bond_nn[0]+1)] -= 1     # the funny element access is b/c sites are defined from right to left.
                            new_state[-(bond_nn[1]+1)] += 1

                            if input.verbose==True:
                                print("new state:", new_state)

                            if self.pars["N"] is not None: # Only calculate things for one N-sector:
                                state_idx = ranks['rel_rank']                           # The original state colexicographic index in the current N-sector basis (e.g. L=4, N=3: '0012' -> 1)
                                new_state_idx = sector_dic[str(new_state)]['rel_rank']  # The new state colexicographic index in the current N-sector basis (e.g. L=4, N=3: '0021' -> 4)
                            else:   # Calculate all N-particle sectors:
                                state_idx = ranks['abs_rank']                           # The original state colexicographic index in the all N-sector basis (e.g. L=4, N=1-4: '0012' -> 6)
                                new_state_idx = sector_dic[str(new_state)]['abs_rank']  # The new state colexicographic index in the current N-sector basis (e.g. L=4, N=1-4: '0021' -> 9)                                
                            
                            rows.append(state_idx)
                            cols.append(new_state_idx)

                            # Check whether hopping is between ends (impurity):
                            if bond_nn[0] == (self.sites-1) and bond_nn[1] == 0:    # Hopping N <-> 0
                                if self.BC=="PBC":
                                    mat_els.append(self.pars["gamma"][1]*np.sqrt(site_occ_ann*(site_occ_cre+1)))   #This is gamma_L
                                elif self.BC=="APBC":
                                    mat_els.append(-self.pars["gamma"][1]*np.sqrt(site_occ_ann*(site_occ_cre+1)))

                            else:   # Hopping is not across ends of the chain (OBC)
                                mat_els.append(self.pars["J"][0]*np.sqrt(site_occ_ann*(site_occ_cre+1)))   # This is JR, because we list matrix elements by row, column (e.g. row=|0011>, col=<0101| --> mat_el= |0011><0101| --> (|0011><0101)|0101> = |0011> --> left hopping)

                        site_occ_ann = self.get_site_value(state, bond_nn[1]) 
                        site_occ_cre = self.get_site_value(state, bond_nn[0]) 
                        # LEFT hopping (i+1 -> i)
                        if site_occ_cre != self.pars["max_occ"] and site_occ_ann != 0:

                            # Generate the new state by performing the hopping in particle number:
                            new_state = state.copy()
                            new_state[-(bond_nn[0]+1)] += 1
                            new_state[-(bond_nn[1]+1)] -= 1

                            if input.verbose==True:
                                print("new state:", new_state)

                            if self.pars["N"] is not None: # Only calculate things for one N-sector:
                                state_idx = ranks['rel_rank']                           # The original state colexicographic index in the current N-sector basis (e.g. L=4, N=3: '0012' -> 1)
                                new_state_idx = sector_dic[str(new_state)]['rel_rank']  # The new state colexicographic index in the current N-sector basis (e.g. L=4, N=3: '0021' -> 4)
                            else:   # Calculate all N-particle sectors:
                                state_idx = ranks['abs_rank']                           # The original state colexicographic index in the all N-sector basis (e.g. L=4, N=1-4: '0012' -> 6)
                                new_state_idx = sector_dic[str(new_state)]['abs_rank']  # The new state colexicographic index in the current N-sector basis (e.g. L=4, N=1-4: '0021' -> 9)                                
                            
                            rows.append(state_idx)
                            cols.append(new_state_idx)

                            # Check whether hopping is between ends (impurity):
                            if bond_nn[0] == (self.sites-1) and bond_nn[1] == 0:    # Hopping N <-> 0
                                if self.BC=="PBC":
                                    mat_els.append(self.pars["gamma"][0]*np.sqrt(site_occ_ann*(site_occ_cre+1)))   #This is gamma_R
                                elif self.BC=="APBC":
                                    mat_els.append(-self.pars["gamma"][0]*np.sqrt(site_occ_ann*(site_occ_cre+1)))

                            else:   # Hopping is not across ends of the chain (OBC)
                                mat_els.append(self.pars["J"][1]*np.sqrt(site_occ_ann*(site_occ_cre+1)))   # This is JL

            # Add empty matrix element in the last row and column (if it's not nonempty)
            # otherwise the sparse matrix diagonalizer will think the dimension is smaller
            if self.pars["N"] is None:
                if (self.Hilbert_dim - 1) not in rows and (self.Hilbert_dim - 1) not in cols:
                    rows.append(self.Hilbert_dim - 1)
                    cols.append(self.Hilbert_dim - 1)
                    mat_els.append(0)
            else:
                if (self.sector_dim - 1) not in rows and (self.sector_dim - 1) not in cols: 
                    rows.append(self.sector_dim - 1)
                    cols.append(self.sector_dim - 1)
                    mat_els.append(0)

        ######################################################################################################################################
        ######################################################################################################################################
        ######### SPINLESS FERMIONIC MODELS
        ######################################################################################################################################
        ######################################################################################################################################
        ######################################################################################################################################
        elif self.model=="dipolar-Aubry-Andre":

            if self.pars["N"] is not None: # Only calculate things for one N-sector:
                N_list = [int(self.pars["N"])]
                self.sector_dim = utils.binomial(self.sites, int(self.pars["N"]))
                self.sector = int(self.pars["N"])
            else:   # Calculate all N-particle sectors:
                N_list = range(0, self.sites, 1)
                self.sector_dim = self.Hilbert_dim
                self.sector = None

            # Generate basis states in lexicographic order:
            self.basis_states = []
            for N in N_list:

                # check if N is valid
                assert (N <= self.sites) and (N >= 0)

                # Append basis states for current sector:
                self.basis_states += self.generate_basis_states_lex(N)

                if input.verbose==True:
                    print("basis states:")
                    for state in self.basis_states:
                        print("state:", bin(state), "lexicographic index:", state, "sector index:", utils.lexRank(np.binary_repr(state, width=self.sites))[0])
                
            # Assemble Hamiltonian action per N-particle sector:
            for N in N_list:
            
                if input.verbose == True:
                    print(f"Working on N={N} sector...")
                    
                bonds = self.get_bonds(d=1)

                # Run through all the fermionic configurations:
                for state in self.basis_states:
                
                    ############################################
                    # on-site potential:
                    ############################################
                    if input.verbose==True:
                        print("Assembling on-site quasiperiodic potential part...")

                    pot=0
                    for site in range(self.sites):
                        if input.verbose==True:
                            print("site:", site)
                        
                        # check whether current site is occupied or empty in current state.
                        # If it's occupied (=1), add the on-site potential for that site:
                        if self.get_site_value(state, site):
                            pot += self.pars["Delta"]*np.cos(2*np.pi*self.pars["beta"]*(site+1) + self.pars["phi"])
                    ############################################


                    ############################################
                    # interactions:
                    ############################################
                    if input.verbose==True:
                        print("Assembling nn interaction part...")

                    eta = self.pars["eta"]

                    bond_dict = {}
                    for d in range(1,len(eta)+1):
                        bond_dict["bond_"+str(d)] = self.get_bonds(d=d)

                    
                    for key in bond_dict:
                        bonds = bond_dict[key]
                        d = int(key[-1])
                        
                        inter=0
                        for bond in bonds:
                            if input.verbose==True:
                                print("bond:", bond)

                            # check whether current site and next site are both occupied.
                            # If they are, add the nearest-neighbor interaction:
                            if self.get_site_value(state, bond[0]) == 1 and self.get_site_value(state, bond[1]) == 1:
                                inter += eta[d-1]
                    ############################################

                    if self.pars["N"] is not None: # Only calculate things for one N-sector:
                        
                        state_idx = ranks['rel_rank']    # The state index in the current sz-sector basis (e.g. '1010' -> 4)
                        rows.append(state_idx)
                        cols.append(state_idx)
                        mat_els.append(pot + inter)

                    else:   # Calculate all N-particle sectors:
                        state_str = np.binary_repr(state, width=self.sites)
                        state_idx = ranks['rel_rank']    # The state index in the current sz-sector basis (e.g. '1010' -> 4)
                        rows.append(state_idx)
                        cols.append(state_idx)
                        mat_els.append(pot + inter)

                    ############################################
                    # hopping:
                    ############################################
                    if input.verbose==True:
                        print("Assembling hopping part...")

                    for bond in bonds:
                        if input.verbose==True:
                            print("bond:", bond)
                        
                        flipmask = (1 << bond[0]) | (1 << bond[1])  # This gives two ones at the positions where the hopping takes place
                        if self.get_site_value(state, bond[0]) != self.get_site_value(state, bond[1]):
                            new_state = state ^ flipmask    # Flip the values of the two sites affected by the hopping
                            if input.verbose==True:
                                print("new state:", new_state)

                            if self.pars["N"] is not None: # Only calculate things for one N-sector:
                                state_idx, _ = utils.lexRank(np.binary_repr(state, width=self.sites))
                                new_state_idx, _ = utils.lexRank(np.binary_repr(new_state, width=self.sites))

                                rows.append(state_idx)
                                cols.append(new_state_idx)
                                mat_els.append(self.pars["t"])

                            else:   # Calculate all N-particle sectors:
                                _, state_idx = utils.lexRank(np.binary_repr(state, width=self.sites))
                                _, new_state_idx = utils.lexRank(np.binary_repr(new_state, width=self.sites))
                            
                                rows.append(state_idx)
                                cols.append(new_state_idx)
                                mat_els.append(self.pars["t"])

            # Add empty matrix element in the last row and column (if it's not nonempty)
            # otherwise the sparse matrix diagonalizer will think the dimension is smaller
            if self.pars["N"] is None:
                if (self.Hilbert_dim - 1) not in rows and (self.Hilbert_dim - 1) not in cols:
                    rows.append(self.Hilbert_dim - 1)
                    cols.append(self.Hilbert_dim - 1)
                    mat_els.append(0)
            else:
                if (self.sector_dim - 1) not in rows and (self.sector_dim - 1) not in cols: 
                    rows.append(self.sector_dim - 1)
                    cols.append(self.sector_dim - 1)
                    mat_els.append(0)
        
                    ############################################

        ######################################################################################################################################
        elif self.model=="fermionic-interacting-Hatano-Nelson":

            if self.pars["N"] is not None: # Only calculate things for one N-sector:
                N_list = [int(self.pars["N"])]
                self.sector_dim = utils.binomial(self.sites, int(self.pars["N"]))
                self.sector = int(self.pars["N"])
            else:   # Calculate all N-particle sectors:
                N_list = range(0, self.sites, 1)
                self.sector_dim = self.Hilbert_dim
                self.sector = None

            # Generate disordered on-site potential:
            pot = np.zeros(self.sites)
            if np.any(self.pars["V"]) is None:
                for site in range(self.sites):
                    pot[site] = (1-2*np.random.rand())*self.pars["Vmax"]
            else:
                for i in range(len(pot)):
                    pot[i] = self.pars["V"][i]
            self.potential = pot

            # Generate basis states in colexicographic order:
            self.basis_states = {}
            for N in N_list:  

                # check if N is valid
                assert (N <= self.sites) and (N >= 0)
                self.basis_states.update(self.generate_basis_states_lex(N))

                if input.verbose==True:
                    print("basis states:", self.basis_states)

            # Assemble Hamiltonian action per N-sector:
            for N in N_list:
            
                if input.verbose == True:
                    print(f"Working on N={N} sector...")

                # Run through all configurations with fixed total particle number N
                sector_dic = self.basis_states[N]

                for bin_state, ranks in sector_dic.items():

                    if input.verbose==True:
                        print("Working on state:", bin_state)

                    state = ranks['abs_rank']       # The state as integer representation of the binary (e.g. '1010' -> 10)

                    # Apply diagonal terms (on-site potential, interactions)
                    diagonal = 0

                    ############################################
                    # on-site potential:
                    ############################################
                    if input.verbose==True:
                        print(f"Assembling on-site disordered potential part for state {bin_state}...")

                    onsite_pot=0
                    for site in range(self.sites):
                        if input.verbose==True:
                            print("site:", site)
                        
                        # check whether current site is occupied or empty in current state.
                        # If it's occupied (=1), add the on-site potential for that site:
                        if self.get_site_value(state, site):
                            onsite_pot += self.potential[site]
                    
                    if input.verbose==True:
                        print("Potential:", onsite_pot)
                    ############################################

                    ############################################
                    # interactions:
                    ############################################
                    if input.verbose==True:
                        print(f"Assembling nn interaction part for state {bin_state}...")

                    eta = self.pars["eta"]

                    bond_dict = {}
                    for d in range(1,len(eta)+1):
                        bond_dict["bond_"+str(d)] = self.get_bonds(d=d)
                    
                    inter=0
                    for key in bond_dict:
                        bonds_lr = bond_dict[key]
                        d = int(key[5:])
                        
                        inter_d=0
                        for bond_lr in bonds_lr:
                            if input.verbose==True:
                                print("long-range bond:", bond_lr)

                            # check whether current site and other site connected by long-range bond are both occupied.
                            # If they are, add the long-range interaction:
                            if self.get_site_value(state, bond_lr[0]) == 1 and self.get_site_value(state, bond_lr[1]) == 1:
                                inter_d += eta[d-1]
                                if input.verbose==True:
                                    print("Local interaction strength:", eta[d-1])
                        if input.verbose==True:
                            print(f"Total interaction strength at distance {d}:", inter_d)
                        inter += inter_d
                    ############################################
                    if input.verbose==True:
                        print(f"Total interaction strength (sum over all distances with weights):", inter)

                    if self.pars["N"] is not None: # Only calculate things for one N-sector:
                        state_idx = ranks['rel_rank']
                        rows.append(state_idx)
                        cols.append(state_idx)
                        mat_els.append(onsite_pot + inter)

                    else:   # Calculate all N-particle sectors:
                        rows.append(state)
                        cols.append(state)
                        mat_els.append(onsite_pot + inter)

                    ############################################
                    # hopping:
                    ############################################
                    if input.verbose==True:
                        print("Assembling hopping part...")

                    bonds_nn = self.get_bonds(d=1)  #If PBC/APBC, this will also include hopping from first to last site
                    
                    for bond_nn in bonds_nn:
                        if input.verbose==True:
                            print("nn bond:", bond_nn)
                        
                        flipmask = (1 << bond_nn[0]) | (1 << bond_nn[1])  # This gives two ones at the positions where the hopping takes place

                        if self.get_site_value(state, bond_nn[0]) != self.get_site_value(state, bond_nn[1]):
                            new_state = state ^ flipmask    # Flip the values of the two sites affected by the hopping
                            if input.verbose==True:
                                print("new state:", new_state, ",", np.binary_repr(new_state, width=self.sites))

                            if self.pars["N"] is not None: # Only calculate things for one N-sector:
                                state_idx = ranks['rel_rank']    # The state index in the current sz-sector basis (e.g. '1010' -> 4)
                                new_state_idx = sector_dic[np.binary_repr(new_state, width=self.sites)]['rel_rank']
                                rows.append(state_idx)
                                cols.append(new_state_idx)

                                # Check whether hopping is between ends (impurity):
                                if bond_nn[0] == (self.sites-1) and bond_nn[1] == 0:    # Hopping N <-> 0
                                    if self.get_site_value(state, 0) > self.get_site_value(state, self.sites-1):  #left end (first site) of target state is occupied, right end (last site) is empty -> hopping from last to first site
                                        if self.BC=="PBC":
                                            mat_els.append(self.pars["gamma"][1])   #This is gamma_R
                                        elif self.BC=="APBC":
                                            mat_els.append(-self.pars["gamma"][1])
                                    else:
                                        if self.BC=="PBC":
                                            mat_els.append(self.pars["gamma"][0])
                                        elif self.BC=="APBC":
                                            mat_els.append(-self.pars["gamma"][0])

                                else:   # Hopping is not across ends of the chain (OBC)

                                    # Check whether hopping is to the left or right
                                    if self.get_site_value(state, bond_nn[0]) > self.get_site_value(state, bond_nn[1]):  #target state on the left occupied, on the right empty -> hopping to the left
                                        mat_els.append(self.pars["J"][0])   # This is JL, because we list matrix elements by row, column (e.g. row=|0011>, col=<0101| --> mat_el= |0011><0101| --> (|0011><0101)|0101> = |0011> --> left hopping)
                                    else:
                                        mat_els.append(self.pars["J"][1])

                            else:   # Calculate all N-particle sectors:
                                rows.append(state)
                                cols.append(new_state)

                                # Check whether hopping is between ends (impurity):
                                if bond_nn[0] == (self.sites-1) and bond_nn[1] == 0:    # Hopping N <-> 0
                                    if self.get_site_value(state, 0) > self.get_site_value(state, self.sites-1):  #left end (first site) empty, right end (last site) occupied -> hopping to the left
                                        if self.BC=="PBC":
                                            mat_els.append(self.pars["gamma"][1])   #This is gamma_R
                                        elif self.BC=="APBC":
                                            mat_els.append(-self.pars["gamma"][1])
                                    else:
                                        if self.BC=="PBC":
                                            mat_els.append(self.pars["gamma"][0])
                                        elif self.BC=="APBC":
                                            mat_els.append(-self.pars["gamma"][0])
                                else:

                                    if self.get_site_value(state, bond_nn[0]) > self.get_site_value(state, bond_nn[1]):  #target state on the left occupied, on the right empty -> hopping to the left
                                        mat_els.append(self.pars["J"][0])
                                    else:
                                        mat_els.append(self.pars["J"][1])

            # Add empty matrix element in the last row and column (if it's not nonempty)
            # otherwise the sparse matrix diagonalizer will think the dimension is smaller
            if self.pars["N"] is None:
                if (self.Hilbert_dim - 1) not in rows and (self.Hilbert_dim - 1) not in cols:
                    rows.append(self.Hilbert_dim - 1)
                    cols.append(self.Hilbert_dim - 1)
                    mat_els.append(0)
            else:
                if (self.sector_dim - 1) not in rows and (self.sector_dim - 1) not in cols: 
                    rows.append(self.sector_dim - 1)
                    cols.append(self.sector_dim - 1)
                    mat_els.append(0)
        
                    ############################################

        ######################################################################################################################################

        ######################################################################################################################################
        ######################################################################################################################################
        ######### SPINFUL FERMION MODELS
        ######################################################################################################################################
        ######################################################################################################################################
        elif self.model=="Hubbard":
            if self.pars["N"] is not None and self.pars["sz"] is not None: # Only calculate things for one N_up and N_down-sector:

                N = self.pars["N"]
                sz = self.pars["sz"]
                Nup = int(N/2.0 + sz)       # (Nup - Ndown) = 2sz
                Ndown = int(N/2.0 - sz)     # Nup + Ndown = N
                Nup_list = [Nup]
                Ndown_list = [Ndown]
                print("Nup_list", Nup_list)
                print("Ndown_list", Ndown_list)
  
                self.Nup_sector_dim = utils.binomial(self.sites, Nup)
                self.Ndown_sector_dim = utils.binomial(self.sites, Ndown)
                self.Nup_sector = Nup
                self.Ndown_sector = Ndown
                self.sector_dim = self.Nup_sector_dim*self.Ndown_sector_dim
                self.sector = (Nup, Ndown)

            else:   # Calculate all N-particle sectors:
                Nup_list = range(0, self.sites, 1)
                Ndown_list = range(0, self.sites, 1)
                self.sector = None
                self.sector_dim = self.Hilbert_dim
                self.Nup_sector_dim = None
                self.Ndown_sector_dim = None
                self.Nup_sector = None
                self.Ndown_sector = None

                raise NotImplementedError()
            
            # Generate basis states in lexicographic order:
            self.basis_states_up = []
            self.basis_states_down = []
            for Nup in Nup_list:
                # check if Nup is valid
                assert (Nup <= self.sites) and (Nup >= 0)
                # Append basis states for current spin up sector:
                self.basis_states_up += self.generate_basis_states_lex(Nup)
                if input.verbose==True:
                    print("basis states spin up:")
                    for state in self.basis_states_up:
                        print("state:", bin(state), "lexicographic index:", state, "sector index:", utils.lexRank(np.binary_repr(state, width=self.sites))[0])
            for Ndown in Ndown_list:  
                # check if Ndown is valid
                assert (Ndown <= self.sites) and (Ndown >= 0)
                # Append basis states for current sector:
                self.basis_states_down += self.generate_basis_states_lex(Ndown)
                if input.verbose==True:
                    print("basis states spin down:")
                    for state in self.basis_states_down:
                        print("state:", bin(state), "lexicographic index:", state, "sector index:", utils.lexRank(np.binary_repr(state, width=self.sites))[0])
  
            # Assemble Hamiltonian action per N-particle sector:
            for Nup in Nup_list:
                for Ndown in Ndown_list:
            
                    if input.verbose == True:
                        print(f"Working on (Nup,Ndown)=({Nup},{Ndown}) sector...")
                    
                    bonds = self.get_bonds(d=1)

                    # Run through all the spin up and spin down configurations:
                    for state_up in self.basis_states_up:
                        for state_down in self.basis_states_down:

                            # Preparing indices for state of spin ups, state of spin downs, and tensor product:
                            state_up_idx, _ = utils.lexRank(np.binary_repr(state_up, width=self.sites))
                            state_down_idx, _ = utils.lexRank(np.binary_repr(state_down, width=self.sites))
                            tot_states_up = utils.binomial(self.sites, Nup)
                            tensor_state_idx = state_down_idx*tot_states_up + state_up_idx
                
                            ############################################
                            # on-site potential:
                            ############################################
                            if input.verbose==True:
                                print("Assembling on-site potential part...")

                            # Count the nonzero spin-ups and spin-downs,
                            # multiply w/ chemical potential
                            mu = self.pars["mu"]*(state_up.bit_count() + state_down.bit_count())
                            ############################################

                            ############################################
                            # on-site interaction:
                            ############################################
                            if input.verbose==True:
                                print("Assembling nn interaction part...")
                                
                            # Count all common 1's in state_up and state_down,
                            # corresponding to double occupancy, then multiply w/ U:
                            os_int = self.pars["U"]*(state_up & state_down).bit_count()
                            ############################################

                            if self.Nup_sector is not None and self.Ndown_sector is not None: # Only calculate things for one N-sector:
                        
                                rows.append(tensor_state_idx)
                                cols.append(tensor_state_idx)
                                mat_els.append(mu + os_int)

                            else:   # Calculate all N-particle sectors:
                                raise NotImplementedError()

                            ############################################
                            # hopping:
                            ############################################
                            if input.verbose==True:
                                print("Assembling hopping part...")

                            for bond in bonds:
                                if input.verbose==True:
                                    print("bond:", bond)
                        
                                flipmask = (1 << bond[0]) | (1 << bond[1])  # This gives two ones at the positions where the hopping takes place
                                
                                # Hopping on up-sector:
                                if self.get_site_value(state_up, bond[0]) != self.get_site_value(state_up, bond[1]):
                                    new_state_up = state_up ^ flipmask    # Flip the values of the two sites affected by the hopping
                                    if input.verbose==True:
                                        print("new state up:", new_state_up)

                                    if self.Nup_sector is not None and self.Ndown_sector is not None: # Only calculate things for one N-sector:
                                        
                                        # Get ordered lexicographic index for new states after hopping and corresponding index for the tensor product:
                                        new_state_up_idx, _ = utils.lexRank(np.binary_repr(new_state_up, width=self.sites))
                                        new_tensor_state_idx = state_down_idx*tot_states_up + new_state_up_idx

                                        rows.append(tensor_state_idx)
                                        cols.append(new_tensor_state_idx)
                                        mat_els.append(self.pars["t"])

                                # Hopping on down-sector:
                                if self.get_site_value(state_down, bond[0]) != self.get_site_value(state_down, bond[1]):
                                    new_state_down = state_down ^ flipmask    # Flip the values of the two sites affected by the hopping
                                    if input.verbose==True:
                                        print("new state down:", new_state_down)

                                    if self.Nup_sector is not None and self.Ndown_sector is not None: # Only calculate things for one N-sector:
                                        
                                        # Get ordered lexicographic index for new states after hopping and corresponding index for the tensor product:
                                        new_state_down_idx, _ = utils.lexRank(np.binary_repr(new_state_down, width=self.sites))
                                        new_tensor_state_idx = new_state_down_idx*tot_states_up + state_up_idx

                                        rows.append(tensor_state_idx)
                                        cols.append(new_tensor_state_idx)
                                        mat_els.append(self.pars["t"])

                                    else:   # Calculate all N-particle sectors:
                                        raise NotImplementedError()

            # Add empty matrix element in the last row and column (if it's not nonempty)
            # otherwise the sparse matrix diagonalizer will think the dimension is smaller
            if self.pars["N"] is None and self.pars["sz"] is None:
                if (self.Hilbert_dim - 1) not in rows and (self.Hilbert_dim - 1) not in cols:
                    rows.append(self.Hilbert_dim - 1)
                    cols.append(self.Hilbert_dim - 1)
                    mat_els.append(0)
            else:
                if (self.Nup_sector_dim*self.Ndown_sector_dim - 1) not in rows and (self.Nup_sector_dim*self.Ndown_sector_dim - 1) not in cols: 
                    rows.append(self.Nup_sector_dim*self.Ndown_sector_dim - 1)
                    cols.append(self.Nup_sector_dim*self.Ndown_sector_dim - 1)
                    mat_els.append(0)
        ######################################################################################################################################



        
        if input.verbose == True:
            print("rows:", rows)
            print("cols:", cols)
            print("matrix elements:", mat_els)

        return rows, cols, mat_els

    ######################################################################################


    ######################################################################################


    ######################################################################################
    def get_site_value(self, state, site):
        """
        Gets the spin value (spins) or occupation (fermions/bosons) at a given site.

        Notes:
        ------
        For spins 0.5/fermions:
            * state is an integer representing the state as binary representation.
        For bosons:
            * state is a list.
        For all:
            * The position of the site in the binary representation is from the RIGHT.
            * The site indices start from ZERO.
        
            e.g. value of site 3 in |10100101> is 0.
            e.g. value of site 2 in |10100101> is 1.


        """ 
        
        # >> is bitwise right shift operators e.g. 14 >> 1 means 14=00001110 --> 00000111=7
        #print(state)
        #print(site)
        if isinstance(state, int):
            return (state >> site) & 1
        elif isinstance(state, list):
            return state[-(site+1)]
    ######################################################################################


    ######################################################################################
    def get_bonds(self, d):
        
        # Define chain lattice
        if self.BC=="PBC" or self.BC=="APBC":
            bonds = [(site, (site+d)%self.sites) for site in range(self.sites)]
        else:
            bonds = [(site, site+d) for site in range(self.sites-d)]

        return bonds
    ######################################################################################


    ######################################################################################
    # Functions to create lexicographic basis
    def first_state_lex(self, L, sz):
        """ 
        Returns first state of Hilbert space in lexicographic order for fermionic systems.

        Parameters:
        -----------
        L, int: number of sites.
        sz, int: total spin sz (spins) or total number of particles (fermions).

        Notes:
        ------
        * The function works for both fermions and spin 1/2's, with the mapping:
          - spin up --> occupied site (1)
          - spin down --> empty site (0)
          - total spin sz --> total number of particles N

        """
        if self.stats == "spin-0.5":
            n = L//2 + int(sz)
        elif self.stats == "spinless-fermions" or self.stats == "spinful-fermions":
            n = int(sz)
        else:
            raise IOError("Lexicographic order is only defined for spin 1/2's and fermions at present!")
        #print((1 << n) - 1)
        return (1 << n) - 1
    ######################################################################################

    ######################################################################################
    def next_state_lex(self, state):
        """
        Returns the next state of Hilbert space for spin-1/2's or fermions in lexicographic order.

        Notes:
        -----
        This function implements a nice trick for spin 1/2 and fermions only,
        see http://graphics.stanford.edu/~seander/bithacks.html
        #NextBitPermutation for details.

        """

        t = (state | (state - 1)) + 1
        return t | ((((t & -t) // (state & -state)) >> 1) - 1)
    ######################################################################################

    ######################################################################################
    def last_state_lex(self, L, sz):
        """
        Returns the last state of the Hilbert space in lexicographic order.

        Notes:
        ------
        * The function works for both fermions and spin 1/2's, with the mapping:
          - spin up --> occupied site (1)
          - spin down --> empty site (0)
          - total spin sz --> total number of particles N

        """
        if self.stats == "spin-0.5":
            n = L//2 + int(sz)
        elif self.stats == "spinless-fermions" or self.stats == "spinful-fermions":
            n = int(sz)
        else:
            raise IOError("Lexicographic order is only defined for spin 1/2's and fermions at present!")
        return ((1 << n) - 1) << (L - n)
    ######################################################################################

    ######################################################################################
    def generate_basis_states_lex(self, sz):
        """
        Returns a dictionary of dictionaries containing a lexicographic ordering of all basis state state of the Hilbert space for the given sector sz.

        Notes:
        ------
        * The function works for both fermions and spin 1/2's, with the mapping:
          - spin up --> occupied site (1)
          - spin down --> empty site (0)
          - total spin sz --> total number of particles N

        """
    
        # Create list of states with fixed N
        basis_states = {}

        state = self.first_state_lex(self.sites, sz)
        state_bin = np.binary_repr(state, width=self.sites)
        rel_rank, _ = utils.lexRank(s=state_bin, get_abs_rank=False, verbose=False)
        basis_states[state_bin] = {"abs_rank": state, "rel_rank": rel_rank}

        end_state = self.last_state_lex(self.sites, sz)
        while state < end_state:
            state = self.next_state_lex(state)
            state_bin = np.binary_repr(state, width=self.sites)
            rel_rank, _ = utils.lexRank(s=state_bin, get_abs_rank=False, verbose=False)
            basis_states[state_bin] = {"abs_rank": state, "rel_rank": rel_rank}

        #basis_states.append(end_state)
        #print(basis_states)
        return {sz: basis_states}
    ######################################################################################

    ######################################################################################
    def first_state_lex_multi(self, L, N):
        x = [0] * L
        x[-1] = N
        return x
    ######################################################################################

    ######################################################################################
    def last_state_lex_multi(self, L, N):
        x = [0] * L 
        x[0] = N
        return x
    ######################################################################################
    
    ######################################################################################
    def next_state_lex_multi(self, state):
        """
        Enumerate all the weak compositions of n into k elements, starting from |000...0k>. Equivalently, enumerate all possible configurations of bosons into k sites.
    
        Parameters:
        -----------
        state, list or 1D numpy array: the state immediately preceding the state we want to create, e.g. state=|0021> --> |0111>.

        Returns:
        --------
        state, list or 1D numpy array: the state immediately following the input state in the lexicographic enumeration.


        Notes:
        ------
        The algorithm works by iteratively shifting one from the rightmost nonzero position towards the left, 
        while also "recycling" the leftmost nonzero number. E.g. for n=3, k=4:
        
        1)  0003 --> take 1 from leftmost nonzero number excluding the first (4th position), take value from first position (0, 1st position), add them to the (4th-1=3rd) position --> 0012
        2)  0012 --> take 1 from leftmost nonzero number excluding the first (3rd position), take value from first position (0, 1st position), add them to the (3rd-1=2nd) position --> 0102
        3)  0102 --> take 1 from leftmost nonzero number excluding the first (2nd position), take value from first position (0, 1st position), add them to the (2nd-1=1st) position --> 1002
        4)  1002 --> take 1 from leftmost nonzero number excluding the first (4th position), take value from first position (1, 1st position), add them to the (1st-1=3rd) position --> 0021
        5)  0021 --> take 1 from leftmost nonzero number excluding the first (3rd position), take value from first position (0, 1st position), add them to the (2nd-1=3rd) position --> 0111
        6)  0111 --> take 1 from leftmost nonzero number excluding the first (2nd position), take value from first position (0, 1st position), add them to the (2nd-1=1st) position --> 1011
        7)  1011 --> take 1 from leftmost nonzero number excluding the first (3rd position), take value from first position (1, 1st position), add them to the (3rd-1=2nd) position --> 0201
        8)  0201 --> take 1 from leftmost nonzero number excluding the first (2nd position), take value from first position (0, 1st position), add them to the (2nd-1=1st) position --> 1101
        9)  1101 --> take 1 from leftmost nonzero number excluding the first (2nd position), take value from first position (1, 1st position), add them to the (2nd-1=1st) position --> 2001
        10) 2001 --> take 1 from leftmost nonzero number excluding the first (4th position), take value from first position (2, 1st position), add them to the (4th-1=3rd) position --> 0030
        11) 0030 --> take 1 from leftmost nonzero number excluding the first (3rd position), take value from first position (0, 1st position), add them to the (3rd-1=2nd) position --> 0120
        12) 0120 --> take 1 from leftmost nonzero number excluding the first (2nd position), take value from first position (0, 1st position), add them to the (2nd-1=1st) position --> 1020
        13) 1020 --> take 1 from leftmost nonzero number excluding the first (3rd position), take value from first position (1, 1st position), add them to the (3rd-1=2nd) position --> 0210
        14) 0210 --> take 1 from leftmost nonzero number excluding the first (2nd position), take value from first position (0, 1st position), add them to the (2nd-1=1st) position --> 1110
        15) 1110 --> take 1 from leftmost nonzero number excluding the first (2nd position), take value from first position (1, 1st position), add them to the (2nd-1=1st) position --> 2010
        16) 2010 --> take 1 from leftmost nonzero number excluding the first (3rd position), take value from first position (2, 1st position), add them to the (3rd-1=2nd) position --> 0300
        17) 0300 --> take 1 from leftmost nonzero number excluding the first (2nd position), take value from first position (0, 1st position), add them to the (2nd-1=1st) position --> 1200
        18) 1200 --> take 1 from leftmost nonzero number excluding the first (2nd position), take value from first position (1, 1st position), add them to the (2nd-1=1st) position --> 2100
        19) 2100 --> take 1 from leftmost nonzero number excluding the first (2nd position), take value from first position (2, 1st position), add them to the (2nd-1=1st) position --> 3000
        20) 3000 --> end of loop

        """

        v = state[0]
        state[0] = 0
        j = 1
        while (0==state[j]):
            j += 1
        state[j] -= 1
        state[j-1] = 1 + v

        return state
    ######################################################################################
    

    ######################################################################################
    def generate_basis_states_lex_bosons(self, N, max_occ):
        """
        Returns a dictionary of dictionaries containing a colexicographic ordering of all basis state state of the Hilbert space for the given sector N and maximal occupation max_occ.

        Notes:
        ------

        """
        # Create list of states with fixed N
        basis_states = {}

        state_occ = self.first_state_lex_multi(self.sites, N)  # vector
        end_state_occ = self.last_state_lex_multi(self.sites, N)

        # Getting the total number of states with particle numbers <N For all particle sector enumeration:
        previous_N_dict_len=0
        if N>1:
            for n in range(1,N):
                previous_N_dict_len += utils.number_of_restricted_compositions(N=n, L=self.sites, M=max_occ)

        state_counter=0
        while state_occ != end_state_occ:

            # Full enumeration without maximal occupation restriction:
            if max_occ is None or max_occ >= N:
                rel_rank, abs_rank = utils.colexRank_multi(s=state_occ)
                basis_states[str(state_occ)] = {"abs_rank": abs_rank, "rel_rank": rel_rank}

            # Restrict the occupation, enumerate the states one by one (no rank function developed for A-restricted weak compositions developed yet)
            else:
                if utils.is_state_allowed(state_occ, max_occ):
                    
                    abs_rank = previous_N_dict_len + state_counter + 1
                    basis_states[str(state_occ)] = {"abs_rank": abs_rank, "rel_rank": state_counter}
                    state_counter+=1
            
            state_occ = self.next_state_lex_multi(state_occ)

        # Full enumeration without maximal occupation restriction:
        if max_occ is None or max_occ >= N:
            rel_rank, abs_rank = utils.colexRank_multi(s=end_state_occ)
            basis_states[str(end_state_occ)] = {"abs_rank": abs_rank, "rel_rank": rel_rank}

        # Restrict the occupation, enumerate the states one by one (no rank function developed for A-restricted weak compositions developed yet)
        else:
            if utils.is_state_allowed(end_state_occ, max_occ):
                #print(previous_N_dict_len, state_counter)    
                abs_rank = previous_N_dict_len + state_counter
                basis_states[str(end_state_occ)] = {"abs_rank": abs_rank, "rel_rank": state_counter}


        #print(basis_states)
        #print("\n")
        return {N: basis_states}
    ######################################################################################

    ######################################################################################
    def generate_basis_states_lex_list(self, sz):
        """
        DEPRECATED: Returns a lexicographic list of all basis state state of the Hilbert space.

        Notes:
        ------
        * The function works for both fermions and spin 1/2's, with the mapping:
          - spin up --> occupied site (1)
          - spin down --> empty site (0)
          - total spin sz --> total number of particles N

        * TODO: update Hubbard model Hamiltonian definition with newest basis generation in terms of dictionaries.

        """

        # Create list of states with fixed N
        basis_states = []
        state = self.first_state_lex(self.sites, sz)
        basis_states.append(state)
        end_state = self.last_state_lex(self.sites, sz)
        while state < end_state:
            state = self.next_state_lex(state)
            basis_states.append(state)
        #basis_states.append(end_state)
        #print(basis_states)
        return basis_states
    ######################################################################################

    ######################################################################################
    @classmethod
    def get_parameters(cls,
                       input):


        ####################################################
        # SPIN 1/2 MODELS
        ####################################################
        if input.model == "transverse-Ising":
            J = input.Ham_par1
            hx = input.Ham_par2
            pars = {"J": J,
                    "hx": hx}
            
        elif input.model == "staggered-Heisenberg":
            J = input.Ham_par1
            hs = input.Ham_par2
            sz = input.Ham_par3
            pars = {"J": J,
                    "hs": hs,
                    "sz": sz}

        elif input.model == "XXZ-DM":
            sz = input.Ham_par1
            J = input.Ham_par2
            Delta = input.Ham_par3
            D = input.Ham_par4
            pars = {"sz": sz,
                    "J": J,
                    "Delta": Delta,
                    "D": D,
                    }

        elif input.model == "disordered-XXZ-bath":
            J = input.Ham_par1
            DeltaA = input.Ham_par2
            DeltaB = input.Ham_par3
            DeltaAB = input.Ham_par4
            Wmax = input.Ham_par5
            W = input.Ham_par6
            LA = int(input.Ham_par7)
            #LA = input.Ham_par7
            sz = input.Ham_par8
            pars = {"J": J,
                    "DeltaA": DeltaA,
                    "DeltaB": DeltaB,
                    "DeltaAB": DeltaAB,
                    "Wmax": Wmax,
                    "W": W,
                    "LA": LA,
                    "sz": sz,
                    }
        ####################################################

        ####################################################
        # BOSONIC MODELS
        ####################################################
        elif input.model == "bosonic-interacting-Hatano-Nelson":
            N = input.Ham_par1
            if N is not None:
                N = int(N)
            J = input.Ham_par2              # vector J=[JL, JR]
            Vmax = input.Ham_par3     
            V = input.Ham_par4              # vector V=[V1,V2,V3,...]
            U = input.Ham_par5              # on-site interaction U
            eta = input.Ham_par6            # vector eta=[eta1, eta2, ...]
            gamma = input.Ham_par7          # vector gamma=[gammaL, gammaR]
            max_occ = input.Ham_par8        # maximal occupation per site
            if max_occ is not None:
                max_occ = int(max_occ)
            else:
                max_occ = N

            pars = {"N": N,
                    "J": J,
                    "Vmax": Vmax,
                    "V": V,
                    "U": U,
                    "eta": eta,
                    "gamma": gamma,
                    "max_occ": max_occ,
                    }

        ####################################################

        ####################################################
        # SPINLESS FERMIONIC MODELS
        ####################################################
        elif input.model == "dipolar-Aubry-Andre":
            N = int(input.Ham_par1)
            t = input.Ham_par2
            Delta = input.Ham_par3
            beta = input.Ham_par4
            phi = input.Ham_par5
            eta = input.Ham_par6
            pars = {"N": N,
                    "t": t,
                    "Delta": Delta,
                    "beta": beta,
                    "phi": phi,
                    "eta": eta}
            
        elif input.model == "fermionic-interacting-Hatano-Nelson":
            N = int(input.Ham_par1)
            J = input.Ham_par2          # vector J=[JL, JR]
            Vmax = input.Ham_par3     
            V = input.Ham_par4          # vector V=[V1,V2,V3,...]
            eta = input.Ham_par5        # vector eta=[eta1, eta2, ...]
            gamma = input.Ham_par6      # vector gamma=[gammaL, gammaR]
            pars = {"N": N,
                    "J": J,
                    "Vmax": Vmax,
                    "V": V,
                    "eta": eta,
                    "gamma": gamma,
                    }
        ####################################################

        ####################################################
        # SPINFUL FERMIONIC MODELS
        ####################################################
        elif input.model == "Hubbard":
            sz = input.Ham_par1
            N = int(input.Ham_par2)
            mu = input.Ham_par3
            t = input.Ham_par4
            U = input.Ham_par5
            pars = {"sz": sz,
                    "N": N,
                    "mu": mu,
                    "t": t,
                    "U": U}
        ####################################################

        # Sort dictionary:  (needed for saving/loading files properly)
        par_keys = list(pars.keys())
        par_keys.sort()
        sorted_pars = {i: pars[i] for i in par_keys}

        return sorted_pars

    ######################################################################################


######################################################################################
#TODO: get rid of this, deprecated function.
def get_state_population_spinless_old(Ham, state):
    """
    Get the population at each site for the given state for spinless fermions or spin 1/2's.

    Notes:
    ------
    This is equivalent to performing <psi| n_i |psi> for every particle number operator at each site i.
    
    e.g. N=2 |psi> = [1.2, 0.1, -0.1, 0.5] means
             |psi> = 1.2 |00> + 0.1 |01> - 0.1 |10> + 0.5 |11>

             n1 |psi> = 0.1 |01> + 0.5 |11>   -->  <n1> = <psi| n1 |psi> = |0.1|^2 + |0.5|^2
             n2 |psi> = -0.1 |10> + 0.5 |11>  -->  <n2> = <psi| n2 |psi> = |-0.1|^2 + |0.5|^2

             --> total spin profile is [<n1>, <n2>] = [0.26, 0.26].

    """

    stats = Ham.stats 
    sites = Ham.sites
    
    if Ham.sector_dim==None:    # state of the full Hamiltonian
        # TODO: rename variables here, they are confusing...

        d = len(state)      # Hilbert space dimension
        L = int(np.log2(d)) # sites

        # Initialize the amplitudes:
        evec = np.zeros(L, dtype=np.complex_)

        # Loop over all states in the Hilbert space, extract the eigenvector weight,
        # and multiply the corresponding sites with the weight:
        for i in range(d):
            bin = np.binary_repr(i, width=L)
            #print("binary ", bin)
            weight = np.abs(state[i])**2
            for idx, c in enumerate(bin):
                #print(c)
                if stats == "spin-0.5":
                    evec[sites - idx - 1] += (int(c)-0.5)*weight    # spins
                elif stats == "fermions":
                    evec[sites - idx - 1] += int(c)*weight    # fermions


    else:   # fixed particle number sector N, smaller dimension of full Hilbert space

        N = int(Ham.sector)
        # Get basis for sector:
        #sector_basis = Ham.generate_basis_states_lex_list(N)
        sector_basis_dic = Ham.basis_states[N]

        # Initialize the amplitudes:
        evec = np.zeros(Ham.sites, dtype=float)

        for bin,ranks in sector_basis_dic.items():
            b_state = ranks['abs_rank']           # integer used to store the state
            idx_b = ranks['rel_rank']           # index of basis state in the given sector
            weight = np.abs(state[idx_b])**2    # weight that the state has in the basis state b

            for site in range(sites):   # Loop over every element (site) of the basis state b
                if stats == "spin-0.5":
                    evec[site] += (Ham.get_site_value(b_state, site)-0.5)*weight    # spins
                elif stats == "spinless-fermions":
                    evec[site] += Ham.get_site_value(b_state, site)*weight    # fermions

    return evec

######################################################################################

######################################################################################
def get_state_population_spinless(Ham, state):
    """
    Get the population at each site for the given state for spinless fermions or spin 1/2's.

    """

    # Initialize the amplitudes:
    pop_vec = np.zeros(Ham.sites, dtype=float)

    # Populate all the entries in the array:
    for site in range(Ham.sites):
        pop_vec[site] = get_state_population_spinless_site(Ham=Ham, state=state, site=site)

    return pop_vec

######################################################################################

######################################################################################
def get_state_population_spinless_site(Ham, state, site):
    """
    Get the population at the given site for the given state for spinless fermions or spin 1/2's.

    Notes:
    ------
    This is equivalent to performing <psi| n_i |psi> for every particle number operator at each site i.
    
    e.g. N=2 |psi> = [1.2, 0.1, -0.1, 0.5] means
             |psi> = 1.2 |00> + 0.1 |01> - 0.1 |10> + 0.5 |11>

             n1 |psi> = 0.1 |01> + 0.5 |11>   -->  <n1> = <psi| n1 |psi> = |0.1|^2 + |0.5|^2
             --> <n1> = 0.26

    """

    stats = Ham.stats 
    sites = Ham.sites
    
    # Initialize population at the given site:
    pop = 0
    
    if Ham.sector_dim==None:    # state of the full Hamiltonian
        # TODO: rename variables here, they are confusing...

        d = len(state)      # Hilbert space dimension
        L = int(np.log2(d)) # sites

        # Loop over all states in the Hilbert space, extract the eigenvector weight,
        # and multiply the corresponding site with the weight:
        for i in range(d):
            bin = np.binary_repr(i, width=L)
            #print("binary ", bin)
            weight = np.abs(state[i])**2    # Contribution of the current basis state to the full state
            c = bin[sites - site - 1]    # The value of the state at the given site (as a string --> either 0 or 1)

            if stats == "spin-0.5":
                pop += (int(c)-0.5)*weight    # spins
            elif stats == "spinless-fermions":
                pop += int(c)*weight    # fermions


    else:   # fixed particle number sector N, smaller dimension of full Hilbert space

        N = int(Ham.sector)

        # Get basis for sector:
        #sector_basis = Ham.generate_basis_states_lex_list(N)
        sector_basis_dic = Ham.basis_states[N]

        for bin,ranks in sector_basis_dic.items():

            b_state = ranks['abs_rank']           # integer used to store the state
            idx_b = ranks['rel_rank']           # index of basis state in the given sector
            weight = np.abs(state[idx_b])**2    # weight that the state has in the basis state b

            # print("b_state:", b_state)
            # print("idx_b:", idx_b)
            # print("weight:", weight)
            
            if stats == "spin-0.5":
                pop += (Ham.get_site_value(b_state, site)-0.5)*weight    # spins
            elif stats == "spinless-fermions":
                pop += Ham.get_site_value(b_state, site)*weight    # fermions
            # print("pop:", pop)

    return pop

######################################################################################

######################################################################################
def get_state_population_bosons(Ham, state):
    """
    Get the population at each site for the given state for spinless fermions or spin 1/2's.

    """

    # Initialize the amplitudes:
    pop_vec = np.zeros(Ham.sites, dtype=float)

    # Populate all the entries in the array:
    for site in range(Ham.sites):
        #print("\n")
        #print(f"Calculating population for site {site}!!!!!!")
        #print("\n")
        pop_vec[site] = get_state_population_bosons_site(Ham=Ham, state=state, site=site)

    return pop_vec

######################################################################################

######################################################################################
def get_state_population_bosons_site(Ham, state, site):
    """
    Get the population at the given site for the given state for bosons.

    Notes:
    ------
    This is equivalent to performing <psi| n_i |psi> for every particle number operator at each site i.
    
    e.g. L=3, N=2:   |psi> = [-0.5, 0.6, 0.5*1j, -0.2, 0.1, 0.3] means
             |psi> = -0.5 |002> + 0.6 |011> + 0.5*1j |101> - 0.2 |020> + 0.1 |110> + 0.3 |200>

             n1 |psi> = 2*(-0.5) |002> + 1*0.6 |011> + 1*0.5*1j |101>  -->  <n1> = <psi| n1 |psi> = 2*|-0.5|^2 + 1*|0.6|^2 + 1*|0.5|^2
                                                                       -->  <n1> = 1.11
             n2 |psi> = 2*(-0.2) |020> + 1*0.6 |011> + 1*0.1 |110>     -->  <n2> = <psi| n2 |psi> = 2*|-0.2|^2 + 1*|0.6|^2 + 1*|0.1|^2
                                                                       -->  <n2> = 0.45
             n3 |psi> = 2*0.3 |200> + 1*0.1 |110> + 1*0.5*1j |101>     -->  <n3> = <psi| n3 |psi> = 2*|0.3|^2 + 1*|0.1|^2 + 1*|0.5|^2
                                                                       -->  <n3> = 0.44
                                                                       -->  <n1+n2+n3> = 2
    """

    sites = Ham.sites
    
    # Initialize population at the given site:
    pop = 0
    
    if Ham.sector_dim==None:    # state of the full Hamiltonian

        for N in range(0,sites+1):

            # Get basis for sector:
            sector_basis_dic = Ham.basis_states[N]

            for b_state_str, ranks in sector_basis_dic.items():

                b_state = ast.literal_eval(b_state_str)     # basis state in list format
                idx = ranks['abs_rank']                     # index of basis state in the given sector
                weight = np.abs(state[idx])**2              # weight that the state has in the basis state b

                #print("b_state:", b_state)
                #print("idx:", idx)
                #print("weight:", weight)
            
                pop += Ham.get_site_value(b_state, site)*weight
                #print("pop:", pop)

    else:   # fixed particle number sector N, smaller dimension of full Hilbert space

        N = int(Ham.sector)

        # Get basis for sector:
        sector_basis_dic = Ham.basis_states[N]

        for b_state_str, ranks in sector_basis_dic.items():

            b_state = ast.literal_eval(b_state_str)     # basis state in list format
            idx = ranks['rel_rank']                     # index of basis state in the given sector
            weight = np.abs(state[idx])**2              # weight that the state has in the basis state b

            #print("b_state:", b_state)
            #print("idx:", idx)
            #print("weight:", weight)
            
            pop += Ham.get_site_value(b_state, site)*weight
            #print("pop:", pop)

    return pop

######################################################################################
#[ 2.44953694e-12 -4.24264014e-08  1.41421344e-04  1.41421351e-04 -9.99999960e-01  1.41421344e-04 -1.63299303e-08  1.41421351e-04 -4.24264015e-08  2.44948937e-12]

######################################################################################
def get_state_population_spinful(state, Ham):
    """
    Get the population at each site for the given state for spinful fermions.

    Notes:
    ------
    Given state |psi> written in tensor basis:
    1) loop over its elements with index k; the elements are coefficients multiplying the tensor basis: 
        e.g. for Nup=1, Ndown=1, sites=2:
            |psi> = c1*(|01>x|01>) + c2*(|01>x|10>) + c3*(|10>x|01>) + c4*(|10>x|10>)
        where the first ket is for spin up and the second ket is for spin down,
        and |psi> is saved as a vector (c1, c2, c3, c4). This is the input.

    2) from index k of tensor product, get lexicographic indices of spin up and spin down components,
       i.e. k = i*Nsup + j, where Ns_up is the *number* of total states with Nup up spins in the given
       number of sites [ Nsup = (sites choose Nup)]. This is given by

       i = k // Nsup (integer division)
       j = k % Nsup (remainder)

       Note that i and j are indices of the basis states *ordered wrt the lexicographic convention per sector*.

    3) Now we know which indices i,j to use for a given k, but we still need to get the corresponding
       basis vectors in the spin-up and spin-down bases. Because writing the inverse function of lexRank
       would be complicated, we can instead re-generate the basis states for each spin sector together with
       their rank, and sort the states according to their rank. That way, indices i and j from 2) will point 
       to the right states. This step can be done at the very beginning, before starting the k-loop.

    4) Get the binary representation of each state of each spin component (e.g. 13 = 8 + 4 + 1 = |1101>). 
       Then loop over each site in the binary string and sum the spin weight (squared) at every site.
       e.g. for Nup=1, Ndown=1, sites=2 (see point 1) above):

       vec_up = (tot weight at site 1, tot weight at site 2) = (|c1|^2*1 + |c2|^2*1 + |c3|^2*0 + |c4|^2*0, |c1|^2*0 + |c2|^2*0 + |c3|^2*1 + |c4|^2*1)
       vec_down = (tot weight at site 1, tot weight at site 2) = (|c1|^2*1 + |c2|^2*0 + |c3|^2*1 + |c4|^2*0, |c1|^2*0 + |c2|^2*1 + |c3|^2*0 + |c4|^2*1)

    Recall that the sites are counted from the RIGHT, 
    e.g. |123> means there are 3 particles on site 1, 2 particles on site 2, and 1 particle on site 3.

    """

    if Ham.Nup_sector_dim==None and Ham.Ndown_sector_dim==None:    # state of the full Hamiltonian
        raise NotImplementedError()

    else:   # given particle number sectors Nup and Ndown, smaller dimension of full Hilbert space

        Nup = Ham.Nup_sector
        Ndown = Ham.Ndown_sector

        # Get lexicographically ordered basis for each sector:
        sector_basis_up = Ham.generate_basis_states_lex(Nup)
        lex_idx_up = []
        for state_up in sector_basis_up:
            lex_idx_up.append(utils.lexRank(np.binary_repr(state_up, width=Ham.sites))[0])
        sector_basis_up = [x for x, _ in sorted(zip(sector_basis_up,lex_idx_up))] 
        
        sector_basis_down = Ham.generate_basis_states_lex(Ndown)
        lex_idx_down = []
        for state_down in sector_basis_down:
            lex_idx_down.append(utils.lexRank(np.binary_repr(state_down, width=Ham.sites))[0])
        sector_basis_down = [x for x, _ in sorted(zip(sector_basis_down,lex_idx_down))] 

        print(f"sector_basis_up: {sector_basis_up}")
        print(f"sector_basis_down: {sector_basis_down}")

        # Now the two bases for up and down basis states are ordered according to 
        # their lexicographic index for the sector. We can then access the correct state
        # by sector_basis_down[idx], where idx comes directly from decoupling the tensor index
        # into spin up index and spin down index.

        tot_states_up = utils.binomial(Ham.sites, Nup)

        # Initialize the amplitudes:
        vec_up = np.zeros(Ham.sites)
        vec_down = np.zeros(Ham.sites)

        for idx, c in enumerate(state): #idx is the index of the tensor product state, c is its coefficient
            
            # Get the lexicographic index of the state of each spin component
            idx_down = idx // tot_states_up       
            idx_up = idx % tot_states_up

            #print(f"idx_up: {idx_up}")
            #print(f"idx_down: {idx_down}")

            # Get the corresponding state:
            state_up = sector_basis_up[idx_up]
            state_down = sector_basis_down[idx_down]

            #print(state_up)
            #print(state_down)
            
            # Rewrite the state in binary format: (e.g. 13 = 8 + 4 + 1 = |1101>)
            bin_up = np.binary_repr(state_up, width=Ham.sites)
            bin_down = np.binary_repr(state_down, width=Ham.sites)

            # Extract the value of the basis vector at each site and add it to
            # the vector representation for both spin up and spin down:
            for idx in range(Ham.sites):      
                vec_up[idx] += np.abs(c)**2*int(bin_up[idx])    
                vec_down[idx] += np.abs(c)**2*int(bin_down[idx])    

    return vec_up, vec_down

######################################################################################


